using System;
using discord_bot_danbot.Exceptions;
using discord_bot_danbot.Models;
using discord_bot_danbot.Services;
using Xunit;

namespace discord_bot_danbot.Tests.Services;

public class BotCommandParserServiceTest
{
    BotCommandParserService m_service;

    public BotCommandParserServiceTest()
    {
        m_service = new BotCommandParserService();
    }

    [Fact]
    public void Parse_GlobalExclude_Add()
    {
        // Test.
        BotCommand result = m_service.Parse(
            "globalexclude add tag1 tag2 tag3");
        
        // Assert.
        Assert.Equal("globalexclude", result.Command);
        Assert.Empty(result.Parameters);
        Assert.NotNull(result.SubCommand);
        Assert.Equal("add", result.SubCommand!.Command);
        Assert.Single(result.SubCommand.Parameters);
        Assert.Equal("tags", result.SubCommand.Parameters[0].Name);
        Assert.Equal(3, result.SubCommand.Parameters[0].Values.Length);
        Assert.Equal("tag1", result.SubCommand.Parameters[0].Values[0]);
        Assert.Equal("tag2", result.SubCommand.Parameters[0].Values[1]);
        Assert.Equal("tag3", result.SubCommand.Parameters[0].Values[2]);
    }

    [Fact]
    public void Parse_GlobalExclude_Remove()
    {
        // Test.
        BotCommand result = m_service.Parse(
            "globalexclude remove tag1 tag2 tag3");
        
        // Assert.
        Assert.Equal("globalexclude", result.Command);
        Assert.Empty(result.Parameters);
        Assert.NotNull(result.SubCommand);
        Assert.Equal("remove", result.SubCommand!.Command);
        Assert.Single(result.SubCommand.Parameters);
        Assert.Equal("tags", result.SubCommand.Parameters[0].Name);
        Assert.Equal(3, result.SubCommand.Parameters[0].Values.Length);
        Assert.Equal("tag1", result.SubCommand.Parameters[0].Values[0]);
        Assert.Equal("tag2", result.SubCommand.Parameters[0].Values[1]);
        Assert.Equal("tag3", result.SubCommand.Parameters[0].Values[2]);
    }

    [Fact]
    public void Parse_GlobalExclude_Unknown()
    {
        // Test.
        Assert.Throws<InvalidBotCommandException>(() =>
            m_service.Parse(
                "globalexclude"));
        Assert.Throws<InvalidBotCommandException>(() =>
            m_service.Parse(
                "globalexclude whatever"));
        Assert.Throws<InvalidBotCommandException>(() =>
            m_service.Parse(
                "globalexclude whatever tag1 tag2"));
        Assert.Throws<InvalidBotCommandException>(() =>
            m_service.Parse(
                "globalexclude add"));
        Assert.Throws<InvalidBotCommandException>(() =>
            m_service.Parse(
                "globalexclude remove"));
    }

    [Fact]
    public void Parse_Subscribe_Latest()
    {
        // Test.
        BotCommand result = m_service.Parse(
            "subscribe latest");
        
        // Assert.
        Assert.Equal("subscribe", result.Command);
        Assert.Single(result.Parameters);
        Assert.Equal("tags", result.Parameters[0].Name);
        Assert.Single(result.Parameters[0].Values);
        Assert.Equal("latest", result.Parameters[0].Values[0]);
        Assert.Null(result.SubCommand);
    }

    [Fact]
    public void Parse_Subscribe_LatestWithId()
    {
        // Test.
        BotCommand result = m_service.Parse(
            "subscribe latest from 1234");
        
        // Assert.
        Assert.Equal("subscribe", result.Command);
        Assert.Equal(2, result.Parameters.Length);

        Assert.Equal("tags", result.Parameters[0].Name);
        Assert.Single(result.Parameters[0].Values);
        Assert.Equal("latest", result.Parameters[0].Values[0]);

        Assert.Equal("fromId", result.Parameters[1].Name);
        Assert.Single(result.Parameters[1].Values);
        Assert.Equal("1234", result.Parameters[1].Values[0]);

        Assert.Null(result.SubCommand);
    }

    [Fact]
    public void Parse_Subscribe_LatestWithExclude()
    {
        // Test.
        BotCommand result = m_service.Parse(
            "subscribe latest exclude tag1 tag2");
        
        // Assert.
        Assert.Equal("subscribe", result.Command);
        Assert.Equal(2, result.Parameters.Length);

        Assert.Equal("tags", result.Parameters[0].Name);
        Assert.Single(result.Parameters[0].Values);
        Assert.Equal("latest", result.Parameters[0].Values[0]);

        Assert.Equal("excludeTags", result.Parameters[1].Name);
        Assert.Equal(2, result.Parameters[1].Values.Length);
        Assert.Equal("tag1", result.Parameters[1].Values[0]);
        Assert.Equal("tag2", result.Parameters[1].Values[1]);

        Assert.Null(result.SubCommand);
    }

    [Fact]
    public void Parse_Subscribe_LatestWithIdAndExclude()
    {
        // Test.
        BotCommand result = m_service.Parse(
            "subscribe latest from 1234 exclude tag1 tag2");
        
        // Assert.
        Assert.Equal("subscribe", result.Command);
        Assert.Equal(3, result.Parameters.Length);

        Assert.Equal("tags", result.Parameters[0].Name);
        Assert.Single(result.Parameters[0].Values);
        Assert.Equal("latest", result.Parameters[0].Values[0]);

        Assert.Equal("fromId", result.Parameters[1].Name);
        Assert.Single(result.Parameters[1].Values);
        Assert.Equal("1234", result.Parameters[1].Values[0]);

        Assert.Equal("excludeTags", result.Parameters[2].Name);
        Assert.Equal(2, result.Parameters[2].Values.Length);
        Assert.Equal("tag1", result.Parameters[2].Values[0]);
        Assert.Equal("tag2", result.Parameters[2].Values[1]);

        Assert.Null(result.SubCommand);
    }

    [Fact]
    public void Parse_Subscribe_TagsWithId()
    {
        // Test.
        BotCommand result = m_service.Parse(
            "subscribe tag1 tag2 from 1234");
        
        // Assert.
        Assert.Equal("subscribe", result.Command);
        Assert.Equal(2, result.Parameters.Length);

        Assert.Equal("tags", result.Parameters[0].Name);
        Assert.Equal(2, result.Parameters[0].Values.Length);
        Assert.Equal("tag1", result.Parameters[0].Values[0]);
        Assert.Equal("tag2", result.Parameters[0].Values[1]);

        Assert.Equal("fromId", result.Parameters[1].Name);
        Assert.Single(result.Parameters[1].Values);
        Assert.Equal("1234", result.Parameters[1].Values[0]);

        Assert.Null(result.SubCommand);
    }

    [Fact]
    public void Parse_Subscribe_TagsWithExclude()
    {
        // Test.
        BotCommand result = m_service.Parse(
            "subscribe tag1 tag2 exclude extag1 extag2");
        
        // Assert.
        Assert.Equal("subscribe", result.Command);
        Assert.Equal(2, result.Parameters.Length);

        Assert.Equal("tags", result.Parameters[0].Name);
        Assert.Equal(2, result.Parameters[0].Values.Length);
        Assert.Equal("tag1", result.Parameters[0].Values[0]);
        Assert.Equal("tag2", result.Parameters[0].Values[1]);

        Assert.Equal("excludeTags", result.Parameters[1].Name);
        Assert.Equal(2, result.Parameters[1].Values.Length);
        Assert.Equal("extag1", result.Parameters[1].Values[0]);
        Assert.Equal("extag2", result.Parameters[1].Values[1]);

        Assert.Null(result.SubCommand);
    }

    [Fact]
    public void Parse_Subscribe_TagsWithIdAndExclude()
    {
        // Test.
        BotCommand result = m_service.Parse(
            "subscribe tag1 tag2 from 1234 exclude extag1 extag2");
        
        // Assert.
        Assert.Equal("subscribe", result.Command);
        Assert.Equal(3, result.Parameters.Length);

        Assert.Equal("tags", result.Parameters[0].Name);
        Assert.Equal(2, result.Parameters[0].Values.Length);
        Assert.Equal("tag1", result.Parameters[0].Values[0]);
        Assert.Equal("tag2", result.Parameters[0].Values[1]);

        Assert.Equal("fromId", result.Parameters[1].Name);
        Assert.Single(result.Parameters[1].Values);
        Assert.Equal("1234", result.Parameters[1].Values[0]);

        Assert.Equal("excludeTags", result.Parameters[2].Name);
        Assert.Equal(2, result.Parameters[2].Values.Length);
        Assert.Equal("tag1", result.Parameters[2].Values[0]);
        Assert.Equal("tag2", result.Parameters[2].Values[1]);

        Assert.Null(result.SubCommand);
    }

    [Fact]
    public void Parse_Subscribe_Unknown()
    {
        // Test.
        Assert.Throws<InvalidBotCommandException>(() =>
            m_service.Parse(
                "subscribe latest whatever"));
        Assert.Throws<InvalidBotCommandException>(() =>
            m_service.Parse(
                "subscribe latest from invalidid"));
        Assert.Throws<InvalidBotCommandException>(() =>
            m_service.Parse(
                "subscribe latest from exclude tag1 tag2"));
        Assert.Throws<InvalidBotCommandException>(() =>
            m_service.Parse(
                "subscribe latest exclude"));
        Assert.Throws<InvalidBotCommandException>(() =>
            m_service.Parse(
                "subscribe latest from 1234 exclude"));
    }

    [Fact]
    public void Parse_List()
    {
        // Test.
        BotCommand result = m_service.Parse(
            "list");
        
        // Assert.
        Assert.Equal("list", result.Command);
        Assert.Empty(result.Parameters);
        Assert.Null(result.SubCommand);
    }

    [Fact]
    public void Parse_ListTags()
    {
        // Test.
        BotCommand result = m_service.Parse(
            "list tag1 tag2");
        
        // Assert.
        Assert.Equal("list", result.Command);
        Assert.Single(result.Parameters);
        Assert.Equal("tags", result.Parameters[0].Name);
        Assert.Equal(2, result.Parameters[0].Values.Length);
        Assert.Equal("tag1", result.Parameters[0].Values[0]);
        Assert.Equal("tag2", result.Parameters[0].Values[1]);
        Assert.Null(result.SubCommand);
    }

    [Fact]
    public void Parse_ListTagsWithSkip()
    {
        // Test.
        BotCommand result = m_service.Parse(
            "list tag1 tag2 skip 2");
        
        // Assert.
        Assert.Equal("list", result.Command);
        Assert.Equal(2, result.Parameters.Length);

        Assert.Equal("tags", result.Parameters[0].Name);
        Assert.Equal(2, result.Parameters[0].Values.Length);
        Assert.Equal("tag1", result.Parameters[0].Values[0]);
        Assert.Equal("tag2", result.Parameters[0].Values[1]);

        Assert.Equal("skip", result.Parameters[1].Name);
        Assert.Single(result.Parameters[1].Values);
        Assert.Equal("2", result.Parameters[1].Values[0]);

        Assert.Null(result.SubCommand);
    }

    [Fact]
    public void Parse_ListTagsWithExclude()
    {
        // Test.
        BotCommand result = m_service.Parse(
            "list tag1 tag2 exclude extag1 extag2");
        
        // Assert.
        Assert.Equal("list", result.Command);
        Assert.Equal(2, result.Parameters.Length);

        Assert.Equal("tags", result.Parameters[0].Name);
        Assert.Equal(2, result.Parameters[0].Values.Length);
        Assert.Equal("tag1", result.Parameters[0].Values[0]);
        Assert.Equal("tag2", result.Parameters[0].Values[1]);

        Assert.Equal("excludeTags", result.Parameters[1].Name);
        Assert.Equal(2, result.Parameters[1].Values.Length);
        Assert.Equal("extag1", result.Parameters[1].Values[0]);
        Assert.Equal("extag2", result.Parameters[1].Values[1]);

        Assert.Null(result.SubCommand);
    }

    [Fact]
    public void Parse_ListTagsWithSkipAndExclude()
    {
        // Test.
        BotCommand result = m_service.Parse(
            "list tag1 tag2 skip 2 exclude extag1 extag2");
        
        // Assert.
        Assert.Equal("list", result.Command);
        Assert.Equal(3, result.Parameters.Length);

        Assert.Equal("tags", result.Parameters[0].Name);
        Assert.Equal(2, result.Parameters[0].Values.Length);
        Assert.Equal("tag1", result.Parameters[0].Values[0]);
        Assert.Equal("tag2", result.Parameters[0].Values[1]);

        Assert.Equal("skip", result.Parameters[1].Name);
        Assert.Single(result.Parameters[1].Values);
        Assert.Equal("2", result.Parameters[1].Values[0]);

        Assert.Equal("excludeTags", result.Parameters[2].Name);
        Assert.Equal(2, result.Parameters[2].Values.Length);
        Assert.Equal("extag1", result.Parameters[2].Values[0]);
        Assert.Equal("extag2", result.Parameters[2].Values[1]);

        Assert.Null(result.SubCommand);
    }

    [Fact]
    public void Parse_List_Unknown()
    {
        // Test.
        Assert.Throws<InvalidBotCommandException>(() =>
            m_service.Parse(
                "list skip"));
        Assert.Throws<InvalidBotCommandException>(() =>
            m_service.Parse(
                "list skip invalidnumber"));
        Assert.Throws<InvalidBotCommandException>(() =>
            m_service.Parse(
                "list skip exclude"));
        Assert.Throws<InvalidBotCommandException>(() =>
            m_service.Parse(
                "list skip exclude tag1 tag2"));
        Assert.Throws<InvalidBotCommandException>(() =>
            m_service.Parse(
                "list exclude"));
    }

    [Fact]
    public void Parse_Hot()
    {
        // Test.
        BotCommand result = m_service.Parse(
            "hot");
        
        // Assert.
        Assert.Equal("hot", result.Command);
        Assert.Empty(result.Parameters);
        Assert.Null(result.SubCommand);
    }

    [Fact]
    public void Parse_HotWithSkip()
    {
        // Test.
        BotCommand result = m_service.Parse(
            "hot skip 2");
        
        // Assert.
        Assert.Equal("hot", result.Command);
        Assert.Single(result.Parameters);
        Assert.Equal("skip", result.Parameters[0].Name);
        Assert.Single(result.Parameters[0].Values);
        Assert.Equal("2", result.Parameters[0].Values[0]);
        Assert.Null(result.SubCommand);
    }

    [Fact]
    public void Parse_HotWithExclude()
    {
        // Test.
        BotCommand result = m_service.Parse(
            "hot exclude tag1 tag2");
        
        // Assert.
        Assert.Equal("hot", result.Command);
        Assert.Single(result.Parameters);
        Assert.Equal("excludeTags", result.Parameters[0].Name);
        Assert.Equal(2, result.Parameters[0].Values.Length);
        Assert.Equal("tag1", result.Parameters[0].Values[0]);
        Assert.Equal("tag2", result.Parameters[0].Values[1]);
        Assert.Null(result.SubCommand);
    }

    [Fact]
    public void Parse_HotWithSkipAndExclude()
    {
        // Test.
        BotCommand result = m_service.Parse(
            "hot skip 2");
        
        // Assert.
        Assert.Equal("hot", result.Command);
        Assert.Equal(2, result.Parameters.Length);

        Assert.Equal("skip", result.Parameters[0].Name);
        Assert.Single(result.Parameters[0].Values);
        Assert.Equal("2", result.Parameters[0].Values[0]);

        Assert.Equal("excludeTags", result.Parameters[1].Name);
        Assert.Equal(2, result.Parameters[1].Values.Length);
        Assert.Equal("tag1", result.Parameters[1].Values[0]);
        Assert.Equal("tag2", result.Parameters[1].Values[1]);

        Assert.Null(result.SubCommand);
    }

    [Fact]
    public void Parse_Hot_Unknown()
    {
        // Test.
        Assert.Throws<InvalidBotCommandException>(() =>
            m_service.Parse(
                "hot whatever"));
        Assert.Throws<InvalidBotCommandException>(() =>
            m_service.Parse(
                "hot skip invalidnumber"));
        Assert.Throws<InvalidBotCommandException>(() =>
            m_service.Parse(
                "hot skip exclude"));
        Assert.Throws<InvalidBotCommandException>(() =>
            m_service.Parse(
                "hot skip exclude tag1 tag2"));
        Assert.Throws<InvalidBotCommandException>(() =>
            m_service.Parse(
                "hot exclude"));
    }

    [Fact]
    public void Parse_Unsubscribe()
    {
        // Test.
        BotCommand result = m_service.Parse(
            "unsubscribe");
        
        // Assert.
        Assert.Equal("unsubscribe", result.Command);
        Assert.Empty(result.Parameters);
        Assert.Null(result.SubCommand);
    }

    [Fact]
    public void Parse_Unubscribe_Unknown()
    {
        // Test.
        Assert.Throws<InvalidBotCommandException>(() =>
            m_service.Parse(
                "unsubscribe whatever"));
    }

    [Fact]
    public void Parse_Unknown()
    {
        // Test.
        Assert.Throws<InvalidBotCommandException>(() =>
            m_service.Parse(
                "whatever"));
    }
}
