using System;
using discord_bot_danbot.Exceptions;
using discord_bot_danbot.Models;
using discord_bot_danbot.Services;
using Xunit;

namespace discord_bot_danbot.Tests.Services;

public class DanParserServiceTest
{
    DanParserService m_service;

    public DanParserServiceTest()
    {
        m_service = new DanParserService();
    }

    [Fact]
    public void ParsePost_Test()
    {
        // Test.
        Post result = m_service.ParsePost(POST_RAW_1);

        // Assert.
        Assert.Equal(5345054, result.Id);
        Assert.Equal(DateTime.Parse("2022-05-12T00:24-04:00"), result.Time);
        Assert.Equal("toma_(shinozaki)", result.Artist);
        Assert.Equal(
            new[]
            {
                "jouga_maya",
                "kafuu_chino",
                "natsu_megumi"
            },
            result.CharacterTags);
        Assert.Equal(
            new[]
            {
                "3girls",
                "alternate_costume",
                "bangs",
                "blue_eyes",
                "blue_hair",
                "blue_headwear",
                "blue_shorts",
                "blush",
                "bow",
                "bowtie",
                "brown_eyes",
                "chimame-tai",
                "deerstalker",
                "detective",
                "eyebrows_visible_through_hair",
                "fang",
                "girl_sandwich",
                "green_headwear",
                "green_shorts",
                "grey_hair",
                "hair_ornament",
                "hat",
                "long_hair",
                "looking_at_viewer",
                "multiple_girls",
                "one_eye_closed",
                "open_mouth",
                "pen",
                "plaid",
                "plaid_shorts",
                "pointing",
                "pointing_at_viewer",
                "red_eyes",
                "red_hair",
                "red_headwear",
                "red_shorts",
                "sandwiched",
                "short_hair",
                "shorts",
                "thighhighs",
                "twintails",
                "white_background",
                "white_legwear",
                "x_hair_ornament"
            },
            result.Tags);
        Assert.Equal(
            "https://cdn.donmai.us/original/47/ea/__kafuu_chino_jouga_maya_and_natsu_megumi_gochuumon_wa_usagi_desu_ka_drawn_by_toma_shinozaki__47ead5659950bff11a7436e5a4e4f51f.jpg",
            result.MediaUrl);
    }

    [Fact]
    public void ParsePost_Test2()
    {
        // Test.
        Post result = m_service.ParsePost(POST_RAW_2);

        // Assert.
        Assert.Equal(10, result.Id);
        Assert.Equal(DateTime.Parse("2005-05-24T00:24-04:00"), result.Time);
        Assert.Null(result.Artist);
        Assert.Equal(
            new[]
            {
                "iriya_kana"
            },
            result.CharacterTags);
        Assert.Equal(
            new[]
            {
                "1girl",
                ":o",
                "ahoge",
                "aircraft",
                "arm_behind_head",
                "arm_up",
                "bangs",
                "blood",
                "blush_stickers",
                "breasts",
                "cloud",
                "day",
                "eyebrows_visible_through_hair",
                "from_side",
                "glint",
                "head_tilt",
                "long_hair",
                "looking_at_viewer",
                "looking_back",
                "nosebleed",
                "oekaki",
                "outdoors",
                "parted_lips",
                "purple_eyes",
                "purple_hair",
                "school_uniform",
                "serafuku",
                "shirt",
                "short_sleeves",
                "sky",
                "small_breasts",
                "solo",
                "surprised",
                "ufo",
                "upper_body",
                "white_shirt"
            },
            result.Tags);
        Assert.Equal(
            "https://cdn.donmai.us/original/7c/2b/__iriya_kana_iriya_no_sora_ufo_no_natsu__7c2b4c3197d68c69488643d74815e790.png",
            result.MediaUrl);
    }

    [Fact]
    public void ParsePost_Test3()
    {
        // Test.
        Post result = m_service.ParsePost(POST_RAW_3);

        // Assert.
        Assert.Equal(5390721, result.Id);
        Assert.Equal(DateTime.Parse("2022-05-29T11:36-04:00"), result.Time);
        Assert.Equal("suzu_(susan_slr97)", result.Artist);
        Assert.Empty(result.CharacterTags);
        Assert.Equal(
            new[]
            {
                "blue_flower",
                "building",
                "bush",
                "cloud",
                "day",
                "flower",
                "forest",
                "leaf",
                "mansion",
                "meadow",
                "mountainous_horizon",
                "nature",
                "no_humans",
                "path",
                "rock",
                "scarlet_devil_mansion",
                "scenery",
                "sky",
                "tree",
                "white_flower",
                "yellow_flower"
            },
            result.Tags);
        Assert.Equal(
            "https://cdn.donmai.us/original/ee/8d/__touhou_drawn_by_suzu_susan_slr97__ee8daff3bed3d0d2430af3e8a736e86a.jpg",
            result.MediaUrl);
    }

    [Fact]
    public void ParsePost_TestInvalid()
    {
        // Test.
        bool error = false;

        try
        {
            m_service.ParsePost(RAW_INVALID);
        }
        catch (InvalidDanApiResponseException ex)
        {
            Assert.Contains("post id", ex.Message);
            error = true;
        }

        // Assert.
        Assert.True(error);
    }
    
    [Fact]
    public void ParsePostIdsAndTags_Test()
    {
        // Test.
        Post[] result = m_service.ParsePostIdsAndTags(POSTS_RAW_1);

        // Assert.
        Assert.Equal(2, result.Length);
        Assert.Equal(4928081, result[0].Id);
        Assert.Equal(
            new[]
            {
                "2girls",
                "arm_support",
                "bangs",
                "black_hair",
                "black_legwear",
                "blunt_bangs",
                "blush",
                "bob_cut",
                "brown_footwear",
                "brown_hair",
                "closed_mouth",
                "commentary_request",
                "eyebrows_visible_through_hair",
                "from_behind",
                "full_body",
                "green_skirt",
                "hair_ornament",
                "hair_ribbon",
                "hairclip",
                "highres",
                "hugging_own_legs",
                "kafuuin_hiiro",
                "kinoshita_ran",
                "knees_up",
                "loafers",
                "long_sleeves",
                "looking_at_viewer",
                "looking_back",
                "miniskirt",
                "mob_shika_katan!",
                "multiple_girls",
                "nekoyashiki_pushio",
                "novel_illustration",
                "official_art",
                "pantyhose",
                "ponytail",
                "profile",
                "purple_eyes",
                "red_eyes",
                "red_ribbon",
                "ribbon",
                "school_uniform",
                "shirt",
                "shoes",
                "short_hair",
                "sidelocks",
                "simple_background",
                "skirt",
                "socks",
                "untucked_shirt",
                "white_legwear",
                "white_shirt"
            },
            result[0].Tags);
        Assert.Equal(DateTime.MinValue, result[0].Time);
        Assert.Null(result[0].Artist);
        Assert.Empty(result[0].CharacterTags);
        Assert.Empty(result[0].MediaUrl);
        Assert.Equal(4928078, result[1].Id);
        Assert.Equal(
            new[]
            {
                "3girls",
                ":d",
                "ahoge",
                "anklet",
                "arm_support",
                "bangs",
                "bare_shoulders",
                "barefoot",
                "black_hair",
                "black_legwear",
                "black_ribbon",
                "blonde_hair",
                "blunt_bangs",
                "blush",
                "bob_cut",
                "braid",
                "breasts",
                "brown_footwear",
                "brown_hair",
                "character_request",
                "cleavage",
                "closed_mouth",
                "collarbone",
                "commentary_request",
                "earrings",
                "eyebrows_visible_through_hair",
                "fang",
                "full_body",
                "green_skirt",
                "hair_ornament",
                "hair_ribbon",
                "hairclip",
                "hand_up",
                "head_tilt",
                "highres",
                "hugging_own_legs",
                "jewelry",
                "kafuuin_hiiro",
                "kinoshita_ran",
                "knees_up",
                "large_breasts",
                "loafers",
                "long_hair",
                "long_sleeves",
                "looking_at_viewer",
                "looking_back",
                "miniskirt",
                "mob_shika_katan!",
                "multicolored_hair",
                "multiple_girls",
                "nail_polish",
                "necklace",
                "nekoyashiki_pushio",
                "novel_illustration",
                "off_shoulder",
                "official_art",
                "open_mouth",
                "pantyhose",
                "pink_nails",
                "ponytail",
                "profile",
                "purple_eyes",
                "red_eyes",
                "red_ribbon",
                "ribbon",
                "ring",
                "school_uniform",
                "shell",
                "shell_earrings",
                "shirt",
                "shoes",
                "short_hair",
                "sidelocks",
                "simple_background",
                "sitting",
                "skin_fang",
                "skirt",
                "smile",
                "socks",
                "sparkle",
                "star_(symbol)",
                "star_earrings",
                "streaked_hair",
                "thighs",
                "toenail_polish",
                "toenails",
                "toes",
                "very_long_hair",
                "wariza",
                "wedding_ring",
                "white_background",
                "white_legwear",
                "white_shirt"
            },
            result[1].Tags);
        Assert.Equal(DateTime.MinValue, result[1].Time);
        Assert.Null(result[1].Artist);
        Assert.Empty(result[1].CharacterTags);
        Assert.Empty(result[1].MediaUrl);
    }

    [Fact]
    public void ParsePostIdsAndTags_Test2()
    {
        // Test.
        Post[] result = m_service.ParsePostIdsAndTags(POSTS_RAW_2);

        // Assert.
        Assert.Empty(result);
    }

    [Fact]
    public void ParsePostIdsAndTags_TestInvalid()
    {
        // Test.
        Post[] result = m_service.ParsePostIdsAndTags(RAW_INVALID);

        // Assert.
        Assert.Empty(result);
    }

    [Fact]
    public void ParsePostPreview_Test()
    {
        // Test.
        Post result = m_service.ParsePostPreview(1234, POST_PREVIEW_RAW_1);

        // Assert.
        Assert.Equal(1234, result.Id);
        Assert.Equal(DateTime.Parse("2021-11-20T18:12-05:00"), result.Time);
        Assert.Equal("nekoyashiki_pushio", result.Artist);
        Assert.Equal(
            new[]
            {
                "kafuuin_hiiro",
                "kinoshita_ran"
            },
            result.CharacterTags);
        Assert.Equal(
            new[]
            {
                "2girls",
                "arm_support",
                "bangs",
                "black_hair",
                "black_legwear",
                "blunt_bangs",
                "blush",
                "bob_cut",
                "brown_footwear",
                "brown_hair",
                "closed_mouth",
                "eyebrows_visible_through_hair",
                "from_behind",
                "full_body",
                "green_skirt",
                "hair_ornament",
                "hair_ribbon",
                "hairclip",
                "hugging_own_legs",
                "knees_up",
                "loafers",
                "long_sleeves",
                "looking_at_viewer",
                "looking_back",
                "miniskirt",
                "multiple_girls",
                "pantyhose",
                "ponytail",
                "profile",
                "purple_eyes",
                "red_eyes",
                "red_ribbon",
                "ribbon",
                "school_uniform",
                "shirt",
                "shoes",
                "short_hair",
                "sidelocks",
                "simple_background",
                "skirt",
                "socks",
                "untucked_shirt",
                "white_legwear",
                "white_shirt"
            },
            result.Tags);
        Assert.Equal(
            "https://cdn.donmai.us/original/c4/56/c4560222ebe8a87818dd8f8667108c58.jpg",
            result.MediaUrl);
    }

    [Fact]
    public void ParsePostPreview_Test2()
    {
        // Test.
        Post result = m_service.ParsePostPreview(10, POST_PREVIEW_RAW_2);

        // Assert.
        Assert.Equal(10, result.Id);
        Assert.Equal(DateTime.Parse("2005-05-24T00:24-04:00"), result.Time);
        Assert.Null(result.Artist);
        Assert.Equal(
            new[]
            {
                "iriya_kana"
            },
            result.CharacterTags);
        Assert.Equal(
            new[]
            {
                "1girl",
                ":o",
                "ahoge",
                "aircraft",
                "arm_behind_head",
                "arm_up",
                "bangs",
                "blood",
                "blush_stickers",
                "breasts",
                "cloud",
                "day",
                "eyebrows_visible_through_hair",
                "from_side",
                "glint",
                "head_tilt",
                "long_hair",
                "looking_at_viewer",
                "looking_back",
                "nosebleed",
                "oekaki",
                "outdoors",
                "parted_lips",
                "purple_eyes",
                "purple_hair",
                "school_uniform",
                "serafuku",
                "shirt",
                "short_sleeves",
                "sky",
                "small_breasts",
                "solo",
                "surprised",
                "ufo",
                "upper_body",
                "white_shirt"
            },
            result.Tags);
        Assert.Equal(
            "https://cdn.donmai.us/original/7c/2b/7c2b4c3197d68c69488643d74815e790.png",
            result.MediaUrl);
    }

    [Fact]
    public void ParsePostPreview_Test3()
    {
        // Test.
        Post result = m_service.ParsePostPreview(5390721, POST_PREVIEW_RAW_3);

        // Assert.
        Assert.Equal(5390721, result.Id);
        Assert.Equal(DateTime.Parse("2022-05-29T11:36-04:00"), result.Time);
        Assert.Equal("suzu_(susan_slr97)", result.Artist);
        Assert.Empty(result.CharacterTags);
        Assert.Equal(
            new[]
            {
                "blue_flower",
                "building",
                "bush",
                "cloud",
                "day",
                "flower",
                "forest",
                "leaf",
                "mansion",
                "meadow",
                "mountainous_horizon",
                "nature",
                "no_humans",
                "path",
                "rock",
                "scarlet_devil_mansion",
                "scenery",
                "sky",
                "tree",
                "white_flower",
                "yellow_flower"
            },
            result.Tags);
        Assert.Equal(
            "https://cdn.donmai.us/original/ee/8d/ee8daff3bed3d0d2430af3e8a736e86a.jpg",
            result.MediaUrl);
    }

    [Fact]
    public void ParsePostPreview_TestInvalid()
    {
        // Test.
        bool error = false;

        try
        {
            m_service.ParsePostPreview(10, RAW_INVALID);
        }
        catch (InvalidDanApiResponseException ex)
        {
            Assert.Contains("post time", ex.Message);
            error = true;
        }

        // Assert.
        Assert.True(error);
    }

    const string POST_RAW_1 = @"
<!doctype html>
<html lang=""en"">
<head>
  <meta charset=""utf-8"">
  <title>kafuu chino, jouga maya, and natsu megumi (gochuumon wa usagi desu ka?) drawn by toma_(shinozaki) | Danbooru</title>
  <link rel=""icon"" href=""/favicon.ico"" sizes=""16x16"" type=""image/x-icon"">
  <link rel=""icon"" href=""/favicon.svg"" sizes=""any"" type=""image/svg+xml"">

  <link rel=""canonical"" href=""https://danbooru.donmai.us/posts/5345054"">
  <link rel=""search"" type=""application/opensearchdescription+xml"" href=""https://danbooru.donmai.us/opensearch.xml?version=2"" title=""Search posts"">

  <meta name=""csrf-param"" content=""authenticity_token"" />
<meta name=""csrf-token"" content=""vlizcKAJiqjIzQsYojEXNokFMmkgBoKfbK41GppjZes8iLithG-_cbKOgFrY279v7BgrFCRXbhhVG0jDz297XQ"" />
    <meta name=""viewport"" content=""width=device-width,initial-scale=1"">
    <meta name=""blacklisted-tags"" content=""guro,scat,furry -rating:s"">

  <meta name=""autocomplete-tag-prefixes"" content=""[&quot;ch:&quot;,&quot;co:&quot;,&quot;gen:&quot;,&quot;char:&quot;,&quot;copy:&quot;,&quot;art:&quot;,&quot;meta:&quot;,&quot;general:&quot;,&quot;character:&quot;,&quot;copyright:&quot;,&quot;artist:&quot;]"">

    <script src=""/packs/js/runtime-502cc943ed047c87eac6.js""></script>
<script src=""/packs/js/20-34b75d7170810064ff25.js""></script>
<script src=""/packs/js/application-1f97225d1d2f4b3370ec.js""></script>
<script src=""/packs/js/780-9a149046694c464f5a58.js""></script>
<script src=""/packs/js/alpine-983543cca5034868c8ce.js""></script>

  <link rel=""stylesheet"" href=""/packs/css/20-1647607e.css"" />
<link rel=""stylesheet"" href=""/packs/css/application-2632588c.css"" />


  <meta name=""description"" content=""View this 1144x990 1 MB image"">
  <meta property=""og:type"" content=""website"">
  <meta property=""og:site_name"" content=""Danbooru"">
  <meta property=""og:title"" content=""kafuu chino, jouga maya, and natsu megumi (gochuumon wa usagi desu ka?) drawn by toma_(shinozaki) | Danbooru"">
  <meta property=""og:description"" content=""View this 1144x990 1 MB image"">
  <meta property=""og:url"" content=""https://danbooru.donmai.us/posts/5345054"">

    <meta name=""twitter:site"" content=""@danboorubot"">

  <meta name=""twitter:title"" content=""kafuu chino, jouga maya, and natsu megumi (gochuumon wa usagi desu ka?) drawn by toma_(shinozaki) | Danbooru"">
  <meta name=""twitter:description"" content=""View this 1144x990 1 MB image"">

  <meta name=""git-hash"" content=""81bd86d202afc2558f73d04f0683d7f806e8cdc2"">
  <meta name=""theme-color"" content=""hsl(213, 100%, 50%)"">

  <link rel=""alternate"" type=""application/atom+xml"" title=""Comments for post #5345054"" href=""https://danbooru.donmai.us/comments.atom?search%5Bpost_id%5D=5345054"" />  <meta name=""post-id"" content=""5345054"">
  <meta name=""post-has-embedded-notes"" content=""false"">

    <meta property=""og:image"" content=""https://cdn.donmai.us/sample/47/ea/sample-47ead5659950bff11a7436e5a4e4f51f.jpg"">


    <meta name=""twitter:card"" content=""summary_large_image"">

      <meta name=""twitter:image"" content=""https://cdn.donmai.us/sample/47/ea/sample-47ead5659950bff11a7436e5a4e4f51f.jpg"">



</head>

<body lang=""en"" class=""c-posts a-show flex flex-col"" spellcheck=""false"" data-controller=""posts"" data-action=""show"" data-layout=""sidebar"" data-current-user-ip-addr=""110.137.195.145"" data-current-user-save-data=""false"" data-current-user-id=""null"" data-current-user-name=""Anonymous"" data-current-user-level=""0"" data-current-user-level-string=""Anonymous"" data-current-user-theme=""auto"" data-current-user-comment-threshold=""-8"" data-current-user-default-image-size=""large"" data-current-user-time-zone=""Eastern Time (US &amp; Canada)"" data-current-user-per-page=""20"" data-current-user-is-banned=""false"" data-current-user-receive-email-notifications=""false"" data-current-user-new-post-navigation-layout=""true"" data-current-user-enable-private-favorites=""false"" data-current-user-style-usernames=""false"" data-current-user-show-deleted-children=""false"" data-current-user-can-approve-posts=""false"" data-current-user-can-upload-free=""false"" data-current-user-disable-categorized-saved-searches=""false"" data-current-user-disable-tagged-filenames=""false"" data-current-user-disable-mobile-gestures=""false"" data-current-user-enable-safe-mode=""false"" data-current-user-enable-desktop-mode=""false"" data-current-user-disable-post-tooltips=""false"" data-current-user-requires-verification=""false"" data-current-user-is-verified=""false"" data-current-user-show-deleted-posts=""false"" data-current-user-is-member=""false"" data-current-user-is-gold=""false"" data-current-user-is-platinum=""false"" data-current-user-is-builder=""false"" data-current-user-is-restricted=""false"" data-current-user-is-moderator=""false"" data-current-user-is-anonymous=""true"" data-current-user-is-owner=""false"" data-current-user-is-admin=""false"" data-current-user-is-approver=""false"" data-cookie-news-ticker=""null"" data-cookie-hide-upgrade-account-notice=""null"" data-cookie-hide-verify-account-notice=""null"" data-cookie-hide-dmail-notice=""null"" data-cookie-dab=""null"" data-cookie-show-relationship-previews=""null"" data-cookie-post-preview-size=""null"" data-cookie-post-preview-show-votes=""null"" data-post-id=""5345054"" data-post-created-at=""&quot;2022-05-12T00:24:36.520-04:00&quot;"" data-post-uploader-id=""898759"" data-post-score=""7"" data-post-last-comment-bumped-at=""null"" data-post-image-width=""1144"" data-post-image-height=""990"" data-post-fav-count=""7"" data-post-last-noted-at=""null"" data-post-parent-id=""null"" data-post-has-children=""false"" data-post-approver-id=""602326"" data-post-tag-count-general=""44"" data-post-tag-count-artist=""1"" data-post-tag-count-character=""3"" data-post-tag-count-copyright=""1"" data-post-file-size=""1051655"" data-post-up-score=""7"" data-post-down-score=""0"" data-post-is-pending=""false"" data-post-is-flagged=""false"" data-post-is-deleted=""false"" data-post-tag-count=""50"" data-post-updated-at=""&quot;2022-05-17T10:20:02.462-04:00&quot;"" data-post-is-banned=""false"" data-post-pixiv-id=""70480481"" data-post-last-commented-at=""null"" data-post-has-active-children=""false"" data-post-bit-flags=""0"" data-post-tag-count-meta=""1"" data-post-has-large=""true"" data-post-current-image-size=""large"">

  <header id=""top"">
    <div id=""app-name-header"" class=""font-bold font-header leading-normal inline-flex items-center gap-1"">
      <a id=""app-logo"" href=""/""><img src=""/packs/static/danbooru-logo-128x128-ea111b6658173e847734.png"" /></a>
      <a id=""app-name"" href=""/"">Danbooru</a>
    </div>

    <div id=""maintoggle"" class=""mobile-only"">
      <a href=""#""><i class=""icon fas fa-bars "" id=""maintoggle-on""></i></a>
      <a href=""#""><i class=""icon fas fa-times "" id=""maintoggle-off"" style=""display: none;""></i></a>
    </div>

    <nav id=""nav"">
      <menu id=""main-menu"" class=""main"">
          <li id=""nav-login""><a id=""nav-login-link"" rel=""nofollow"" href=""/login?url=%2Fposts%2F5345054"">Login</a></li>

        <li id=""nav-posts"" class="" current""><a id=""nav-posts-link"" href=""/posts"">Posts</a></li>
        <li id=""nav-comments""><a id=""nav-comments-link"" href=""/comments"">Comments</a></li>
        <li id=""nav-notes""><a id=""nav-notes-link"" href=""/notes"">Notes</a></li>
        <li id=""nav-artists""><a id=""nav-artists-link"" href=""/artists"">Artists</a></li>
        <li id=""nav-tags""><a id=""nav-tags-link"" href=""/tags"">Tags</a></li>
        <li id=""nav-pools""><a id=""nav-pools-link"" href=""/pools/gallery"">Pools</a></li>
        <li id=""nav-wiki""><a id=""nav-wiki-link"" href=""/wiki_pages/help:home"">Wiki</a></li>
        <li id=""nav-forum""><a id=""nav-forum-link"" href=""/forum_topics"">Forum</a></li>


        <li id=""nav-more""><a id=""nav-more-link"" href=""/static/site_map"">More »</a></li>
      </menu>

        <menu id=""subnav-menu"">
            <li id=""subnav-listing""><a id=""subnav-listing-link"" href=""/posts"">Listing</a></li>
    <li id=""subnav-upload""><a id=""subnav-upload-link"" href=""/login?url=%2Fuploads%2Fnew"">Upload</a></li>
  <li id=""subnav-hot""><a id=""subnav-hot-link"" href=""/posts?d=1&amp;tags=order%3Arank"">Hot</a></li>
  <li id=""subnav-changes""><a id=""subnav-changes-link"" href=""/post_versions"">Changes</a></li>
  <li id=""subnav-help""><a id=""subnav-help-link"" href=""/wiki_pages/help:posts"">Help</a></li>

        </menu>
    </nav>
  </header>

  <div id=""page"" class=""flex-1 mt-4"">





    <div class=""notice notice-info"" id=""notice"" style=""display: none;"">
      <span class=""prose"">.</span>
      <a href=""#"" id=""close-notice-link"">close</a>
    </div>









  <script type=""text/javascript"">
    $(function() {
      $.post(""https://isshiki.donmai.us/post_views"", {
        msg: ""IjUzNDUwNTQsNjU5ZDQzYzczZjk1ZDhiOWM2MGJkZjEzNDNhMTUyMGQi--f259a157197ffd86b42a51e8211fb42564c7633dd267669805d9afd2a09eacf6""
      });
    });
  </script>

  <div id=""c-posts"">
    <div id=""a-show"">

      <div class=""sidebar-container"">
        <aside id=""sidebar"">

<section id=""search-box"">
  <h2>Search</h2>
  <form id=""search-box-form"" class=""flex"" action=""/posts"" accept-charset=""UTF-8"" method=""get"">
    <input type=""text"" name=""tags"" id=""tags"" class=""flex-auto"" data-shortcut=""q"" data-autocomplete=""tag-query"" />
    <input type=""hidden"" name=""z"" id=""z"" value=""5"" autocomplete=""off"" />
    <button id=""search-box-submit"" type=""submit""><i class=""icon fas fa-search ""></i></button>
</form></section>


  <div id=""blacklist-box"" class=""sidebar-blacklist"">
  <h2>Blacklisted (<a class=""wiki-link"" href=""/wiki_pages/help:blacklists"">help</a>)</h2>
  <ul id=""blacklist-list"" class=""list-bulleted""></ul>
  <a id=""disable-all-blacklists"" style=""display: none;"" href=""#"">Disable all</a>
  <a id=""re-enable-all-blacklists"" style=""display: none;"" href=""#"">Re-enable all</a>
</div>


  <section id=""tag-list"">
    <div class=""tag-list categorized-tag-list"">
    <h3 class=""artist-tag-list"">
      Artists
    </h3>

    <ul class=""artist-tag-list"">
        <li class=""tag-type-1"" data-tag-name=""toma_(shinozaki)"" data-is-deprecated=""false"">
            <a class=""wiki-link"" href=""/artists/show_or_new?name=toma_%28shinozaki%29&amp;z=1"">?</a>


          <a class=""search-tag"" href=""/posts?tags=toma_%28shinozaki%29&amp;z=1"">toma (shinozaki)</a>

          <span class=""post-count"" title=""108"">108</span>
        </li>
    </ul>
    <h3 class=""copyright-tag-list"">
      Copyrights
    </h3>

    <ul class=""copyright-tag-list"">
        <li class=""tag-type-3"" data-tag-name=""gochuumon_wa_usagi_desu_ka?"" data-is-deprecated=""false"">
            <a class=""wiki-link"" href=""/wiki_pages/gochuumon_wa_usagi_desu_ka%3F?z=1"">?</a>


          <a class=""search-tag"" href=""/posts?tags=gochuumon_wa_usagi_desu_ka%3F&amp;z=1"">gochuumon wa usagi desu ka?</a>

          <span class=""post-count"" title=""12130"">12k</span>
        </li>
    </ul>
    <h3 class=""character-tag-list"">
      Characters
    </h3>

    <ul class=""character-tag-list"">
        <li class=""tag-type-4"" data-tag-name=""jouga_maya"" data-is-deprecated=""false"">
            <a class=""wiki-link"" href=""/wiki_pages/jouga_maya?z=1"">?</a>


          <a class=""search-tag"" href=""/posts?tags=jouga_maya&amp;z=1"">jouga maya</a>

          <span class=""post-count"" title=""806"">806</span>
        </li>
        <li class=""tag-type-4"" data-tag-name=""kafuu_chino"" data-is-deprecated=""false"">
            <a class=""wiki-link"" href=""/wiki_pages/kafuu_chino?z=1"">?</a>


          <a class=""search-tag"" href=""/posts?tags=kafuu_chino&amp;z=1"">kafuu chino</a>

          <span class=""post-count"" title=""6035"">6.0k</span>
        </li>
        <li class=""tag-type-4"" data-tag-name=""natsu_megumi"" data-is-deprecated=""false"">
            <a class=""wiki-link"" href=""/wiki_pages/natsu_megumi?z=1"">?</a>


          <a class=""search-tag"" href=""/posts?tags=natsu_megumi&amp;z=1"">natsu megumi</a>

          <span class=""post-count"" title=""760"">760</span>
        </li>
    </ul>
    <h3 class=""general-tag-list"">
      General
    </h3>

    <ul class=""general-tag-list"">
        <li class=""tag-type-0"" data-tag-name=""3girls"" data-is-deprecated=""false"">
            <a class=""wiki-link"" href=""/wiki_pages/3girls?z=1"">?</a>


          <a class=""search-tag"" href=""/posts?tags=3girls&amp;z=1"">3girls</a>

          <span class=""post-count"" title=""170387"">170k</span>
        </li>
        <li class=""tag-type-0"" data-tag-name=""alternate_costume"" data-is-deprecated=""false"">
            <a class=""wiki-link"" href=""/wiki_pages/alternate_costume?z=1"">?</a>


          <a class=""search-tag"" href=""/posts?tags=alternate_costume&amp;z=1"">alternate costume</a>

          <span class=""post-count"" title=""214897"">214k</span>
        </li>
        <li class=""tag-type-0"" data-tag-name=""bangs"" data-is-deprecated=""false"">
            <a class=""wiki-link"" href=""/wiki_pages/bangs?z=1"">?</a>


          <a class=""search-tag"" href=""/posts?tags=bangs&amp;z=1"">bangs</a>

          <span class=""post-count"" title=""1263374"">1.3M</span>
        </li>
        <li class=""tag-type-0"" data-tag-name=""blue_eyes"" data-is-deprecated=""false"">
            <a class=""wiki-link"" href=""/wiki_pages/blue_eyes?z=1"">?</a>


          <a class=""search-tag"" href=""/posts?tags=blue_eyes&amp;z=1"">blue eyes</a>

          <span class=""post-count"" title=""1106874"">1.1M</span>
        </li>
        <li class=""tag-type-0"" data-tag-name=""blue_hair"" data-is-deprecated=""false"">
            <a class=""wiki-link"" href=""/wiki_pages/blue_hair?z=1"">?</a>


          <a class=""search-tag"" href=""/posts?tags=blue_hair&amp;z=1"">blue hair</a>

          <span class=""post-count"" title=""494818"">494k</span>
        </li>
        <li class=""tag-type-0"" data-tag-name=""blue_headwear"" data-is-deprecated=""false"">
            <a class=""wiki-link"" href=""/wiki_pages/blue_headwear?z=1"">?</a>


          <a class=""search-tag"" href=""/posts?tags=blue_headwear&amp;z=1"">blue headwear</a>

          <span class=""post-count"" title=""20445"">20k</span>
        </li>
        <li class=""tag-type-0"" data-tag-name=""blue_shorts"" data-is-deprecated=""false"">
            <a class=""wiki-link"" href=""/wiki_pages/blue_shorts?z=1"">?</a>


          <a class=""search-tag"" href=""/posts?tags=blue_shorts&amp;z=1"">blue shorts</a>

          <span class=""post-count"" title=""18617"">18k</span>
        </li>
        <li class=""tag-type-0"" data-tag-name=""blush"" data-is-deprecated=""false"">
            <a class=""wiki-link"" href=""/wiki_pages/blush?z=1"">?</a>


          <a class=""search-tag"" href=""/posts?tags=blush&amp;z=1"">blush</a>

          <span class=""post-count"" title=""1843523"">1.8M</span>
        </li>
        <li class=""tag-type-0"" data-tag-name=""bow"" data-is-deprecated=""false"">
            <a class=""wiki-link"" href=""/wiki_pages/bow?z=1"">?</a>


          <a class=""search-tag"" href=""/posts?tags=bow&amp;z=1"">bow</a>

          <span class=""post-count"" title=""718815"">718k</span>
        </li>
        <li class=""tag-type-0"" data-tag-name=""bowtie"" data-is-deprecated=""false"">
            <a class=""wiki-link"" href=""/wiki_pages/bowtie?z=1"">?</a>


          <a class=""search-tag"" href=""/posts?tags=bowtie&amp;z=1"">bowtie</a>

          <span class=""post-count"" title=""147244"">147k</span>
        </li>
        <li class=""tag-type-0"" data-tag-name=""brown_eyes"" data-is-deprecated=""false"">
            <a class=""wiki-link"" href=""/wiki_pages/brown_eyes?z=1"">?</a>


          <a class=""search-tag"" href=""/posts?tags=brown_eyes&amp;z=1"">brown eyes</a>

          <span class=""post-count"" title=""595884"">595k</span>
        </li>
        <li class=""tag-type-0"" data-tag-name=""chimame-tai"" data-is-deprecated=""false"">
            <a class=""wiki-link"" href=""/wiki_pages/chimame-tai?z=1"">?</a>


          <a class=""search-tag"" href=""/posts?tags=chimame-tai&amp;z=1"">chimame-tai</a>

          <span class=""post-count"" title=""166"">166</span>
        </li>
        <li class=""tag-type-0"" data-tag-name=""deerstalker"" data-is-deprecated=""false"">
            <a class=""wiki-link"" href=""/wiki_pages/deerstalker?z=1"">?</a>


          <a class=""search-tag"" href=""/posts?tags=deerstalker&amp;z=1"">deerstalker</a>

          <span class=""post-count"" title=""1095"">1.1k</span>
        </li>
        <li class=""tag-type-0"" data-tag-name=""detective"" data-is-deprecated=""false"">
            <a class=""wiki-link"" href=""/wiki_pages/detective?z=1"">?</a>


          <a class=""search-tag"" href=""/posts?tags=detective&amp;z=1"">detective</a>

          <span class=""post-count"" title=""960"">960</span>
        </li>
        <li class=""tag-type-0"" data-tag-name=""eyebrows_visible_through_hair"" data-is-deprecated=""false"">
            <a class=""wiki-link"" href=""/wiki_pages/eyebrows_visible_through_hair?z=1"">?</a>


          <a class=""search-tag"" href=""/posts?tags=eyebrows_visible_through_hair&amp;z=1"">eyebrows visible through hair</a>

          <span class=""post-count"" title=""901344"">901k</span>
        </li>
        <li class=""tag-type-0"" data-tag-name=""fang"" data-is-deprecated=""false"">
            <a class=""wiki-link"" href=""/wiki_pages/fang?z=1"">?</a>


          <a class=""search-tag"" href=""/posts?tags=fang&amp;z=1"">fang</a>

          <span class=""post-count"" title=""199606"">199k</span>
        </li>
        <li class=""tag-type-0"" data-tag-name=""girl_sandwich"" data-is-deprecated=""false"">
            <a class=""wiki-link"" href=""/wiki_pages/girl_sandwich?z=1"">?</a>


          <a class=""search-tag"" href=""/posts?tags=girl_sandwich&amp;z=1"">girl sandwich</a>

          <span class=""post-count"" title=""5843"">5.8k</span>
        </li>
        <li class=""tag-type-0"" data-tag-name=""green_headwear"" data-is-deprecated=""false"">
            <a class=""wiki-link"" href=""/wiki_pages/green_headwear?z=1"">?</a>


          <a class=""search-tag"" href=""/posts?tags=green_headwear&amp;z=1"">green headwear</a>

          <span class=""post-count"" title=""13094"">13k</span>
        </li>
        <li class=""tag-type-0"" data-tag-name=""green_shorts"" data-is-deprecated=""false"">
            <a class=""wiki-link"" href=""/wiki_pages/green_shorts?z=1"">?</a>


          <a class=""search-tag"" href=""/posts?tags=green_shorts&amp;z=1"">green shorts</a>

          <span class=""post-count"" title=""4771"">4.8k</span>
        </li>
        <li class=""tag-type-0"" data-tag-name=""grey_hair"" data-is-deprecated=""false"">
            <a class=""wiki-link"" href=""/wiki_pages/grey_hair?z=1"">?</a>


          <a class=""search-tag"" href=""/posts?tags=grey_hair&amp;z=1"">grey hair</a>

          <span class=""post-count"" title=""164382"">164k</span>
        </li>
        <li class=""tag-type-0"" data-tag-name=""hair_ornament"" data-is-deprecated=""false"">
            <a class=""wiki-link"" href=""/wiki_pages/hair_ornament?z=1"">?</a>


          <a class=""search-tag"" href=""/posts?tags=hair_ornament&amp;z=1"">hair ornament</a>

          <span class=""post-count"" title=""832439"">832k</span>
        </li>
        <li class=""tag-type-0"" data-tag-name=""hat"" data-is-deprecated=""false"">
            <a class=""wiki-link"" href=""/wiki_pages/hat?z=1"">?</a>


          <a class=""search-tag"" href=""/posts?tags=hat&amp;z=1"">hat</a>

          <span class=""post-count"" title=""811470"">811k</span>
        </li>
        <li class=""tag-type-0"" data-tag-name=""long_hair"" data-is-deprecated=""false"">
            <a class=""wiki-link"" href=""/wiki_pages/long_hair?z=1"">?</a>


          <a class=""search-tag"" href=""/posts?tags=long_hair&amp;z=1"">long hair</a>

          <span class=""post-count"" title=""2682611"">2.7M</span>
        </li>
        <li class=""tag-type-0"" data-tag-name=""looking_at_viewer"" data-is-deprecated=""false"">
            <a class=""wiki-link"" href=""/wiki_pages/looking_at_viewer?z=1"">?</a>


          <a class=""search-tag"" href=""/posts?tags=looking_at_viewer&amp;z=1"">looking at viewer</a>

          <span class=""post-count"" title=""1817656"">1.8M</span>
        </li>
        <li class=""tag-type-0"" data-tag-name=""multiple_girls"" data-is-deprecated=""false"">
            <a class=""wiki-link"" href=""/wiki_pages/multiple_girls?z=1"">?</a>


          <a class=""search-tag"" href=""/posts?tags=multiple_girls&amp;z=1"">multiple girls</a>

          <span class=""post-count"" title=""1039760"">1.0M</span>
        </li>
        <li class=""tag-type-0"" data-tag-name=""one_eye_closed"" data-is-deprecated=""false"">
            <a class=""wiki-link"" href=""/wiki_pages/one_eye_closed?z=1"">?</a>


          <a class=""search-tag"" href=""/posts?tags=one_eye_closed&amp;z=1"">one eye closed</a>

          <span class=""post-count"" title=""261839"">261k</span>
        </li>
        <li class=""tag-type-0"" data-tag-name=""open_mouth"" data-is-deprecated=""false"">
            <a class=""wiki-link"" href=""/wiki_pages/open_mouth?z=1"">?</a>


          <a class=""search-tag"" href=""/posts?tags=open_mouth&amp;z=1"">open mouth</a>

          <span class=""post-count"" title=""1410970"">1.4M</span>
        </li>
        <li class=""tag-type-0"" data-tag-name=""pen"" data-is-deprecated=""false"">
            <a class=""wiki-link"" href=""/wiki_pages/pen?z=1"">?</a>


          <a class=""search-tag"" href=""/posts?tags=pen&amp;z=1"">pen</a>

          <span class=""post-count"" title=""11186"">11k</span>
        </li>
        <li class=""tag-type-0"" data-tag-name=""plaid"" data-is-deprecated=""false"">
            <a class=""wiki-link"" href=""/wiki_pages/plaid?z=1"">?</a>


          <a class=""search-tag"" href=""/posts?tags=plaid&amp;z=1"">plaid</a>

          <span class=""post-count"" title=""88486"">88k</span>
        </li>
        <li class=""tag-type-0"" data-tag-name=""plaid_shorts"" data-is-deprecated=""false"">
            <a class=""wiki-link"" href=""/wiki_pages/plaid_shorts?z=1"">?</a>


          <a class=""search-tag"" href=""/posts?tags=plaid_shorts&amp;z=1"">plaid shorts</a>

          <span class=""post-count"" title=""318"">318</span>
        </li>
        <li class=""tag-type-0"" data-tag-name=""pointing"" data-is-deprecated=""false"">
            <a class=""wiki-link"" href=""/wiki_pages/pointing?z=1"">?</a>


          <a class=""search-tag"" href=""/posts?tags=pointing&amp;z=1"">pointing</a>

          <span class=""post-count"" title=""38253"">38k</span>
        </li>
        <li class=""tag-type-0"" data-tag-name=""pointing_at_viewer"" data-is-deprecated=""false"">
            <a class=""wiki-link"" href=""/wiki_pages/pointing_at_viewer?z=1"">?</a>


          <a class=""search-tag"" href=""/posts?tags=pointing_at_viewer&amp;z=1"">pointing at viewer</a>

          <span class=""post-count"" title=""5917"">5.9k</span>
        </li>
        <li class=""tag-type-0"" data-tag-name=""red_eyes"" data-is-deprecated=""false"">
            <a class=""wiki-link"" href=""/wiki_pages/red_eyes?z=1"">?</a>


          <a class=""search-tag"" href=""/posts?tags=red_eyes&amp;z=1"">red eyes</a>

          <span class=""post-count"" title=""811351"">811k</span>
        </li>
        <li class=""tag-type-0"" data-tag-name=""red_hair"" data-is-deprecated=""false"">
            <a class=""wiki-link"" href=""/wiki_pages/red_hair?z=1"">?</a>


          <a class=""search-tag"" href=""/posts?tags=red_hair&amp;z=1"">red hair</a>

          <span class=""post-count"" title=""299902"">299k</span>
        </li>
        <li class=""tag-type-0"" data-tag-name=""red_headwear"" data-is-deprecated=""false"">
            <a class=""wiki-link"" href=""/wiki_pages/red_headwear?z=1"">?</a>


          <a class=""search-tag"" href=""/posts?tags=red_headwear&amp;z=1"">red headwear</a>

          <span class=""post-count"" title=""25676"">25k</span>
        </li>
        <li class=""tag-type-0"" data-tag-name=""red_shorts"" data-is-deprecated=""false"">
            <a class=""wiki-link"" href=""/wiki_pages/red_shorts?z=1"">?</a>


          <a class=""search-tag"" href=""/posts?tags=red_shorts&amp;z=1"">red shorts</a>

          <span class=""post-count"" title=""7860"">7.9k</span>
        </li>
        <li class=""tag-type-0"" data-tag-name=""sandwiched"" data-is-deprecated=""false"">
            <a class=""wiki-link"" href=""/wiki_pages/sandwiched?z=1"">?</a>


          <a class=""search-tag"" href=""/posts?tags=sandwiched&amp;z=1"">sandwiched</a>

          <span class=""post-count"" title=""7098"">7.1k</span>
        </li>
        <li class=""tag-type-0"" data-tag-name=""short_hair"" data-is-deprecated=""false"">
            <a class=""wiki-link"" href=""/wiki_pages/short_hair?z=1"">?</a>


          <a class=""search-tag"" href=""/posts?tags=short_hair&amp;z=1"">short hair</a>

          <span class=""post-count"" title=""1428028"">1.4M</span>
        </li>
        <li class=""tag-type-0"" data-tag-name=""shorts"" data-is-deprecated=""false"">
            <a class=""wiki-link"" href=""/wiki_pages/shorts?z=1"">?</a>


          <a class=""search-tag"" href=""/posts?tags=shorts&amp;z=1"">shorts</a>

          <span class=""post-count"" title=""227123"">227k</span>
        </li>
        <li class=""tag-type-0"" data-tag-name=""thighhighs"" data-is-deprecated=""false"">
            <a class=""wiki-link"" href=""/wiki_pages/thighhighs?z=1"">?</a>


          <a class=""search-tag"" href=""/posts?tags=thighhighs&amp;z=1"">thighhighs</a>

          <span class=""post-count"" title=""816255"">816k</span>
        </li>
        <li class=""tag-type-0"" data-tag-name=""twintails"" data-is-deprecated=""false"">
            <a class=""wiki-link"" href=""/wiki_pages/twintails?z=1"">?</a>


          <a class=""search-tag"" href=""/posts?tags=twintails&amp;z=1"">twintails</a>

          <span class=""post-count"" title=""591197"">591k</span>
        </li>
        <li class=""tag-type-0"" data-tag-name=""white_background"" data-is-deprecated=""false"">
            <a class=""wiki-link"" href=""/wiki_pages/white_background?z=1"">?</a>


          <a class=""search-tag"" href=""/posts?tags=white_background&amp;z=1"">white background</a>

          <span class=""post-count"" title=""759122"">759k</span>
        </li>
        <li class=""tag-type-0"" data-tag-name=""white_legwear"" data-is-deprecated=""false"">
            <a class=""wiki-link"" href=""/wiki_pages/white_legwear?z=1"">?</a>


          <a class=""search-tag"" href=""/posts?tags=white_legwear&amp;z=1"">white legwear</a>

          <span class=""post-count"" title=""253843"">253k</span>
        </li>
        <li class=""tag-type-0"" data-tag-name=""x_hair_ornament"" data-is-deprecated=""false"">
            <a class=""wiki-link"" href=""/wiki_pages/x_hair_ornament?z=1"">?</a>


          <a class=""search-tag"" href=""/posts?tags=x_hair_ornament&amp;z=1"">x hair ornament</a>

          <span class=""post-count"" title=""38693"">38k</span>
        </li>
    </ul>
    <h3 class=""meta-tag-list"">
      Meta
    </h3>

    <ul class=""meta-tag-list"">
        <li class=""tag-type-5"" data-tag-name=""commentary_request"" data-is-deprecated=""false"">
            <a class=""wiki-link"" href=""/wiki_pages/commentary_request?z=1"">?</a>


          <a class=""search-tag"" href=""/posts?tags=commentary_request&amp;z=1"">commentary request</a>

          <span class=""post-count"" title=""2521955"">2.5M</span>
        </li>
    </ul>
</div>

  </section>

  <section id=""post-information"">
    <h2>Information</h2>

    <ul>
      <li id=""post-info-id"">ID: 5345054</li>

      <li id=""post-info-uploader"">
        Uploader: <a class=""user user-member "" data-user-id=""898759"" data-user-name=""kidonng"" data-user-level=""20"" href=""/users/898759"">kidonng</a>
        <a href=""/posts?tags=user%3Akidonng"">»</a>
      </li>

      <li id=""post-info-date"">
        Date: <a href=""/posts?tags=date%3A2022-05-12""><time datetime=""2022-05-12T00:24-04:00"" title=""2022-05-12 00:24:36 -0400"" class="""">17 days ago</time></a>
      </li>

        <li id=""post-info-approver"">
          Approver: <a class=""user user-builder  user-post-approver user-post-uploader"" data-user-id=""602326"" data-user-name=""mongirlfan"" data-user-level=""32"" href=""/users/602326"">mongirlfan</a>
          <a href=""/posts?tags=approver%3Amongirlfan"">»</a>
        </li>

      <li id=""post-info-size"">
        Size: <a href=""https://cdn.donmai.us/original/47/ea/__kafuu_chino_jouga_maya_and_natsu_megumi_gochuumon_wa_usagi_desu_ka_drawn_by_toma_shinozaki__47ead5659950bff11a7436e5a4e4f51f.jpg"">1 MB .jpg</a>
        (1144x990)
        <a href=""/media_assets/5926192"">»</a>
      </li>

      <li id=""post-info-source"">Source: <a rel=""external noreferrer nofollow"" href=""https://www.pixiv.net/artworks/70480481"">pixiv.net/artworks/70480481</a>&nbsp;<a rel=""external noreferrer nofollow"" href=""https://i.pximg.net/img-original/img/2018/09/01/00/19/19/70480481_p0.jpg"">»</a></li>
      <li id=""post-info-rating"">Rating: Sensitive</li>

      <li id=""post-info-score"">
        Score: <span class=""post-votes"" data-id=""5345054"">
    <a class=""post-upvote-link inactive-link"" href=""/login?url=%2Fposts%2F5345054""><svg class=""icon svg-icon upvote-icon "" role=""img"" xmlns=""http://www.w3.org/2000/svg"" viewBox=""0 0 448 512""><path fill=""currentColor"" d=""M272 480h-96c-13.3 0-24-10.7-24-24V256H48.2c-21.4 0-32.1-25.8-17-41L207 39c9.4-9.4 24.6-9.4 34 0l175.8 176c15.1 15.1 4.4 41-17 41H296v200c0 13.3-10.7 24-24 24z"" /></svg></a>

  <span class=""post-score"">
    <a rel=""nofollow"" href=""/post_votes?search%5Bpost_id%5D=5345054&amp;variant=compact"">7</a>
  </span>

    <a class=""post-downvote-link inactive-link"" href=""/login?url=%2Fposts%2F5345054""><svg class=""icon svg-icon downvote-icon "" role=""img"" xmlns=""http://www.w3.org/2000/svg"" viewBox=""0 0 448 512""><path fill=""currentColor"" d=""M176 32h96c13.3 0 24 10.7 24 24v200h103.8c21.4 0 32.1 25.8 17 41L241 473c-9.4 9.4-24.6 9.4-34 0L31.3 297c-15.1-15.1-4.4-41 17-41H152V56c0-13.3 10.7-24 24-24z"" /></svg></a>
</span>

      </li>

      <li id=""post-info-favorites"">
        Favorites:
        <span class=""post-favcount"" data-id=""5345054"">
          <a rel=""nofollow"" href=""/posts/5345054/favorites"">7</a>
</span>      </li>

      <li id=""post-info-status"">
        Status:
          Active

      </li>
    </ul>
  </section>

  <section id=""post-options"">
    <h2>Options</h2>

    <ul>
        <li id=""post-option-resize-to-window"">
          <a class=""image-resize-to-window-link"" data-shortcut=""z"" href=""#"">Resize to window</a>
        </li>

        <li id=""post-option-view-large"">
          <a class=""image-view-large-link"" href=""https://cdn.donmai.us/sample/47/ea/__kafuu_chino_jouga_maya_and_natsu_megumi_gochuumon_wa_usagi_desu_ka_drawn_by_toma_shinozaki__sample-47ead5659950bff11a7436e5a4e4f51f.jpg"">View smaller</a>
        </li>

        <li id=""post-option-view-original"">
          <a class=""image-view-original-link"" href=""https://cdn.donmai.us/original/47/ea/__kafuu_chino_jouga_maya_and_natsu_megumi_gochuumon_wa_usagi_desu_ka_drawn_by_toma_shinozaki__47ead5659950bff11a7436e5a4e4f51f.jpg"">View original</a>
        </li>

      <li id=""post-option-find-similar"">
        <a ref=""nofollow"" href=""/iqdb_queries?post_id=5345054"">Find similar</a>
      </li>

        <li id=""post-option-download"">
          <a download=""kafuu chino, jouga maya, and natsu megumi (gochuumon wa usagi desu ka?) drawn by toma_(shinozaki) - 47ead5659950bff11a7436e5a4e4f51f.jpg"" href=""https://cdn.donmai.us/original/47/ea/__kafuu_chino_jouga_maya_and_natsu_megumi_gochuumon_wa_usagi_desu_ka_drawn_by_toma_shinozaki__47ead5659950bff11a7436e5a4e4f51f.jpg?download=1"">Download</a>
        </li>







    </ul>
  </section>

  <section id=""post-history"">
    <h2>History</h2>
    <ul>
      <li id=""post-history-tags""><a href=""/post_versions?search%5Bpost_id%5D=5345054"">Tags</a></li>
      <li id=""post-history-pools""><a href=""/pool_versions?search%5Bpost_id%5D=5345054"">Pools</a></li>
      <li id=""post-history-notes""><a href=""/note_versions?search%5Bpost_id%5D=5345054"">Notes</a></li>
      <li id=""post-history-moderation""><a href=""/posts/5345054/events"">Moderation</a></li>
      <li id=""post-history-commentary""><a href=""/artist_commentary_versions?search%5Bpost_id%5D=5345054"">Commentary</a></li>
      <li id=""post-history-replacements""><a href=""/post_replacements?search%5Bpost_id%5D=5345054"">Replacements</a></li>
    </ul>
  </section>

        </aside>

        <section id=""content"">





    <div class=""notice notice-small post-notice post-notice-resized"" id=""image-resize-notice"">
      Resized to 74% of original (<a class=""image-view-original-link"" href=""https://cdn.donmai.us/original/47/ea/__kafuu_chino_jouga_maya_and_natsu_megumi_gochuumon_wa_usagi_desu_ka_drawn_by_toma_shinozaki__47ead5659950bff11a7436e5a4e4f51f.jpg"">view original</a>)
    </div>

  <section class=""image-container note-container"" data-id=""5345054"" data-tags=""3girls alternate_costume bangs blue_eyes blue_hair blue_headwear blue_shorts blush bow bowtie brown_eyes chimame-tai commentary_request deerstalker detective eyebrows_visible_through_hair fang girl_sandwich gochuumon_wa_usagi_desu_ka? green_headwear green_shorts grey_hair hair_ornament hat jouga_maya kafuu_chino long_hair looking_at_viewer multiple_girls natsu_megumi one_eye_closed open_mouth pen plaid plaid_shorts pointing pointing_at_viewer red_eyes red_hair red_headwear red_shorts sandwiched short_hair shorts thighhighs toma_(shinozaki) twintails white_background white_legwear x_hair_ornament"" data-rating=""s"" data-large-width=""850"" data-large-height=""735"" data-width=""1144"" data-height=""990"" data-flags="""" data-score=""7"" data-uploader-id=""898759"" data-source=""https://i.pximg.net/img-original/img/2018/09/01/00/19/19/70480481_p0.jpg"" data-normalized-source=""https://www.pixiv.net/artworks/70480481"" data-file-url=""https://cdn.donmai.us/original/47/ea/47ead5659950bff11a7436e5a4e4f51f.jpg"">      <picture>
        <source media=""(max-width: 660px)"" srcset=""https://cdn.donmai.us/sample/47/ea/__kafuu_chino_jouga_maya_and_natsu_megumi_gochuumon_wa_usagi_desu_ka_drawn_by_toma_shinozaki__sample-47ead5659950bff11a7436e5a4e4f51f.jpg"">
        <img width=""850"" height=""735"" id=""image"" class=""fit-width"" alt=""kafuu chino, jouga maya, and natsu megumi (gochuumon wa usagi desu ka?) drawn by toma_(shinozaki)"" src=""https://cdn.donmai.us/sample/47/ea/__kafuu_chino_jouga_maya_and_natsu_megumi_gochuumon_wa_usagi_desu_ka_drawn_by_toma_shinozaki__sample-47ead5659950bff11a7436e5a4e4f51f.jpg"">
      </picture>

    <div id=""note-preview""></div>
</section>

  <section id=""mark-as-translated-section"">
    <form class=""simple_form edit_post"" id=""edit_post_5345054"" autocomplete=""off"" novalidate=""novalidate"" action=""/posts/5345054/mark_as_translated"" accept-charset=""UTF-8"" method=""post""><input type=""hidden"" name=""_method"" value=""put"" autocomplete=""off"" /><input type=""hidden"" name=""authenticity_token"" value=""6dbLoDBnPZrJAlPorKto5cSoWwzKJzJI1h_9Xr3XyjawLDUYN8n10QqgcU6ZFkKcKE2Sn0GXnYz-RpglH7Bbpw"" autocomplete=""off"" />
      <div class=""input hidden post_tags_query""><input name=""tags_query"" class=""hidden"" autocomplete=""off"" type=""hidden"" /></div>
      <div class=""input hidden post_pool_id""><input name=""pool_id"" class=""hidden"" autocomplete=""off"" type=""hidden"" /></div>
      <div class=""input hidden post_favgroup_id""><input name=""favgroup_id"" class=""hidden"" autocomplete=""off"" type=""hidden"" /></div>

      <fieldset class=""inline-fieldset"">
        <div class=""input boolean optional post_check_translation""><input name=""post[check_translation]"" type=""hidden"" value=""0"" autocomplete=""off"" /><input class=""boolean optional"" type=""checkbox"" value=""1"" name=""post[check_translation]"" id=""post_check_translation"" /><label class=""boolean optional"" for=""post_check_translation"">Check translation</label></div>
        <div class=""input boolean optional post_partially_translated""><input name=""post[partially_translated]"" type=""hidden"" value=""0"" autocomplete=""off"" /><input class=""boolean optional"" type=""checkbox"" value=""1"" name=""post[partially_translated]"" id=""post_partially_translated"" /><label class=""boolean optional"" for=""post_partially_translated"">Partially translated</label></div>
      </fieldset>

      <input type=""submit"" name=""commit"" value=""Mark as translated"" data-disable-with=""Mark as translated"" />
</form>  </section>

    <div id=""artist-commentary"">
      <h2>Artist's commentary</h2>

<menu id=""commentary-sections"">
    <li><b>Original</b></li>
</menu>

  <section id=""original-artist-commentary"">
    <h3><span class=""prose "">チマメ隊におまかせ！</span></h3>
    <div class=""prose ""><p>謎解きイベント行けそうにない…。</p></div>
</section>

    </div>

    <ul class=""notice post-notice post-notice-search"">
    <li class=""search-navbar"" data-selected=""true"">
      <a rel=""nofollow prev"" class=""prev"" data-shortcut=""a"" href=""/posts/5345054/show_seq?q=status%3Aany&amp;seq=prev"">‹ prev</a>
      <span class=""search-name"">Search: <a rel=""nofollow"" href=""/posts?tags=status%3Aany"">status:any</a></span>
      <a rel=""nofollow next"" class=""next"" data-shortcut=""d"" href=""/posts/5345054/show_seq?q=status%3Aany&amp;seq=next"">next ›</a>
    </li>


</ul>


  <menu id=""post-sections"" class=""mb-4"">
      <li class=""active""><a href=""#comments"">Comments</a></li>

      <li><a href=""#recommended"">Recommended</a></li>

  </menu>

    <section id=""recommended"" style=""display: none;"">
      <p><em>Loading...</em></p>
    </section>

    <section id=""comments"">
      <div class=""comments-for-post"" data-post-id=""5345054"">

  <div class=""list-of-comments list-of-messages"">
      <p>There are no comments.</p>
  </div>

</div>

    </section>

  <section id=""notes"" style=""display: none;"">
  </section>


        </section>
      </div>
    </div>
  </div>

  </div>

  <div id=""tooltips"">
    <div id=""post-tooltips""></div>
    <div id=""comment-votes-tooltips""></div>
    <div id=""post-votes-tooltips""></div>
    <div id=""post-favorites-tooltips""></div>
    <div id=""user-tooltips""></div>
    <div id=""popup-menus""></div>
  </div>

  <footer id=""page-footer"" class=""text-sm flex-initial"">
    <span class=""page-footer-app-name"" title=""Running commit: 81bd86d20"">Danbooru</span>
    / <a href=""/terms_of_service"">Terms</a>
    / <a href=""/privacy"">Privacy</a>
    / <a href=""/2257"">2257</a>
    / <a href=""/upgrade"">Upgrade</a>
    / <a href=""/contact"">Contact</a>
    </span>
  </footer>
<script defer src=""https://static.cloudflareinsights.com/beacon.min.js/v652eace1692a40cfa3763df669d7439c1639079717194"" integrity=""sha512-Gi7xpJR8tSkrpF7aordPZQlW2DLtzUlZcumS8dMQjwDHEnw9I7ZLyiOj/6tZStRBGtGgN6ceN6cMH8z7etPGlw=="" data-cf-beacon='{""rayId"":""712e6ef0faf3017a"",""token"":""d251ec98e50f454880fc91e7b6fa8ba8"",""version"":""2021.12.0"",""si"":100}' crossorigin=""anonymous""></script>
</body></html>
";

    const string POST_RAW_2 = @"
<!doctype html>
<html lang=""en"">
<head>
  <meta charset=""utf-8"">
  <title>iriya kana (iriya no sora ufo no natsu) | Danbooru</title>

  <link rel=""icon"" href=""/favicon.ico"" sizes=""16x16"" type=""image/x-icon"">
  <link rel=""icon"" href=""/favicon.svg"" sizes=""any"" type=""image/svg+xml"">

  <link rel=""canonical"" href=""https://danbooru.donmai.us/posts/10"">
  <link rel=""search"" type=""application/opensearchdescription+xml"" href=""https://danbooru.donmai.us/opensearch.xml?version=2"" title=""Search posts"">

  <meta name=""csrf-param"" content=""authenticity_token"" />
<meta name=""csrf-token"" content=""dsYjmDcxZaMJMLPPpb0pFo3TKgZTdRxW7xqkfZfbizkyLgZy4EzbDtdwYfnSVunBPoGhnO1SjJ1rb-hlzZ7-vQ"" />
    <meta name=""viewport"" content=""width=device-width,initial-scale=1"">
    <meta name=""blacklisted-tags"" content=""guro,scat,furry -rating:s"">

  <meta name=""autocomplete-tag-prefixes"" content=""[&quot;ch:&quot;,&quot;co:&quot;,&quot;gen:&quot;,&quot;char:&quot;,&quot;copy:&quot;,&quot;art:&quot;,&quot;meta:&quot;,&quot;general:&quot;,&quot;character:&quot;,&quot;copyright:&quot;,&quot;artist:&quot;]"">

    <script src=""/packs/js/runtime-502cc943ed047c87eac6.js""></script>
<script src=""/packs/js/20-34b75d7170810064ff25.js""></script>
<script src=""/packs/js/application-1f97225d1d2f4b3370ec.js""></script>
<script src=""/packs/js/780-9a149046694c464f5a58.js""></script>
<script src=""/packs/js/alpine-983543cca5034868c8ce.js""></script>

  <link rel=""stylesheet"" href=""/packs/css/20-1647607e.css"" />
<link rel=""stylesheet"" href=""/packs/css/application-2632588c.css"" />


  <meta name=""description"" content=""View this 400x400 36.4 KB image"">
  <meta property=""og:type"" content=""website"">
  <meta property=""og:site_name"" content=""Danbooru"">
  <meta property=""og:title"" content=""iriya kana (iriya no sora ufo no natsu) | Danbooru"">
  <meta property=""og:description"" content=""View this 400x400 36.4 KB image"">
  <meta property=""og:url"" content=""https://danbooru.donmai.us/posts/10"">

    <meta name=""twitter:site"" content=""@danboorubot"">

  <meta name=""twitter:title"" content=""iriya kana (iriya no sora ufo no natsu) | Danbooru"">
  <meta name=""twitter:description"" content=""View this 400x400 36.4 KB image"">

  <meta name=""git-hash"" content=""81bd86d202afc2558f73d04f0683d7f806e8cdc2"">
  <meta name=""theme-color"" content=""hsl(213, 100%, 50%)"">

  <link rel=""alternate"" type=""application/atom+xml"" title=""Comments for post #10"" href=""https://danbooru.donmai.us/comments.atom?search%5Bpost_id%5D=10"" />  <meta name=""post-id"" content=""10"">
  <meta name=""post-has-embedded-notes"" content=""false"">

    <meta property=""og:image"" content=""https://cdn.donmai.us/original/7c/2b/7c2b4c3197d68c69488643d74815e790.png"">


    <meta name=""twitter:card"" content=""summary_large_image"">

      <meta name=""twitter:image"" content=""https://cdn.donmai.us/original/7c/2b/7c2b4c3197d68c69488643d74815e790.png"">



</head>

<body lang=""en"" class=""c-posts a-show flex flex-col"" spellcheck=""false"" data-controller=""posts"" data-action=""show"" data-layout=""sidebar"" data-current-user-ip-addr=""110.137.195.145"" data-current-user-save-data=""false"" data-current-user-id=""null"" data-current-user-name=""Anonymous"" data-current-user-level=""0"" data-current-user-level-string=""Anonymous"" data-current-user-theme=""auto"" data-current-user-comment-threshold=""-8"" data-current-user-default-image-size=""large"" data-current-user-time-zone=""Eastern Time (US &amp; Canada)"" data-current-user-per-page=""20"" data-current-user-is-banned=""false"" data-current-user-receive-email-notifications=""false"" data-current-user-new-post-navigation-layout=""true"" data-current-user-enable-private-favorites=""false"" data-current-user-style-usernames=""false"" data-current-user-show-deleted-children=""false"" data-current-user-can-approve-posts=""false"" data-current-user-can-upload-free=""false"" data-current-user-disable-categorized-saved-searches=""false"" data-current-user-disable-tagged-filenames=""false"" data-current-user-disable-mobile-gestures=""false"" data-current-user-enable-safe-mode=""false"" data-current-user-enable-desktop-mode=""false"" data-current-user-disable-post-tooltips=""false"" data-current-user-requires-verification=""false"" data-current-user-is-verified=""false"" data-current-user-show-deleted-posts=""false"" data-current-user-is-anonymous=""true"" data-current-user-is-member=""false"" data-current-user-is-gold=""false"" data-current-user-is-platinum=""false"" data-current-user-is-builder=""false"" data-current-user-is-moderator=""false"" data-current-user-is-restricted=""false"" data-current-user-is-admin=""false"" data-current-user-is-owner=""false"" data-current-user-is-approver=""false"" data-cookie-news-ticker=""null"" data-cookie-hide-upgrade-account-notice=""null"" data-cookie-hide-verify-account-notice=""null"" data-cookie-hide-dmail-notice=""null"" data-cookie-dab=""null"" data-cookie-show-relationship-previews=""null"" data-cookie-post-preview-size=""null"" data-cookie-post-preview-show-votes=""null"" data-post-id=""10"" data-post-created-at=""&quot;2005-05-24T00:24:22.000-04:00&quot;"" data-post-uploader-id=""1"" data-post-score=""2"" data-post-last-comment-bumped-at=""&quot;2020-07-02T09:51:23.984-04:00&quot;"" data-post-image-width=""400"" data-post-image-height=""400"" data-post-fav-count=""19"" data-post-last-noted-at=""&quot;2007-06-20T16:27:28.632-04:00&quot;"" data-post-parent-id=""null"" data-post-has-children=""false"" data-post-approver-id=""null"" data-post-tag-count-general=""36"" data-post-tag-count-artist=""0"" data-post-tag-count-character=""1"" data-post-tag-count-copyright=""1"" data-post-file-size=""37230"" data-post-up-score=""4"" data-post-down-score=""-2"" data-post-is-pending=""false"" data-post-is-flagged=""false"" data-post-is-deleted=""true"" data-post-tag-count=""42"" data-post-updated-at=""&quot;2022-05-22T23:54:39.635-04:00&quot;"" data-post-is-banned=""false"" data-post-pixiv-id=""null"" data-post-last-commented-at=""&quot;2020-07-02T09:51:23.984-04:00&quot;"" data-post-has-active-children=""false"" data-post-bit-flags=""0"" data-post-tag-count-meta=""4"" data-post-has-large=""false"" data-post-current-image-size=""original"">

  <header id=""top"">
    <div id=""app-name-header"" class=""font-bold font-header leading-normal inline-flex items-center gap-1"">
      <a id=""app-logo"" href=""/""><img src=""/packs/static/danbooru-logo-128x128-ea111b6658173e847734.png"" /></a>
      <a id=""app-name"" href=""/"">Danbooru</a>
    </div>

    <div id=""maintoggle"" class=""mobile-only"">
      <a href=""#""><i class=""icon fas fa-bars "" id=""maintoggle-on""></i></a>
      <a href=""#""><i class=""icon fas fa-times "" id=""maintoggle-off"" style=""display: none;""></i></a>
    </div>

    <nav id=""nav"">
      <menu id=""main-menu"" class=""main"">
          <li id=""nav-login""><a id=""nav-login-link"" rel=""nofollow"" href=""/login?url=%2Fposts%2F10"">Login</a></li>

        <li id=""nav-posts"" class="" current""><a id=""nav-posts-link"" href=""/posts"">Posts</a></li>
        <li id=""nav-comments""><a id=""nav-comments-link"" href=""/comments"">Comments</a></li>
        <li id=""nav-notes""><a id=""nav-notes-link"" href=""/notes"">Notes</a></li>
        <li id=""nav-artists""><a id=""nav-artists-link"" href=""/artists"">Artists</a></li>
        <li id=""nav-tags""><a id=""nav-tags-link"" href=""/tags"">Tags</a></li>
        <li id=""nav-pools""><a id=""nav-pools-link"" href=""/pools/gallery"">Pools</a></li>
        <li id=""nav-wiki""><a id=""nav-wiki-link"" href=""/wiki_pages/help:home"">Wiki</a></li>
        <li id=""nav-forum""><a id=""nav-forum-link"" href=""/forum_topics"">Forum</a></li>


        <li id=""nav-more""><a id=""nav-more-link"" href=""/static/site_map"">More »</a></li>
      </menu>

        <menu id=""subnav-menu"">
            <li id=""subnav-listing""><a id=""subnav-listing-link"" href=""/posts"">Listing</a></li>
    <li id=""subnav-upload""><a id=""subnav-upload-link"" href=""/login?url=%2Fuploads%2Fnew"">Upload</a></li>
  <li id=""subnav-hot""><a id=""subnav-hot-link"" href=""/posts?d=1&amp;tags=order%3Arank"">Hot</a></li>
  <li id=""subnav-changes""><a id=""subnav-changes-link"" href=""/post_versions"">Changes</a></li>
  <li id=""subnav-help""><a id=""subnav-help-link"" href=""/wiki_pages/help:posts"">Help</a></li>

        </menu>
    </nav>
  </header>

  <div id=""page"" class=""flex-1 mt-4"">





    <div class=""notice notice-info"" id=""notice"" style=""display: none;"">
      <span class=""prose"">.</span>
      <a href=""#"" id=""close-notice-link"">close</a>
    </div>









  <script type=""text/javascript"">
    $(function() {
      $.post(""https://isshiki.donmai.us/post_views"", {
        msg: ""IjEwLDZlMzI5NmYwZDYzNWRkZjgxOTUwZDAwNzQzNTVjMjljIg==--2f374db5ad4286a751c00cc4d57a195ac8badb23a114c4c969ecd7e6b37b36f1""
      });
    });
  </script>

  <div id=""c-posts"">
    <div id=""a-show"">

      <div class=""sidebar-container"">
        <aside id=""sidebar"">

<section id=""search-box"">
  <h2>Search</h2>
  <form id=""search-box-form"" class=""flex"" action=""/posts"" accept-charset=""UTF-8"" method=""get"">
    <input type=""text"" name=""tags"" id=""tags"" class=""flex-auto"" data-shortcut=""q"" data-autocomplete=""tag-query"" />
    <input type=""hidden"" name=""z"" id=""z"" value=""5"" autocomplete=""off"" />
    <button id=""search-box-submit"" type=""submit""><i class=""icon fas fa-search ""></i></button>
</form></section>


  <div id=""blacklist-box"" class=""sidebar-blacklist"">
  <h2>Blacklisted (<a class=""wiki-link"" href=""/wiki_pages/help:blacklists"">help</a>)</h2>
  <ul id=""blacklist-list"" class=""list-bulleted""></ul>
  <a id=""disable-all-blacklists"" style=""display: none;"" href=""#"">Disable all</a>
  <a id=""re-enable-all-blacklists"" style=""display: none;"" href=""#"">Re-enable all</a>
</div>


  <section id=""tag-list"">
    <div class=""tag-list categorized-tag-list"">
    <h3 class=""copyright-tag-list"">
      Copyrights
    </h3>

    <ul class=""copyright-tag-list"">
        <li class=""tag-type-3"" data-tag-name=""iriya_no_sora_ufo_no_natsu"" data-is-deprecated=""false"">
            <a class=""wiki-link"" href=""/wiki_pages/iriya_no_sora_ufo_no_natsu?z=1"">?</a>


          <a class=""search-tag"" href=""/posts?tags=iriya_no_sora_ufo_no_natsu&amp;z=1"">iriya no sora ufo no natsu</a>

          <span class=""post-count"" title=""76"">76</span>
        </li>
    </ul>
    <h3 class=""character-tag-list"">
      Characters
    </h3>

    <ul class=""character-tag-list"">
        <li class=""tag-type-4"" data-tag-name=""iriya_kana"" data-is-deprecated=""false"">
            <a class=""wiki-link"" href=""/wiki_pages/iriya_kana?z=1"">?</a>


          <a class=""search-tag"" href=""/posts?tags=iriya_kana&amp;z=1"">iriya kana</a>

          <span class=""post-count"" title=""64"">64</span>
        </li>
    </ul>
    <h3 class=""general-tag-list"">
      General
    </h3>

    <ul class=""general-tag-list"">
        <li class=""tag-type-0"" data-tag-name=""1girl"" data-is-deprecated=""false"">
            <a class=""wiki-link"" href=""/wiki_pages/1girl?z=1"">?</a>


          <a class=""search-tag"" href=""/posts?tags=1girl&amp;z=1"">1girl</a>

          <span class=""post-count"" title=""3818937"">3.8M</span>
        </li>
        <li class=""tag-type-0"" data-tag-name="":o"" data-is-deprecated=""false"">
            <a class=""wiki-link"" href=""/wiki_pages/:o?z=1"">?</a>


          <a class=""search-tag"" href=""/posts?tags=%3Ao&amp;z=1"">:o</a>

          <span class=""post-count"" title=""125063"">125k</span>
        </li>
        <li class=""tag-type-0"" data-tag-name=""ahoge"" data-is-deprecated=""false"">
            <a class=""wiki-link"" href=""/wiki_pages/ahoge?z=1"">?</a>


          <a class=""search-tag"" href=""/posts?tags=ahoge&amp;z=1"">ahoge</a>

          <span class=""post-count"" title=""358828"">358k</span>
        </li>
        <li class=""tag-type-0"" data-tag-name=""aircraft"" data-is-deprecated=""false"">
            <a class=""wiki-link"" href=""/wiki_pages/aircraft?z=1"">?</a>


          <a class=""search-tag"" href=""/posts?tags=aircraft&amp;z=1"">aircraft</a>

          <span class=""post-count"" title=""15178"">15k</span>
        </li>
        <li class=""tag-type-0"" data-tag-name=""arm_behind_head"" data-is-deprecated=""false"">
            <a class=""wiki-link"" href=""/wiki_pages/arm_behind_head?z=1"">?</a>


          <a class=""search-tag"" href=""/posts?tags=arm_behind_head&amp;z=1"">arm behind head</a>

          <span class=""post-count"" title=""19355"">19k</span>
        </li>
        <li class=""tag-type-0"" data-tag-name=""arm_up"" data-is-deprecated=""false"">
            <a class=""wiki-link"" href=""/wiki_pages/arm_up?z=1"">?</a>


          <a class=""search-tag"" href=""/posts?tags=arm_up&amp;z=1"">arm up</a>

          <span class=""post-count"" title=""112394"">112k</span>
        </li>
        <li class=""tag-type-0"" data-tag-name=""bangs"" data-is-deprecated=""false"">
            <a class=""wiki-link"" href=""/wiki_pages/bangs?z=1"">?</a>


          <a class=""search-tag"" href=""/posts?tags=bangs&amp;z=1"">bangs</a>

          <span class=""post-count"" title=""1263373"">1.3M</span>
        </li>
        <li class=""tag-type-0"" data-tag-name=""blood"" data-is-deprecated=""false"">
            <a class=""wiki-link"" href=""/wiki_pages/blood?z=1"">?</a>


          <a class=""search-tag"" href=""/posts?tags=blood&amp;z=1"">blood</a>

          <span class=""post-count"" title=""72209"">72k</span>
        </li>
        <li class=""tag-type-0"" data-tag-name=""blush_stickers"" data-is-deprecated=""false"">
            <a class=""wiki-link"" href=""/wiki_pages/blush_stickers?z=1"">?</a>


          <a class=""search-tag"" href=""/posts?tags=blush_stickers&amp;z=1"">blush stickers</a>

          <span class=""post-count"" title=""55090"">55k</span>
        </li>
        <li class=""tag-type-0"" data-tag-name=""breasts"" data-is-deprecated=""false"">
            <a class=""wiki-link"" href=""/wiki_pages/breasts?z=1"">?</a>


          <a class=""search-tag"" href=""/posts?tags=breasts&amp;z=1"">breasts</a>

          <span class=""post-count"" title=""2052173"">2.1M</span>
        </li>
        <li class=""tag-type-0"" data-tag-name=""cloud"" data-is-deprecated=""false"">
            <a class=""wiki-link"" href=""/wiki_pages/cloud?z=1"">?</a>


          <a class=""search-tag"" href=""/posts?tags=cloud&amp;z=1"">cloud</a>

          <span class=""post-count"" title=""173717"">173k</span>
        </li>
        <li class=""tag-type-0"" data-tag-name=""day"" data-is-deprecated=""false"">
            <a class=""wiki-link"" href=""/wiki_pages/day?z=1"">?</a>


          <a class=""search-tag"" href=""/posts?tags=day&amp;z=1"">day</a>

          <span class=""post-count"" title=""216940"">216k</span>
        </li>
        <li class=""tag-type-0"" data-tag-name=""eyebrows_visible_through_hair"" data-is-deprecated=""false"">
            <a class=""wiki-link"" href=""/wiki_pages/eyebrows_visible_through_hair?z=1"">?</a>


          <a class=""search-tag"" href=""/posts?tags=eyebrows_visible_through_hair&amp;z=1"">eyebrows visible through hair</a>

          <span class=""post-count"" title=""901343"">901k</span>
        </li>
        <li class=""tag-type-0"" data-tag-name=""from_side"" data-is-deprecated=""false"">
            <a class=""wiki-link"" href=""/wiki_pages/from_side?z=1"">?</a>


          <a class=""search-tag"" href=""/posts?tags=from_side&amp;z=1"">from side</a>

          <span class=""post-count"" title=""105191"">105k</span>
        </li>
        <li class=""tag-type-0"" data-tag-name=""glint"" data-is-deprecated=""false"">
            <a class=""wiki-link"" href=""/wiki_pages/glint?z=1"">?</a>


          <a class=""search-tag"" href=""/posts?tags=glint&amp;z=1"">glint</a>

          <span class=""post-count"" title=""14284"">14k</span>
        </li>
        <li class=""tag-type-0"" data-tag-name=""head_tilt"" data-is-deprecated=""false"">
            <a class=""wiki-link"" href=""/wiki_pages/head_tilt?z=1"">?</a>


          <a class=""search-tag"" href=""/posts?tags=head_tilt&amp;z=1"">head tilt</a>

          <span class=""post-count"" title=""94367"">94k</span>
        </li>
        <li class=""tag-type-0"" data-tag-name=""long_hair"" data-is-deprecated=""false"">
            <a class=""wiki-link"" href=""/wiki_pages/long_hair?z=1"">?</a>


          <a class=""search-tag"" href=""/posts?tags=long_hair&amp;z=1"">long hair</a>

          <span class=""post-count"" title=""2682607"">2.7M</span>
        </li>
        <li class=""tag-type-0"" data-tag-name=""looking_at_viewer"" data-is-deprecated=""false"">
            <a class=""wiki-link"" href=""/wiki_pages/looking_at_viewer?z=1"">?</a>


          <a class=""search-tag"" href=""/posts?tags=looking_at_viewer&amp;z=1"">looking at viewer</a>

          <span class=""post-count"" title=""1817653"">1.8M</span>
        </li>
        <li class=""tag-type-0"" data-tag-name=""looking_back"" data-is-deprecated=""false"">
            <a class=""wiki-link"" href=""/wiki_pages/looking_back?z=1"">?</a>


          <a class=""search-tag"" href=""/posts?tags=looking_back&amp;z=1"">looking back</a>

          <span class=""post-count"" title=""189851"">189k</span>
        </li>
        <li class=""tag-type-0"" data-tag-name=""nosebleed"" data-is-deprecated=""false"">
            <a class=""wiki-link"" href=""/wiki_pages/nosebleed?z=1"">?</a>


          <a class=""search-tag"" href=""/posts?tags=nosebleed&amp;z=1"">nosebleed</a>

          <span class=""post-count"" title=""8463"">8.5k</span>
        </li>
        <li class=""tag-type-0"" data-tag-name=""oekaki"" data-is-deprecated=""false"">
            <a class=""wiki-link"" href=""/wiki_pages/oekaki?z=1"">?</a>


          <a class=""search-tag"" href=""/posts?tags=oekaki&amp;z=1"">oekaki</a>

          <span class=""post-count"" title=""16602"">16k</span>
        </li>
        <li class=""tag-type-0"" data-tag-name=""outdoors"" data-is-deprecated=""false"">
            <a class=""wiki-link"" href=""/wiki_pages/outdoors?z=1"">?</a>


          <a class=""search-tag"" href=""/posts?tags=outdoors&amp;z=1"">outdoors</a>

          <span class=""post-count"" title=""242226"">242k</span>
        </li>
        <li class=""tag-type-0"" data-tag-name=""parted_lips"" data-is-deprecated=""false"">
            <a class=""wiki-link"" href=""/wiki_pages/parted_lips?z=1"">?</a>


          <a class=""search-tag"" href=""/posts?tags=parted_lips&amp;z=1"">parted lips</a>

          <span class=""post-count"" title=""257196"">257k</span>
        </li>
        <li class=""tag-type-0"" data-tag-name=""purple_eyes"" data-is-deprecated=""false"">
            <a class=""wiki-link"" href=""/wiki_pages/purple_eyes?z=1"">?</a>


          <a class=""search-tag"" href=""/posts?tags=purple_eyes&amp;z=1"">purple eyes</a>

          <span class=""post-count"" title=""472412"">472k</span>
        </li>
        <li class=""tag-type-0"" data-tag-name=""purple_hair"" data-is-deprecated=""false"">
            <a class=""wiki-link"" href=""/wiki_pages/purple_hair?z=1"">?</a>


          <a class=""search-tag"" href=""/posts?tags=purple_hair&amp;z=1"">purple hair</a>

          <span class=""post-count"" title=""385177"">385k</span>
        </li>
        <li class=""tag-type-0"" data-tag-name=""school_uniform"" data-is-deprecated=""false"">
            <a class=""wiki-link"" href=""/wiki_pages/school_uniform?z=1"">?</a>


          <a class=""search-tag"" href=""/posts?tags=school_uniform&amp;z=1"">school uniform</a>

          <span class=""post-count"" title=""538359"">538k</span>
        </li>
        <li class=""tag-type-0"" data-tag-name=""serafuku"" data-is-deprecated=""false"">
            <a class=""wiki-link"" href=""/wiki_pages/serafuku?z=1"">?</a>


          <a class=""search-tag"" href=""/posts?tags=serafuku&amp;z=1"">serafuku</a>

          <span class=""post-count"" title=""235486"">235k</span>
        </li>
        <li class=""tag-type-0"" data-tag-name=""shirt"" data-is-deprecated=""false"">
            <a class=""wiki-link"" href=""/wiki_pages/shirt?z=1"">?</a>


          <a class=""search-tag"" href=""/posts?tags=shirt&amp;z=1"">shirt</a>

          <span class=""post-count"" title=""848496"">848k</span>
        </li>
        <li class=""tag-type-0"" data-tag-name=""short_sleeves"" data-is-deprecated=""false"">
            <a class=""wiki-link"" href=""/wiki_pages/short_sleeves?z=1"">?</a>


          <a class=""search-tag"" href=""/posts?tags=short_sleeves&amp;z=1"">short sleeves</a>

          <span class=""post-count"" title=""340414"">340k</span>
        </li>
        <li class=""tag-type-0"" data-tag-name=""sky"" data-is-deprecated=""false"">
            <a class=""wiki-link"" href=""/wiki_pages/sky?z=1"">?</a>


          <a class=""search-tag"" href=""/posts?tags=sky&amp;z=1"">sky</a>

          <span class=""post-count"" title=""243658"">243k</span>
        </li>
        <li class=""tag-type-0"" data-tag-name=""small_breasts"" data-is-deprecated=""false"">
            <a class=""wiki-link"" href=""/wiki_pages/small_breasts?z=1"">?</a>


          <a class=""search-tag"" href=""/posts?tags=small_breasts&amp;z=1"">small breasts</a>

          <span class=""post-count"" title=""288104"">288k</span>
        </li>
        <li class=""tag-type-0"" data-tag-name=""solo"" data-is-deprecated=""false"">
            <a class=""wiki-link"" href=""/wiki_pages/solo?z=1"">?</a>


          <a class=""search-tag"" href=""/posts?tags=solo&amp;z=1"">solo</a>

          <span class=""post-count"" title=""3160776"">3.2M</span>
        </li>
        <li class=""tag-type-0"" data-tag-name=""surprised"" data-is-deprecated=""false"">
            <a class=""wiki-link"" href=""/wiki_pages/surprised?z=1"">?</a>


          <a class=""search-tag"" href=""/posts?tags=surprised&amp;z=1"">surprised</a>

          <span class=""post-count"" title=""34195"">34k</span>
        </li>
        <li class=""tag-type-0"" data-tag-name=""ufo"" data-is-deprecated=""false"">
            <a class=""wiki-link"" href=""/wiki_pages/ufo?z=1"">?</a>


          <a class=""search-tag"" href=""/posts?tags=ufo&amp;z=1"">ufo</a>

          <span class=""post-count"" title=""2165"">2.2k</span>
        </li>
        <li class=""tag-type-0"" data-tag-name=""upper_body"" data-is-deprecated=""false"">
            <a class=""wiki-link"" href=""/wiki_pages/upper_body?z=1"">?</a>


          <a class=""search-tag"" href=""/posts?tags=upper_body&amp;z=1"">upper body</a>

          <span class=""post-count"" title=""365147"">365k</span>
        </li>
        <li class=""tag-type-0"" data-tag-name=""white_shirt"" data-is-deprecated=""false"">
            <a class=""wiki-link"" href=""/wiki_pages/white_shirt?z=1"">?</a>


          <a class=""search-tag"" href=""/posts?tags=white_shirt&amp;z=1"">white shirt</a>

          <span class=""post-count"" title=""357441"">357k</span>
        </li>
    </ul>
    <h3 class=""meta-tag-list"">
      Meta
    </h3>

    <ul class=""meta-tag-list"">
        <li class=""tag-type-5"" data-tag-name=""artist_request"" data-is-deprecated=""false"">
            <a class=""wiki-link"" href=""/wiki_pages/artist_request?z=1"">?</a>


          <a class=""search-tag"" href=""/posts?tags=artist_request&amp;z=1"">artist request</a>

          <span class=""post-count"" title=""93188"">93k</span>
        </li>
        <li class=""tag-type-5"" data-tag-name=""lowres"" data-is-deprecated=""false"">
            <a class=""wiki-link"" href=""/wiki_pages/lowres?z=1"">?</a>


          <a class=""search-tag"" href=""/posts?tags=lowres&amp;z=1"">lowres</a>

          <span class=""post-count"" title=""82501"">82k</span>
        </li>
        <li class=""tag-type-5"" data-tag-name=""source_request"" data-is-deprecated=""false"">
            <a class=""wiki-link"" href=""/wiki_pages/source_request?z=1"">?</a>


          <a class=""search-tag"" href=""/posts?tags=source_request&amp;z=1"">source request</a>

          <span class=""post-count"" title=""17223"">17k</span>
        </li>
        <li class=""tag-type-5"" data-tag-name=""translated"" data-is-deprecated=""false"">
            <a class=""wiki-link"" href=""/wiki_pages/translated?z=1"">?</a>


          <a class=""search-tag"" href=""/posts?tags=translated&amp;z=1"">translated</a>

          <span class=""post-count"" title=""438815"">438k</span>
        </li>
    </ul>
</div>

  </section>

  <section id=""post-information"">
    <h2>Information</h2>

    <ul>
      <li id=""post-info-id"">ID: 10</li>

      <li id=""post-info-uploader"">
        Uploader: <a class=""user user-admin  user-post-approver user-post-uploader"" data-user-id=""1"" data-user-name=""albert"" data-user-level=""50"" href=""/users/1"">albert</a>
        <a href=""/posts?tags=user%3Aalbert"">»</a>
      </li>

      <li id=""post-info-date"">
        Date: <a href=""/posts?tags=date%3A2005-05-24""><time datetime=""2005-05-24T00:24-04:00"" title=""2005-05-24 00:24:22 -0400"" class="""">about 17 years ago</time></a>
      </li>


      <li id=""post-info-size"">
        Size: <a href=""https://cdn.donmai.us/original/7c/2b/__iriya_kana_iriya_no_sora_ufo_no_natsu__7c2b4c3197d68c69488643d74815e790.png"">36.4 KB .png</a>
        (400x400)
        <a href=""/media_assets/9"">»</a>
      </li>

      <li id=""post-info-source"">Source: </li>
      <li id=""post-info-rating"">Rating: General</li>

      <li id=""post-info-score"">
        Score: <span class=""post-votes"" data-id=""10"">
    <a class=""post-upvote-link inactive-link"" href=""/login?url=%2Fposts%2F10""><svg class=""icon svg-icon upvote-icon "" role=""img"" xmlns=""http://www.w3.org/2000/svg"" viewBox=""0 0 448 512""><path fill=""currentColor"" d=""M272 480h-96c-13.3 0-24-10.7-24-24V256H48.2c-21.4 0-32.1-25.8-17-41L207 39c9.4-9.4 24.6-9.4 34 0l175.8 176c15.1 15.1 4.4 41-17 41H296v200c0 13.3-10.7 24-24 24z"" /></svg></a>

  <span class=""post-score"">
    <a rel=""nofollow"" href=""/post_votes?search%5Bpost_id%5D=10&amp;variant=compact"">2</a>
  </span>

    <a class=""post-downvote-link inactive-link"" href=""/login?url=%2Fposts%2F10""><svg class=""icon svg-icon downvote-icon "" role=""img"" xmlns=""http://www.w3.org/2000/svg"" viewBox=""0 0 448 512""><path fill=""currentColor"" d=""M176 32h96c13.3 0 24 10.7 24 24v200h103.8c21.4 0 32.1 25.8 17 41L241 473c-9.4 9.4-24.6 9.4-34 0L31.3 297c-15.1-15.1-4.4-41 17-41H152V56c0-13.3 10.7-24 24-24z"" /></svg></a>
</span>

      </li>

      <li id=""post-info-favorites"">
        Favorites:
        <span class=""post-favcount"" data-id=""10"">
          <a rel=""nofollow"" href=""/posts/10/favorites"">19</a>
</span>      </li>

      <li id=""post-info-status"">
        Status:
          Deleted

      </li>
    </ul>
  </section>

  <section id=""post-options"">
    <h2>Options</h2>

    <ul>
        <li id=""post-option-resize-to-window"">
          <a class=""image-resize-to-window-link"" data-shortcut=""z"" href=""#"">Resize to window</a>
        </li>


      <li id=""post-option-find-similar"">
        <a ref=""nofollow"" href=""/iqdb_queries?post_id=10"">Find similar</a>
      </li>

        <li id=""post-option-download"">
          <a download=""iriya kana (iriya no sora ufo no natsu) - 7c2b4c3197d68c69488643d74815e790.png"" href=""https://cdn.donmai.us/original/7c/2b/__iriya_kana_iriya_no_sora_ufo_no_natsu__7c2b4c3197d68c69488643d74815e790.png?download=1"">Download</a>
        </li>







    </ul>
  </section>

  <section id=""post-history"">
    <h2>History</h2>
    <ul>
      <li id=""post-history-tags""><a href=""/post_versions?search%5Bpost_id%5D=10"">Tags</a></li>
      <li id=""post-history-pools""><a href=""/pool_versions?search%5Bpost_id%5D=10"">Pools</a></li>
      <li id=""post-history-notes""><a href=""/note_versions?search%5Bpost_id%5D=10"">Notes</a></li>
      <li id=""post-history-moderation""><a href=""/posts/10/events"">Moderation</a></li>
      <li id=""post-history-commentary""><a href=""/artist_commentary_versions?search%5Bpost_id%5D=10"">Commentary</a></li>
      <li id=""post-history-replacements""><a href=""/post_replacements?search%5Bpost_id%5D=10"">Replacements</a></li>
    </ul>
  </section>

        </aside>

        <section id=""content"">


    <div class=""notice notice-small post-notice post-notice-deleted"">
        <p>This post was deleted for the following reason: </p>
        <ul class=""post-flag-reason list-bulleted"">
  <li>
    <span class=""prose"">Unapproved in three days after returning to moderation queue</span>


      (<time datetime=""2022-03-05T07:00-05:00"" title=""2022-03-05 07:00:05 -0500"" class="""">3 months ago</time>)
  </li>
</ul>




    </div>




  <section class=""image-container note-container"" data-id=""10"" data-tags=""1girl :o ahoge aircraft arm_behind_head arm_up artist_request bangs blood blush_stickers breasts cloud day eyebrows_visible_through_hair from_side glint head_tilt iriya_kana iriya_no_sora_ufo_no_natsu long_hair looking_at_viewer looking_back lowres nosebleed oekaki outdoors parted_lips purple_eyes purple_hair school_uniform serafuku shirt short_sleeves sky small_breasts solo source_request surprised translated ufo upper_body white_shirt"" data-rating=""g"" data-large-width=""400"" data-large-height=""400"" data-width=""400"" data-height=""400"" data-flags=""deleted"" data-score=""2"" data-uploader-id=""1"" data-source="""" data-normalized-source="""" data-file-url=""https://cdn.donmai.us/original/7c/2b/7c2b4c3197d68c69488643d74815e790.png"">      <picture>
        <source media=""(max-width: 660px)"" srcset=""https://cdn.donmai.us/original/7c/2b/__iriya_kana_iriya_no_sora_ufo_no_natsu__7c2b4c3197d68c69488643d74815e790.png"">
        <img width=""400"" height=""400"" id=""image"" class=""fit-width"" alt=""iriya kana (iriya no sora ufo no natsu)"" src=""https://cdn.donmai.us/original/7c/2b/__iriya_kana_iriya_no_sora_ufo_no_natsu__7c2b4c3197d68c69488643d74815e790.png"">
      </picture>

    <div id=""note-preview""></div>
</section>

  <section id=""mark-as-translated-section"">
    <form class=""simple_form edit_post"" id=""edit_post_10"" autocomplete=""off"" novalidate=""novalidate"" action=""/posts/10/mark_as_translated"" accept-charset=""UTF-8"" method=""post""><input type=""hidden"" name=""_method"" value=""put"" autocomplete=""off"" /><input type=""hidden"" name=""authenticity_token"" value=""-rTxMrAh1iaC_BGcjtcBto4DbKtePJI6H0pKaC_SA5_A7vdI5osFtFRLyiFaxie_renuYd0RlCVRbSWmlfCPoA"" autocomplete=""off"" />
      <div class=""input hidden post_tags_query""><input name=""tags_query"" class=""hidden"" autocomplete=""off"" type=""hidden"" /></div>
      <div class=""input hidden post_pool_id""><input name=""pool_id"" class=""hidden"" autocomplete=""off"" type=""hidden"" /></div>
      <div class=""input hidden post_favgroup_id""><input name=""favgroup_id"" class=""hidden"" autocomplete=""off"" type=""hidden"" /></div>

      <fieldset class=""inline-fieldset"">
        <div class=""input boolean optional post_check_translation""><input name=""post[check_translation]"" type=""hidden"" value=""0"" autocomplete=""off"" /><input class=""boolean optional"" type=""checkbox"" value=""1"" name=""post[check_translation]"" id=""post_check_translation"" /><label class=""boolean optional"" for=""post_check_translation"">Check translation</label></div>
        <div class=""input boolean optional post_partially_translated""><input name=""post[partially_translated]"" type=""hidden"" value=""0"" autocomplete=""off"" /><input class=""boolean optional"" type=""checkbox"" value=""1"" name=""post[partially_translated]"" id=""post_partially_translated"" /><label class=""boolean optional"" for=""post_partially_translated"">Partially translated</label></div>
      </fieldset>

      <input type=""submit"" name=""commit"" value=""Mark as translated"" data-disable-with=""Mark as translated"" />
</form>  </section>


    <ul class=""notice post-notice post-notice-search"">
    <li class=""search-navbar"" data-selected=""true"">
      <a rel=""nofollow prev"" class=""prev"" data-shortcut=""a"" href=""/posts/10/show_seq?q=status%3Aany&amp;seq=prev"">‹ prev</a>
      <span class=""search-name"">Search: <a rel=""nofollow"" href=""/posts?tags=status%3Aany"">status:any</a></span>
      <a rel=""nofollow next"" class=""next"" data-shortcut=""d"" href=""/posts/10/show_seq?q=status%3Aany&amp;seq=next"">next ›</a>
    </li>


</ul>


  <menu id=""post-sections"" class=""mb-4"">
      <li class=""active""><a href=""#comments"">Comments</a></li>

      <li><a href=""#recommended"">Recommended</a></li>

  </menu>

    <section id=""recommended"" style=""display: none;"">
      <p><em>Loading...</em></p>
    </section>

    <section id=""comments"">
      <div class=""comments-for-post"" data-post-id=""10"">

  <div class=""list-of-comments list-of-messages"">
      <article id=""comment_58055"" class=""comment message""
          x-data=""{ showThresholded: false }""
          x-bind:data-show-thresholded=""String(showThresholded)""
          data-id=""58055""
          data-post-id=""10""
          data-creator-id=""7624""
          data-updater-id=""7624""
          data-score=""2""
          data-do-not-bump-post=""false""
          data-is-deleted=""false""
          data-is-sticky=""false""
          data-is-dimmed=""false""
          data-is-thresholded=""false""
          data-is-reported=""false""
          data-is-upvoted=""false""
          data-is-downvoted=""false""
          data-show-thresholded=""false"">

  <div class=""author"">
    <div class=""author-name"">
        <a class=""user user-builder  user-post-uploader"" data-user-id=""7624"" data-user-name=""LaC"" data-user-level=""32"" href=""/users/7624"">LaC</a>
    </div>
    <a class=""message-timestamp"" rel=""nofollow"" href=""/comments/58055""><time datetime=""2007-08-20T13:32-04:00"" title=""2007-08-20 13:32:10 -0400"" class="""">almost 15 years ago</time></a>
  </div>

  <div class=""content"">
    <a class=""unhide-comment-link"" x-on:click=""showThresholded = true"" href=""javascript:void(0)"">[hidden]</a>

    <div class=""body prose"">
        <p>whut</p>



</div>
    <menu class=""mt-2"">
        <li class=""comment-votes"">
            <a class=""comment-upvote-link inactive-link"" href=""/login?url=%2Fposts%2F10""><svg class=""icon svg-icon upvote-icon "" role=""img"" xmlns=""http://www.w3.org/2000/svg"" viewBox=""0 0 448 512""><path fill=""currentColor"" d=""M272 480h-96c-13.3 0-24-10.7-24-24V256H48.2c-21.4 0-32.1-25.8-17-41L207 39c9.4-9.4 24.6-9.4 34 0l175.8 176c15.1 15.1 4.4 41-17 41H296v200c0 13.3-10.7 24-24 24z"" /></svg></a>

          <span class=""comment-score"">
              2
          </span>

            <a class=""comment-downvote-link inactive-link"" href=""/login?url=%2Fposts%2F10""><svg class=""icon svg-icon downvote-icon "" role=""img"" xmlns=""http://www.w3.org/2000/svg"" viewBox=""0 0 448 512""><path fill=""currentColor"" d=""M176 32h96c13.3 0 24 10.7 24 24v200h103.8c21.4 0 32.1 25.8 17 41L241 473c-9.4 9.4-24.6 9.4-34 0L31.3 297c-15.1-15.1-4.4-41 17-41H152V56c0-13.3 10.7-24 24-24z"" /></svg></a>
        </li>

        <li class=""comment-reply"">
            <a href=""/login?url=%2Fposts%2F10"">Reply</a>
        </li>


      <div class=""popup-menu inline-block "">
  <a class=""popup-menu-button inline-block rounded p-1"" href=""javascript:void(0)"">
      <i class=""icon fas fa-ellipsis-h ""></i>
  </a>

  <ul class=""popup-menu-content"">
      <li>          <a class=""comment-copy-id"" href=""/comments/58055"">
            <svg class=""icon svg-icon hashtag-icon "" role=""img"" xmlns=""http://www.w3.org/2000/svg"" viewBox=""0 0 448 512""><path fill=""currentColor"" d=""M416 127.1h-58.23l9.789-58.74c2.906-17.44-8.875-33.92-26.3-36.83c-17.53-2.875-33.92 8.891-36.83 26.3L292.9 127.1H197.8l9.789-58.74c2.906-17.44-8.875-33.92-26.3-36.83c-17.53-2.875-33.92 8.891-36.83 26.3L132.9 127.1H64c-17.67 0-32 14.33-32 32C32 177.7 46.33 191.1 64 191.1h58.23l-21.33 128H32c-17.67 0-32 14.33-32 32c0 17.67 14.33 31.1 32 31.1h58.23l-9.789 58.74c-2.906 17.44 8.875 33.92 26.3 36.83C108.5 479.9 110.3 480 112 480c15.36 0 28.92-11.09 31.53-26.73l11.54-69.27h95.12l-9.789 58.74c-2.906 17.44 8.875 33.92 26.3 36.83C268.5 479.9 270.3 480 272 480c15.36 0 28.92-11.09 31.53-26.73l11.54-69.27H384c17.67 0 32-14.33 32-31.1c0-17.67-14.33-32-32-32h-58.23l21.33-128H416c17.67 0 32-14.32 32-31.1C448 142.3 433.7 127.1 416 127.1zM260.9 319.1H165.8L187.1 191.1h95.12L260.9 319.1z"" /></svg> Copy ID
</a></li>
      <li>          <a class=""comment-copy-link"" href=""/comments/58055"">
            <i class=""icon fas fa-link ""></i> Copy Link
</a></li>
  </ul>
</div>
    </menu>

  </div>
</article>
<article id=""comment_1287079"" class=""comment message""
          x-data=""{ showThresholded: false }""
          x-bind:data-show-thresholded=""String(showThresholded)""
          data-id=""1287079""
          data-post-id=""10""
          data-creator-id=""423018""
          data-updater-id=""423018""
          data-score=""2""
          data-do-not-bump-post=""true""
          data-is-deleted=""false""
          data-is-sticky=""false""
          data-is-dimmed=""false""
          data-is-thresholded=""false""
          data-is-reported=""false""
          data-is-upvoted=""false""
          data-is-downvoted=""false""
          data-show-thresholded=""false"">

  <div class=""author"">
    <div class=""author-name"">
        <a class=""user user-builder "" data-user-id=""423018"" data-user-name=""Demundo"" data-user-level=""32"" href=""/users/423018"">Demundo</a>
    </div>
    <a class=""message-timestamp"" rel=""nofollow"" href=""/comments/1287079""><time datetime=""2014-07-23T03:13-04:00"" title=""2014-07-23 03:13:14 -0400"" class="""">almost 8 years ago</time></a>
  </div>

  <div class=""content"">
    <a class=""unhide-comment-link"" x-on:click=""showThresholded = true"" href=""javascript:void(0)"">[hidden]</a>

    <div class=""body prose"">
        <p>Nose bleed for a 10th post. =w=.</p>


  <p class=""fineprint edit-notice"">Updated <time datetime=""2015-05-28T01:04-04:00"" title=""2015-05-28 01:04:52 -0400"" class="""">about 7 years ago</time></p>

</div>
    <menu class=""mt-2"">
        <li class=""comment-votes"">
            <a class=""comment-upvote-link inactive-link"" href=""/login?url=%2Fposts%2F10""><svg class=""icon svg-icon upvote-icon "" role=""img"" xmlns=""http://www.w3.org/2000/svg"" viewBox=""0 0 448 512""><path fill=""currentColor"" d=""M272 480h-96c-13.3 0-24-10.7-24-24V256H48.2c-21.4 0-32.1-25.8-17-41L207 39c9.4-9.4 24.6-9.4 34 0l175.8 176c15.1 15.1 4.4 41-17 41H296v200c0 13.3-10.7 24-24 24z"" /></svg></a>

          <span class=""comment-score"">
              2
          </span>

            <a class=""comment-downvote-link inactive-link"" href=""/login?url=%2Fposts%2F10""><svg class=""icon svg-icon downvote-icon "" role=""img"" xmlns=""http://www.w3.org/2000/svg"" viewBox=""0 0 448 512""><path fill=""currentColor"" d=""M176 32h96c13.3 0 24 10.7 24 24v200h103.8c21.4 0 32.1 25.8 17 41L241 473c-9.4 9.4-24.6 9.4-34 0L31.3 297c-15.1-15.1-4.4-41 17-41H152V56c0-13.3 10.7-24 24-24z"" /></svg></a>
        </li>

        <li class=""comment-reply"">
            <a href=""/login?url=%2Fposts%2F10"">Reply</a>
        </li>


      <div class=""popup-menu inline-block "">
  <a class=""popup-menu-button inline-block rounded p-1"" href=""javascript:void(0)"">
      <i class=""icon fas fa-ellipsis-h ""></i>
  </a>

  <ul class=""popup-menu-content"">
      <li>          <a class=""comment-copy-id"" href=""/comments/1287079"">
            <svg class=""icon svg-icon hashtag-icon "" role=""img"" xmlns=""http://www.w3.org/2000/svg"" viewBox=""0 0 448 512""><path fill=""currentColor"" d=""M416 127.1h-58.23l9.789-58.74c2.906-17.44-8.875-33.92-26.3-36.83c-17.53-2.875-33.92 8.891-36.83 26.3L292.9 127.1H197.8l9.789-58.74c2.906-17.44-8.875-33.92-26.3-36.83c-17.53-2.875-33.92 8.891-36.83 26.3L132.9 127.1H64c-17.67 0-32 14.33-32 32C32 177.7 46.33 191.1 64 191.1h58.23l-21.33 128H32c-17.67 0-32 14.33-32 32c0 17.67 14.33 31.1 32 31.1h58.23l-9.789 58.74c-2.906 17.44 8.875 33.92 26.3 36.83C108.5 479.9 110.3 480 112 480c15.36 0 28.92-11.09 31.53-26.73l11.54-69.27h95.12l-9.789 58.74c-2.906 17.44 8.875 33.92 26.3 36.83C268.5 479.9 270.3 480 272 480c15.36 0 28.92-11.09 31.53-26.73l11.54-69.27H384c17.67 0 32-14.33 32-31.1c0-17.67-14.33-32-32-32h-58.23l21.33-128H416c17.67 0 32-14.32 32-31.1C448 142.3 433.7 127.1 416 127.1zM260.9 319.1H165.8L187.1 191.1h95.12L260.9 319.1z"" /></svg> Copy ID
</a></li>
      <li>          <a class=""comment-copy-link"" href=""/comments/1287079"">
            <i class=""icon fas fa-link ""></i> Copy Link
</a></li>
  </ul>
</div>
    </menu>

  </div>
</article>
<article id=""comment_2023221"" class=""comment message""
          x-data=""{ showThresholded: false }""
          x-bind:data-show-thresholded=""String(showThresholded)""
          data-id=""2023221""
          data-post-id=""10""
          data-creator-id=""538042""
          data-updater-id=""538042""
          data-score=""2""
          data-do-not-bump-post=""false""
          data-is-deleted=""false""
          data-is-sticky=""false""
          data-is-dimmed=""false""
          data-is-thresholded=""false""
          data-is-reported=""false""
          data-is-upvoted=""false""
          data-is-downvoted=""false""
          data-show-thresholded=""false"">

  <div class=""author"">
    <div class=""author-name"">
        <a class=""user user-member "" data-user-id=""538042"" data-user-name=""HydraVarania"" data-user-level=""20"" href=""/users/538042"">HydraVarania</a>
    </div>
    <a class=""message-timestamp"" rel=""nofollow"" href=""/comments/2023221""><time datetime=""2020-07-02T09:51-04:00"" title=""2020-07-02 09:51:23 -0400"" class="""">almost 2 years ago</time></a>
  </div>

  <div class=""content"">
    <a class=""unhide-comment-link"" x-on:click=""showThresholded = true"" href=""javascript:void(0)"">[hidden]</a>

    <div class=""body prose"">
        <p>Top bad we never got to know who the artist was</p>



</div>
    <menu class=""mt-2"">
        <li class=""comment-votes"">
            <a class=""comment-upvote-link inactive-link"" href=""/login?url=%2Fposts%2F10""><svg class=""icon svg-icon upvote-icon "" role=""img"" xmlns=""http://www.w3.org/2000/svg"" viewBox=""0 0 448 512""><path fill=""currentColor"" d=""M272 480h-96c-13.3 0-24-10.7-24-24V256H48.2c-21.4 0-32.1-25.8-17-41L207 39c9.4-9.4 24.6-9.4 34 0l175.8 176c15.1 15.1 4.4 41-17 41H296v200c0 13.3-10.7 24-24 24z"" /></svg></a>

          <span class=""comment-score"">
              2
          </span>

            <a class=""comment-downvote-link inactive-link"" href=""/login?url=%2Fposts%2F10""><svg class=""icon svg-icon downvote-icon "" role=""img"" xmlns=""http://www.w3.org/2000/svg"" viewBox=""0 0 448 512""><path fill=""currentColor"" d=""M176 32h96c13.3 0 24 10.7 24 24v200h103.8c21.4 0 32.1 25.8 17 41L241 473c-9.4 9.4-24.6 9.4-34 0L31.3 297c-15.1-15.1-4.4-41 17-41H152V56c0-13.3 10.7-24 24-24z"" /></svg></a>
        </li>

        <li class=""comment-reply"">
            <a href=""/login?url=%2Fposts%2F10"">Reply</a>
        </li>


      <div class=""popup-menu inline-block "">
  <a class=""popup-menu-button inline-block rounded p-1"" href=""javascript:void(0)"">
      <i class=""icon fas fa-ellipsis-h ""></i>
  </a>

  <ul class=""popup-menu-content"">
      <li>          <a class=""comment-copy-id"" href=""/comments/2023221"">
            <svg class=""icon svg-icon hashtag-icon "" role=""img"" xmlns=""http://www.w3.org/2000/svg"" viewBox=""0 0 448 512""><path fill=""currentColor"" d=""M416 127.1h-58.23l9.789-58.74c2.906-17.44-8.875-33.92-26.3-36.83c-17.53-2.875-33.92 8.891-36.83 26.3L292.9 127.1H197.8l9.789-58.74c2.906-17.44-8.875-33.92-26.3-36.83c-17.53-2.875-33.92 8.891-36.83 26.3L132.9 127.1H64c-17.67 0-32 14.33-32 32C32 177.7 46.33 191.1 64 191.1h58.23l-21.33 128H32c-17.67 0-32 14.33-32 32c0 17.67 14.33 31.1 32 31.1h58.23l-9.789 58.74c-2.906 17.44 8.875 33.92 26.3 36.83C108.5 479.9 110.3 480 112 480c15.36 0 28.92-11.09 31.53-26.73l11.54-69.27h95.12l-9.789 58.74c-2.906 17.44 8.875 33.92 26.3 36.83C268.5 479.9 270.3 480 272 480c15.36 0 28.92-11.09 31.53-26.73l11.54-69.27H384c17.67 0 32-14.33 32-31.1c0-17.67-14.33-32-32-32h-58.23l21.33-128H416c17.67 0 32-14.32 32-31.1C448 142.3 433.7 127.1 416 127.1zM260.9 319.1H165.8L187.1 191.1h95.12L260.9 319.1z"" /></svg> Copy ID
</a></li>
      <li>          <a class=""comment-copy-link"" href=""/comments/2023221"">
            <i class=""icon fas fa-link ""></i> Copy Link
</a></li>
  </ul>
</div>
    </menu>

  </div>
</article>

  </div>

</div>

    </section>

  <section id=""notes"" style=""display: none;"">
        <article data-width=""59"" data-height=""189"" data-x=""35"" data-y=""188"" data-id=""8941"" data-body=""Kanabuuu&#39;s sky"">Kanabuuu's sky</article>
  </section>


        </section>
      </div>
    </div>
  </div>

  </div>

  <div id=""tooltips"">
    <div id=""post-tooltips""></div>
    <div id=""comment-votes-tooltips""></div>
    <div id=""post-votes-tooltips""></div>
    <div id=""post-favorites-tooltips""></div>
    <div id=""user-tooltips""></div>
    <div id=""popup-menus""></div>
  </div>

  <footer id=""page-footer"" class=""text-sm flex-initial"">
    <span class=""page-footer-app-name"" title=""Running commit: 81bd86d20"">Danbooru</span>
    / <a href=""/terms_of_service"">Terms</a>
    / <a href=""/privacy"">Privacy</a>
    / <a href=""/2257"">2257</a>
    / <a href=""/upgrade"">Upgrade</a>
    / <a href=""/contact"">Contact</a>
    </span>
  </footer>
<script defer src=""https://static.cloudflareinsights.com/beacon.min.js/v652eace1692a40cfa3763df669d7439c1639079717194"" integrity=""sha512-Gi7xpJR8tSkrpF7aordPZQlW2DLtzUlZcumS8dMQjwDHEnw9I7ZLyiOj/6tZStRBGtGgN6ceN6cMH8z7etPGlw=="" data-cf-beacon='{""rayId"":""712e6a54b99c017a"",""token"":""d251ec98e50f454880fc91e7b6fa8ba8"",""version"":""2021.12.0"",""si"":100}' crossorigin=""anonymous""></script>
</body></html>
";

    const string POST_RAW_3 = @"
<!doctype html>
<html lang=""en"">
<head>
  <meta charset=""utf-8"">
  <title>touhou drawn by suzu_(susan_slr97) | Danbooru</title>

  <link rel=""icon"" href=""/favicon.ico"" sizes=""16x16"" type=""image/x-icon"">
  <link rel=""icon"" href=""/favicon.svg"" sizes=""any"" type=""image/svg+xml"">

  <link rel=""canonical"" href=""https://danbooru.donmai.us/posts/5390721"">
  <link rel=""search"" type=""application/opensearchdescription+xml"" href=""https://danbooru.donmai.us/opensearch.xml?version=2"" title=""Search posts"">

  <meta name=""csrf-param"" content=""authenticity_token"" />
<meta name=""csrf-token"" content=""7cUjiz2PBScOHjkVpVfanfqOj7mx4blzvd6yoRx2N74FI5xA6ZW4BQVUjfDsZ-PtP3TYuK9XyDILvOVurxdXXg"" />
    <meta name=""viewport"" content=""width=device-width,initial-scale=1"">
    <meta name=""blacklisted-tags"" content=""guro,scat,furry -rating:s"">

  <meta name=""autocomplete-tag-prefixes"" content=""[&quot;ch:&quot;,&quot;co:&quot;,&quot;gen:&quot;,&quot;char:&quot;,&quot;copy:&quot;,&quot;art:&quot;,&quot;meta:&quot;,&quot;general:&quot;,&quot;character:&quot;,&quot;copyright:&quot;,&quot;artist:&quot;]"">

    <script src=""/packs/js/runtime-502cc943ed047c87eac6.js""></script>
<script src=""/packs/js/20-34b75d7170810064ff25.js""></script>
<script src=""/packs/js/application-1f97225d1d2f4b3370ec.js""></script>
<script src=""/packs/js/780-9a149046694c464f5a58.js""></script>
<script src=""/packs/js/alpine-983543cca5034868c8ce.js""></script>

  <link rel=""stylesheet"" href=""/packs/css/20-1647607e.css"" />
<link rel=""stylesheet"" href=""/packs/css/application-2632588c.css"" />


  <meta name=""description"" content=""View this 1628x874 339 KB image"">
  <meta property=""og:type"" content=""website"">
  <meta property=""og:site_name"" content=""Danbooru"">
  <meta property=""og:title"" content=""touhou drawn by suzu_(susan_slr97) | Danbooru"">
  <meta property=""og:description"" content=""View this 1628x874 339 KB image"">
  <meta property=""og:url"" content=""https://danbooru.donmai.us/posts/5390721"">

    <meta name=""twitter:site"" content=""@danboorubot"">

  <meta name=""twitter:title"" content=""touhou drawn by suzu_(susan_slr97) | Danbooru"">
  <meta name=""twitter:description"" content=""View this 1628x874 339 KB image"">

  <meta name=""git-hash"" content=""81bd86d202afc2558f73d04f0683d7f806e8cdc2"">
  <meta name=""theme-color"" content=""hsl(213, 100%, 50%)"">

  <link rel=""alternate"" type=""application/atom+xml"" title=""Comments for post #5390721"" href=""https://danbooru.donmai.us/comments.atom?search%5Bpost_id%5D=5390721"" />  <meta name=""post-id"" content=""5390721"">
  <meta name=""post-has-embedded-notes"" content=""false"">

    <meta property=""og:image"" content=""https://cdn.donmai.us/sample/ee/8d/sample-ee8daff3bed3d0d2430af3e8a736e86a.jpg"">


    <meta name=""twitter:card"" content=""summary_large_image"">

      <meta name=""twitter:image"" content=""https://cdn.donmai.us/sample/ee/8d/sample-ee8daff3bed3d0d2430af3e8a736e86a.jpg"">



</head>

<body lang=""en"" class=""c-posts a-show flex flex-col"" spellcheck=""false"" data-controller=""posts"" data-action=""show"" data-layout=""sidebar"" data-current-user-ip-addr=""110.137.195.145"" data-current-user-save-data=""false"" data-current-user-id=""null"" data-current-user-name=""Anonymous"" data-current-user-level=""0"" data-current-user-level-string=""Anonymous"" data-current-user-theme=""auto"" data-current-user-comment-threshold=""-8"" data-current-user-default-image-size=""large"" data-current-user-time-zone=""Eastern Time (US &amp; Canada)"" data-current-user-per-page=""20"" data-current-user-is-banned=""false"" data-current-user-receive-email-notifications=""false"" data-current-user-new-post-navigation-layout=""true"" data-current-user-enable-private-favorites=""false"" data-current-user-style-usernames=""false"" data-current-user-show-deleted-children=""false"" data-current-user-can-approve-posts=""false"" data-current-user-can-upload-free=""false"" data-current-user-disable-categorized-saved-searches=""false"" data-current-user-disable-tagged-filenames=""false"" data-current-user-disable-mobile-gestures=""false"" data-current-user-enable-safe-mode=""false"" data-current-user-enable-desktop-mode=""false"" data-current-user-disable-post-tooltips=""false"" data-current-user-requires-verification=""false"" data-current-user-is-verified=""false"" data-current-user-show-deleted-posts=""false"" data-current-user-is-member=""false"" data-current-user-is-gold=""false"" data-current-user-is-platinum=""false"" data-current-user-is-builder=""false"" data-current-user-is-restricted=""false"" data-current-user-is-moderator=""false"" data-current-user-is-anonymous=""true"" data-current-user-is-owner=""false"" data-current-user-is-admin=""false"" data-current-user-is-approver=""false"" data-cookie-news-ticker=""null"" data-cookie-hide-upgrade-account-notice=""null"" data-cookie-hide-verify-account-notice=""null"" data-cookie-hide-dmail-notice=""null"" data-cookie-dab=""null"" data-cookie-show-relationship-previews=""null"" data-cookie-post-preview-size=""null"" data-cookie-post-preview-show-votes=""null"" data-post-id=""5390721"" data-post-created-at=""&quot;2022-05-29T11:36:15.684-04:00&quot;"" data-post-uploader-id=""782896"" data-post-score=""5"" data-post-last-comment-bumped-at=""null"" data-post-image-width=""1628"" data-post-image-height=""874"" data-post-fav-count=""3"" data-post-last-noted-at=""null"" data-post-parent-id=""null"" data-post-has-children=""false"" data-post-approver-id=""null"" data-post-tag-count-general=""21"" data-post-tag-count-artist=""1"" data-post-tag-count-character=""0"" data-post-tag-count-copyright=""1"" data-post-file-size=""346789"" data-post-up-score=""5"" data-post-down-score=""0"" data-post-is-pending=""false"" data-post-is-flagged=""false"" data-post-is-deleted=""false"" data-post-tag-count=""25"" data-post-updated-at=""&quot;2022-05-29T11:36:15.684-04:00&quot;"" data-post-is-banned=""false"" data-post-pixiv-id=""null"" data-post-last-commented-at=""null"" data-post-has-active-children=""false"" data-post-bit-flags=""0"" data-post-tag-count-meta=""2"" data-post-has-large=""true"" data-post-current-image-size=""large"">

  <header id=""top"">
    <div id=""app-name-header"" class=""font-bold font-header leading-normal inline-flex items-center gap-1"">
      <a id=""app-logo"" href=""/""><img src=""/packs/static/danbooru-logo-128x128-ea111b6658173e847734.png"" /></a>
      <a id=""app-name"" href=""/"">Danbooru</a>
    </div>

    <div id=""maintoggle"" class=""mobile-only"">
      <a href=""#""><i class=""icon fas fa-bars "" id=""maintoggle-on""></i></a>
      <a href=""#""><i class=""icon fas fa-times "" id=""maintoggle-off"" style=""display: none;""></i></a>
    </div>

    <nav id=""nav"">
      <menu id=""main-menu"" class=""main"">
          <li id=""nav-login""><a id=""nav-login-link"" rel=""nofollow"" href=""/login?url=%2Fposts%2F5390721"">Login</a></li>

        <li id=""nav-posts"" class="" current""><a id=""nav-posts-link"" href=""/posts"">Posts</a></li>
        <li id=""nav-comments""><a id=""nav-comments-link"" href=""/comments"">Comments</a></li>
        <li id=""nav-notes""><a id=""nav-notes-link"" href=""/notes"">Notes</a></li>
        <li id=""nav-artists""><a id=""nav-artists-link"" href=""/artists"">Artists</a></li>
        <li id=""nav-tags""><a id=""nav-tags-link"" href=""/tags"">Tags</a></li>
        <li id=""nav-pools""><a id=""nav-pools-link"" href=""/pools/gallery"">Pools</a></li>
        <li id=""nav-wiki""><a id=""nav-wiki-link"" href=""/wiki_pages/help:home"">Wiki</a></li>
        <li id=""nav-forum""><a id=""nav-forum-link"" href=""/forum_topics"">Forum</a></li>


        <li id=""nav-more""><a id=""nav-more-link"" href=""/static/site_map"">More »</a></li>
      </menu>

        <menu id=""subnav-menu"">
            <li id=""subnav-listing""><a id=""subnav-listing-link"" href=""/posts"">Listing</a></li>
    <li id=""subnav-upload""><a id=""subnav-upload-link"" href=""/login?url=%2Fuploads%2Fnew"">Upload</a></li>
  <li id=""subnav-hot""><a id=""subnav-hot-link"" href=""/posts?d=1&amp;tags=order%3Arank"">Hot</a></li>
  <li id=""subnav-changes""><a id=""subnav-changes-link"" href=""/post_versions"">Changes</a></li>
  <li id=""subnav-help""><a id=""subnav-help-link"" href=""/wiki_pages/help:posts"">Help</a></li>

        </menu>
    </nav>
  </header>

  <div id=""page"" class=""flex-1 mt-4"">





    <div class=""notice notice-info"" id=""notice"" style=""display: none;"">
      <span class=""prose"">.</span>
      <a href=""#"" id=""close-notice-link"">close</a>
    </div>









  <script type=""text/javascript"">
    $(function() {
      $.post(""https://isshiki.donmai.us/post_views"", {
        msg: ""IjUzOTA3MjEsOGQ2Y2ZhZjBjNTllNWIyYTlkMmEwN2UwYTJlN2E2NTAi--05c65d31da753ed86d87fc365ef909176a70590b35ed88ad08f5b7a977eac0ba""
      });
    });
  </script>

  <div id=""c-posts"">
    <div id=""a-show"">

      <div class=""sidebar-container"">
        <aside id=""sidebar"">

<section id=""search-box"">
  <h2>Search</h2>
  <form id=""search-box-form"" class=""flex"" action=""/posts"" accept-charset=""UTF-8"" method=""get"">
    <input type=""text"" name=""tags"" id=""tags"" class=""flex-auto"" data-shortcut=""q"" data-autocomplete=""tag-query"" />
    <input type=""hidden"" name=""z"" id=""z"" value=""5"" autocomplete=""off"" />
    <button id=""search-box-submit"" type=""submit""><i class=""icon fas fa-search ""></i></button>
</form></section>


  <div id=""blacklist-box"" class=""sidebar-blacklist"">
  <h2>Blacklisted (<a class=""wiki-link"" href=""/wiki_pages/help:blacklists"">help</a>)</h2>
  <ul id=""blacklist-list"" class=""list-bulleted""></ul>
  <a id=""disable-all-blacklists"" style=""display: none;"" href=""#"">Disable all</a>
  <a id=""re-enable-all-blacklists"" style=""display: none;"" href=""#"">Re-enable all</a>
</div>


  <section id=""tag-list"">
    <div class=""tag-list categorized-tag-list"">
    <h3 class=""artist-tag-list"">
      Artists
    </h3>

    <ul class=""artist-tag-list"">
        <li class=""tag-type-1"" data-tag-name=""suzu_(susan_slr97)"" data-is-deprecated=""false"">
            <a class=""wiki-link"" href=""/artists/show_or_new?name=suzu_%28susan_slr97%29&amp;z=1"">?</a>


          <a class=""search-tag"" href=""/posts?tags=suzu_%28susan_slr97%29&amp;z=1"">suzu (susan slr97)</a>

          <span class=""post-count"" title=""41"">41</span>
        </li>
    </ul>
    <h3 class=""copyright-tag-list"">
      Copyrights
    </h3>

    <ul class=""copyright-tag-list"">
        <li class=""tag-type-3"" data-tag-name=""touhou"" data-is-deprecated=""false"">
            <a class=""wiki-link"" href=""/wiki_pages/touhou?z=1"">?</a>


          <a class=""search-tag"" href=""/posts?tags=touhou&amp;z=1"">touhou</a>

          <span class=""post-count"" title=""743537"">743k</span>
        </li>
    </ul>
    <h3 class=""general-tag-list"">
      General
    </h3>

    <ul class=""general-tag-list"">
        <li class=""tag-type-0"" data-tag-name=""blue_flower"" data-is-deprecated=""false"">
            <a class=""wiki-link"" href=""/wiki_pages/blue_flower?z=1"">?</a>


          <a class=""search-tag"" href=""/posts?tags=blue_flower&amp;z=1"">blue flower</a>

          <span class=""post-count"" title=""18018"">18k</span>
        </li>
        <li class=""tag-type-0"" data-tag-name=""building"" data-is-deprecated=""false"">
            <a class=""wiki-link"" href=""/wiki_pages/building?z=1"">?</a>


          <a class=""search-tag"" href=""/posts?tags=building&amp;z=1"">building</a>

          <span class=""post-count"" title=""33636"">33k</span>
        </li>
        <li class=""tag-type-0"" data-tag-name=""bush"" data-is-deprecated=""false"">
            <a class=""wiki-link"" href=""/wiki_pages/bush?z=1"">?</a>


          <a class=""search-tag"" href=""/posts?tags=bush&amp;z=1"">bush</a>

          <span class=""post-count"" title=""9680"">9.7k</span>
        </li>
        <li class=""tag-type-0"" data-tag-name=""cloud"" data-is-deprecated=""false"">
            <a class=""wiki-link"" href=""/wiki_pages/cloud?z=1"">?</a>


          <a class=""search-tag"" href=""/posts?tags=cloud&amp;z=1"">cloud</a>

          <span class=""post-count"" title=""173753"">173k</span>
        </li>
        <li class=""tag-type-0"" data-tag-name=""day"" data-is-deprecated=""false"">
            <a class=""wiki-link"" href=""/wiki_pages/day?z=1"">?</a>


          <a class=""search-tag"" href=""/posts?tags=day&amp;z=1"">day</a>

          <span class=""post-count"" title=""216965"">216k</span>
        </li>
        <li class=""tag-type-0"" data-tag-name=""flower"" data-is-deprecated=""false"">
            <a class=""wiki-link"" href=""/wiki_pages/flower?z=1"">?</a>


          <a class=""search-tag"" href=""/posts?tags=flower&amp;z=1"">flower</a>

          <span class=""post-count"" title=""364993"">364k</span>
        </li>
        <li class=""tag-type-0"" data-tag-name=""forest"" data-is-deprecated=""false"">
            <a class=""wiki-link"" href=""/wiki_pages/forest?z=1"">?</a>


          <a class=""search-tag"" href=""/posts?tags=forest&amp;z=1"">forest</a>

          <span class=""post-count"" title=""22464"">22k</span>
        </li>
        <li class=""tag-type-0"" data-tag-name=""leaf"" data-is-deprecated=""false"">
            <a class=""wiki-link"" href=""/wiki_pages/leaf?z=1"">?</a>


          <a class=""search-tag"" href=""/posts?tags=leaf&amp;z=1"">leaf</a>

          <span class=""post-count"" title=""57532"">57k</span>
        </li>
        <li class=""tag-type-0"" data-tag-name=""mansion"" data-is-deprecated=""false"">
            <a class=""wiki-link"" href=""/wiki_pages/mansion?z=1"">?</a>


          <a class=""search-tag"" href=""/posts?tags=mansion&amp;z=1"">mansion</a>

          <span class=""post-count"" title=""466"">466</span>
        </li>
        <li class=""tag-type-0"" data-tag-name=""meadow"" data-is-deprecated=""false"">
            <a class=""wiki-link"" href=""/wiki_pages/meadow?z=1"">?</a>


          <a class=""search-tag"" href=""/posts?tags=meadow&amp;z=1"">meadow</a>

          <span class=""post-count"" title=""528"">528</span>
        </li>
        <li class=""tag-type-0"" data-tag-name=""mountainous_horizon"" data-is-deprecated=""false"">
            <a class=""wiki-link"" href=""/wiki_pages/mountainous_horizon?z=1"">?</a>


          <a class=""search-tag"" href=""/posts?tags=mountainous_horizon&amp;z=1"">mountainous horizon</a>

          <span class=""post-count"" title=""2985"">3.0k</span>
        </li>
        <li class=""tag-type-0"" data-tag-name=""nature"" data-is-deprecated=""false"">
            <a class=""wiki-link"" href=""/wiki_pages/nature?z=1"">?</a>


          <a class=""search-tag"" href=""/posts?tags=nature&amp;z=1"">nature</a>

          <span class=""post-count"" title=""31504"">31k</span>
        </li>
        <li class=""tag-type-0"" data-tag-name=""no_humans"" data-is-deprecated=""false"">
            <a class=""wiki-link"" href=""/wiki_pages/no_humans?z=1"">?</a>


          <a class=""search-tag"" href=""/posts?tags=no_humans&amp;z=1"">no humans</a>

          <span class=""post-count"" title=""78072"">78k</span>
        </li>
        <li class=""tag-type-0"" data-tag-name=""path"" data-is-deprecated=""false"">
            <a class=""wiki-link"" href=""/wiki_pages/path?z=1"">?</a>


          <a class=""search-tag"" href=""/posts?tags=path&amp;z=1"">path</a>

          <span class=""post-count"" title=""2168"">2.2k</span>
        </li>
        <li class=""tag-type-0"" data-tag-name=""rock"" data-is-deprecated=""false"">
            <a class=""wiki-link"" href=""/wiki_pages/rock?z=1"">?</a>


          <a class=""search-tag"" href=""/posts?tags=rock&amp;z=1"">rock</a>

          <span class=""post-count"" title=""15561"">15k</span>
        </li>
        <li class=""tag-type-0"" data-tag-name=""scarlet_devil_mansion"" data-is-deprecated=""false"">
            <a class=""wiki-link"" href=""/wiki_pages/scarlet_devil_mansion?z=1"">?</a>


          <a class=""search-tag"" href=""/posts?tags=scarlet_devil_mansion&amp;z=1"">scarlet devil mansion</a>

          <span class=""post-count"" title=""1584"">1.6k</span>
        </li>
        <li class=""tag-type-0"" data-tag-name=""scenery"" data-is-deprecated=""false"">
            <a class=""wiki-link"" href=""/wiki_pages/scenery?z=1"">?</a>


          <a class=""search-tag"" href=""/posts?tags=scenery&amp;z=1"">scenery</a>

          <span class=""post-count"" title=""30073"">30k</span>
        </li>
        <li class=""tag-type-0"" data-tag-name=""sky"" data-is-deprecated=""false"">
            <a class=""wiki-link"" href=""/wiki_pages/sky?z=1"">?</a>


          <a class=""search-tag"" href=""/posts?tags=sky&amp;z=1"">sky</a>

          <span class=""post-count"" title=""243716"">243k</span>
        </li>
        <li class=""tag-type-0"" data-tag-name=""tree"" data-is-deprecated=""false"">
            <a class=""wiki-link"" href=""/wiki_pages/tree?z=1"">?</a>


          <a class=""search-tag"" href=""/posts?tags=tree&amp;z=1"">tree</a>

          <span class=""post-count"" title=""100807"">100k</span>
        </li>
        <li class=""tag-type-0"" data-tag-name=""white_flower"" data-is-deprecated=""false"">
            <a class=""wiki-link"" href=""/wiki_pages/white_flower?z=1"">?</a>


          <a class=""search-tag"" href=""/posts?tags=white_flower&amp;z=1"">white flower</a>

          <span class=""post-count"" title=""35050"">35k</span>
        </li>
        <li class=""tag-type-0"" data-tag-name=""yellow_flower"" data-is-deprecated=""false"">
            <a class=""wiki-link"" href=""/wiki_pages/yellow_flower?z=1"">?</a>


          <a class=""search-tag"" href=""/posts?tags=yellow_flower&amp;z=1"">yellow flower</a>

          <span class=""post-count"" title=""12301"">12k</span>
        </li>
    </ul>
    <h3 class=""meta-tag-list"">
      Meta
    </h3>

    <ul class=""meta-tag-list"">
        <li class=""tag-type-5"" data-tag-name=""commentary"" data-is-deprecated=""false"">
            <a class=""wiki-link"" href=""/wiki_pages/commentary?z=1"">?</a>


          <a class=""search-tag"" href=""/posts?tags=commentary&amp;z=1"">commentary</a>

          <span class=""post-count"" title=""530601"">530k</span>
        </li>
        <li class=""tag-type-5"" data-tag-name=""highres"" data-is-deprecated=""false"">
            <a class=""wiki-link"" href=""/wiki_pages/highres?z=1"">?</a>


          <a class=""search-tag"" href=""/posts?tags=highres&amp;z=1"">highres</a>

          <span class=""post-count"" title=""2686766"">2.7M</span>
        </li>
    </ul>
</div>

  </section>

  <section id=""post-information"">
    <h2>Information</h2>

    <ul>
      <li id=""post-info-id"">ID: 5390721</li>

      <li id=""post-info-uploader"">
        Uploader: <a class=""user user-builder  user-post-uploader"" data-user-id=""782896"" data-user-name=""Ardouru"" data-user-level=""32"" href=""/users/782896"">Ardouru</a>
        <a href=""/posts?tags=user%3AArdouru"">»</a>
      </li>

      <li id=""post-info-date"">
        Date: <a href=""/posts?tags=date%3A2022-05-29""><time datetime=""2022-05-29T11:36-04:00"" title=""2022-05-29 11:36:15 -0400"" class="""">about 1 hour ago</time></a>
      </li>


      <li id=""post-info-size"">
        Size: <a href=""https://cdn.donmai.us/original/ee/8d/__touhou_drawn_by_suzu_susan_slr97__ee8daff3bed3d0d2430af3e8a736e86a.jpg"">339 KB .jpg</a>
        (1628x874)
        <a href=""/media_assets/6033620"">»</a>
      </li>

      <li id=""post-info-source"">Source: <a rel=""external noreferrer nofollow"" href=""https://twitter.com/swing94139193/status/1530521038424928257"">twitter.com/swing94139193/status/1530521038424928257</a>&nbsp;<a rel=""external noreferrer nofollow"" href=""https://twitter.com/swing94139193/status/1530521038424928257"">»</a></li>
      <li id=""post-info-rating"">Rating: General</li>

      <li id=""post-info-score"">
        Score: <span class=""post-votes"" data-id=""5390721"">
    <a class=""post-upvote-link inactive-link"" href=""/login?url=%2Fposts%2F5390721""><svg class=""icon svg-icon upvote-icon "" role=""img"" xmlns=""http://www.w3.org/2000/svg"" viewBox=""0 0 448 512""><path fill=""currentColor"" d=""M272 480h-96c-13.3 0-24-10.7-24-24V256H48.2c-21.4 0-32.1-25.8-17-41L207 39c9.4-9.4 24.6-9.4 34 0l175.8 176c15.1 15.1 4.4 41-17 41H296v200c0 13.3-10.7 24-24 24z"" /></svg></a>

  <span class=""post-score"">
    <a rel=""nofollow"" href=""/post_votes?search%5Bpost_id%5D=5390721&amp;variant=compact"">5</a>
  </span>

    <a class=""post-downvote-link inactive-link"" href=""/login?url=%2Fposts%2F5390721""><svg class=""icon svg-icon downvote-icon "" role=""img"" xmlns=""http://www.w3.org/2000/svg"" viewBox=""0 0 448 512""><path fill=""currentColor"" d=""M176 32h96c13.3 0 24 10.7 24 24v200h103.8c21.4 0 32.1 25.8 17 41L241 473c-9.4 9.4-24.6 9.4-34 0L31.3 297c-15.1-15.1-4.4-41 17-41H152V56c0-13.3 10.7-24 24-24z"" /></svg></a>
</span>

      </li>

      <li id=""post-info-favorites"">
        Favorites:
        <span class=""post-favcount"" data-id=""5390721"">
          <a rel=""nofollow"" href=""/posts/5390721/favorites"">3</a>
</span>      </li>

      <li id=""post-info-status"">
        Status:
          Active

      </li>
    </ul>
  </section>

  <section id=""post-options"">
    <h2>Options</h2>

    <ul>
        <li id=""post-option-resize-to-window"">
          <a class=""image-resize-to-window-link"" data-shortcut=""z"" href=""#"">Resize to window</a>
        </li>

        <li id=""post-option-view-large"">
          <a class=""image-view-large-link"" href=""https://cdn.donmai.us/sample/ee/8d/__touhou_drawn_by_suzu_susan_slr97__sample-ee8daff3bed3d0d2430af3e8a736e86a.jpg"">View smaller</a>
        </li>

        <li id=""post-option-view-original"">
          <a class=""image-view-original-link"" href=""https://cdn.donmai.us/original/ee/8d/__touhou_drawn_by_suzu_susan_slr97__ee8daff3bed3d0d2430af3e8a736e86a.jpg"">View original</a>
        </li>

      <li id=""post-option-find-similar"">
        <a ref=""nofollow"" href=""/iqdb_queries?post_id=5390721"">Find similar</a>
      </li>

        <li id=""post-option-download"">
          <a download=""touhou drawn by suzu_(susan_slr97) - ee8daff3bed3d0d2430af3e8a736e86a.jpg"" href=""https://cdn.donmai.us/original/ee/8d/__touhou_drawn_by_suzu_susan_slr97__ee8daff3bed3d0d2430af3e8a736e86a.jpg?download=1"">Download</a>
        </li>







    </ul>
  </section>

  <section id=""post-history"">
    <h2>History</h2>
    <ul>
      <li id=""post-history-tags""><a href=""/post_versions?search%5Bpost_id%5D=5390721"">Tags</a></li>
      <li id=""post-history-pools""><a href=""/pool_versions?search%5Bpost_id%5D=5390721"">Pools</a></li>
      <li id=""post-history-notes""><a href=""/note_versions?search%5Bpost_id%5D=5390721"">Notes</a></li>
      <li id=""post-history-moderation""><a href=""/posts/5390721/events"">Moderation</a></li>
      <li id=""post-history-commentary""><a href=""/artist_commentary_versions?search%5Bpost_id%5D=5390721"">Commentary</a></li>
      <li id=""post-history-replacements""><a href=""/post_replacements?search%5Bpost_id%5D=5390721"">Replacements</a></li>
    </ul>
  </section>

        </aside>

        <section id=""content"">





    <div class=""notice notice-small post-notice post-notice-resized"" id=""image-resize-notice"">
      Resized to 52% of original (<a class=""image-view-original-link"" href=""https://cdn.donmai.us/original/ee/8d/__touhou_drawn_by_suzu_susan_slr97__ee8daff3bed3d0d2430af3e8a736e86a.jpg"">view original</a>)
    </div>

  <section class=""image-container note-container"" data-id=""5390721"" data-tags=""blue_flower building bush cloud commentary day flower forest highres leaf mansion meadow mountainous_horizon nature no_humans path rock scarlet_devil_mansion scenery sky suzu_(susan_slr97) touhou tree white_flower yellow_flower"" data-rating=""g"" data-large-width=""850"" data-large-height=""456"" data-width=""1628"" data-height=""874"" data-flags="""" data-score=""5"" data-uploader-id=""782896"" data-source=""https://twitter.com/swing94139193/status/1530521038424928257"" data-normalized-source=""https://twitter.com/swing94139193/status/1530521038424928257"" data-file-url=""https://cdn.donmai.us/original/ee/8d/ee8daff3bed3d0d2430af3e8a736e86a.jpg"">      <picture>
        <source media=""(max-width: 660px)"" srcset=""https://cdn.donmai.us/sample/ee/8d/__touhou_drawn_by_suzu_susan_slr97__sample-ee8daff3bed3d0d2430af3e8a736e86a.jpg"">
        <img width=""850"" height=""456"" id=""image"" class=""fit-width"" alt=""touhou drawn by suzu_(susan_slr97)"" src=""https://cdn.donmai.us/sample/ee/8d/__touhou_drawn_by_suzu_susan_slr97__sample-ee8daff3bed3d0d2430af3e8a736e86a.jpg"">
      </picture>

    <div id=""note-preview""></div>
</section>

  <section id=""mark-as-translated-section"">
    <form class=""simple_form edit_post"" id=""edit_post_5390721"" autocomplete=""off"" novalidate=""novalidate"" action=""/posts/5390721/mark_as_translated"" accept-charset=""UTF-8"" method=""post""><input type=""hidden"" name=""_method"" value=""put"" autocomplete=""off"" /><input type=""hidden"" name=""authenticity_token"" value=""6h5uPm-EH-5ZQJ4FosTDXYamE7zGr_LM_mrUWkyIlJaiD4sJvb6QuWYja_2KLckz6fJeD8Jmt06abUNaSdMzLQ"" autocomplete=""off"" />
      <div class=""input hidden post_tags_query""><input name=""tags_query"" class=""hidden"" autocomplete=""off"" type=""hidden"" /></div>
      <div class=""input hidden post_pool_id""><input name=""pool_id"" class=""hidden"" autocomplete=""off"" type=""hidden"" /></div>
      <div class=""input hidden post_favgroup_id""><input name=""favgroup_id"" class=""hidden"" autocomplete=""off"" type=""hidden"" /></div>

      <fieldset class=""inline-fieldset"">
        <div class=""input boolean optional post_check_translation""><input name=""post[check_translation]"" type=""hidden"" value=""0"" autocomplete=""off"" /><input class=""boolean optional"" type=""checkbox"" value=""1"" name=""post[check_translation]"" id=""post_check_translation"" /><label class=""boolean optional"" for=""post_check_translation"">Check translation</label></div>
        <div class=""input boolean optional post_partially_translated""><input name=""post[partially_translated]"" type=""hidden"" value=""0"" autocomplete=""off"" /><input class=""boolean optional"" type=""checkbox"" value=""1"" name=""post[partially_translated]"" id=""post_partially_translated"" /><label class=""boolean optional"" for=""post_partially_translated"">Partially translated</label></div>
      </fieldset>

      <input type=""submit"" name=""commit"" value=""Mark as translated"" data-disable-with=""Mark as translated"" />
</form>  </section>

    <div id=""artist-commentary"">
      <h2>Artist's commentary</h2>

<menu id=""commentary-sections"">
    <li><a href=""#original"">Original</a></li> |
    <li class=""active""><a href=""#translated"">Translated</a></li>
</menu>

  <section id=""original-artist-commentary"" style=""display: none"">
    <h3><span class=""prose ""></span></h3>
    <div class=""prose ""><p>紅魔館</p></div>
</section>
  <section id=""translated-artist-commentary"">
      <h3><span class=""prose inactive""></span></h3>

      <div class=""prose ""><p>Scarlet Devil Mansion</p></div>
  </section>

    </div>

    <ul class=""notice post-notice post-notice-search"">
    <li class=""search-navbar"" data-selected=""true"">
      <a rel=""nofollow prev"" class=""prev"" data-shortcut=""a"" href=""/posts/5390721/show_seq?q=status%3Aany&amp;seq=prev"">‹ prev</a>
      <span class=""search-name"">Search: <a rel=""nofollow"" href=""/posts?tags=status%3Aany"">status:any</a></span>
      <a rel=""nofollow next"" class=""next"" data-shortcut=""d"" href=""/posts/5390721/show_seq?q=status%3Aany&amp;seq=next"">next ›</a>
    </li>


</ul>


  <menu id=""post-sections"" class=""mb-4"">
      <li class=""active""><a href=""#comments"">Comments</a></li>


  </menu>


    <section id=""comments"">
      <div class=""comments-for-post"" data-post-id=""5390721"">

  <div class=""list-of-comments list-of-messages"">
      <p>There are no comments.</p>
  </div>

</div>

    </section>

  <section id=""notes"" style=""display: none;"">
  </section>


        </section>
      </div>
    </div>
  </div>

  </div>

  <div id=""tooltips"">
    <div id=""post-tooltips""></div>
    <div id=""comment-votes-tooltips""></div>
    <div id=""post-votes-tooltips""></div>
    <div id=""post-favorites-tooltips""></div>
    <div id=""user-tooltips""></div>
    <div id=""popup-menus""></div>
  </div>

  <footer id=""page-footer"" class=""text-sm flex-initial"">
    <span class=""page-footer-app-name"" title=""Running commit: 81bd86d20"">Danbooru</span>
    / <a href=""/terms_of_service"">Terms</a>
    / <a href=""/privacy"">Privacy</a>
    / <a href=""/2257"">2257</a>
    / <a href=""/upgrade"">Upgrade</a>
    / <a href=""/contact"">Contact</a>
    </span>
  </footer>
<script defer src=""https://static.cloudflareinsights.com/beacon.min.js/v652eace1692a40cfa3763df669d7439c1639079717194"" integrity=""sha512-Gi7xpJR8tSkrpF7aordPZQlW2DLtzUlZcumS8dMQjwDHEnw9I7ZLyiOj/6tZStRBGtGgN6ceN6cMH8z7etPGlw=="" data-cf-beacon='{""rayId"":""713095af8e36017a"",""token"":""d251ec98e50f454880fc91e7b6fa8ba8"",""version"":""2021.12.0"",""si"":100}' crossorigin=""anonymous""></script>
</body></html>
";

    const string RAW_INVALID = @"
<!doctype html>
<html lang=""en"">
<head>
  <meta charset=""utf-8"">
  <title>Error: ActiveRecord::RecordNotFound | Danbooru</title>

  <link rel=""icon"" href=""/favicon.ico"" sizes=""16x16"" type=""image/x-icon"">
  <link rel=""icon"" href=""/favicon.svg"" sizes=""any"" type=""image/svg+xml"">

  <link rel=""canonical"" href=""https://danbooru.donmai.us/posts/x"">
  <link rel=""search"" type=""application/opensearchdescription+xml"" href=""https://danbooru.donmai.us/opensearch.xml?version=2"" title=""Search posts"">

  <meta name=""csrf-param"" content=""authenticity_token"" />
<meta name=""csrf-token"" content=""SnqxXEQCBLefEjorD6pZAtX0Q7O1MwBdRr0ljF2lOIX9Spr0OKZKzXRCRC2ye4DsSLMHZvHnOvHN8S5rqoOaPw"" />
    <meta name=""viewport"" content=""width=device-width,initial-scale=1"">
    <meta name=""blacklisted-tags"" content=""guro,scat,furry -rating:s"">

  <meta name=""autocomplete-tag-prefixes"" content=""[&quot;ch:&quot;,&quot;co:&quot;,&quot;gen:&quot;,&quot;char:&quot;,&quot;copy:&quot;,&quot;art:&quot;,&quot;meta:&quot;,&quot;general:&quot;,&quot;character:&quot;,&quot;copyright:&quot;,&quot;artist:&quot;]"">

    <script src=""/packs/js/runtime-502cc943ed047c87eac6.js""></script>
<script src=""/packs/js/20-34b75d7170810064ff25.js""></script>
<script src=""/packs/js/application-1f97225d1d2f4b3370ec.js""></script>
<script src=""/packs/js/780-9a149046694c464f5a58.js""></script>
<script src=""/packs/js/alpine-983543cca5034868c8ce.js""></script>

  <link rel=""stylesheet"" href=""/packs/css/20-1647607e.css"" />
<link rel=""stylesheet"" href=""/packs/css/application-2632588c.css"" />


  <meta name=""description"">
  <meta property=""og:type"" content=""website"">
  <meta property=""og:site_name"" content=""Danbooru"">
  <meta property=""og:title"" content=""Error: ActiveRecord::RecordNotFound | Danbooru"">
  <meta property=""og:description"">
  <meta property=""og:url"" content=""https://danbooru.donmai.us/posts/x"">

    <meta name=""twitter:site"" content=""@danboorubot"">

  <meta name=""twitter:title"" content=""Error: ActiveRecord::RecordNotFound | Danbooru"">
  <meta name=""twitter:description"">

  <meta name=""git-hash"" content=""81bd86d202afc2558f73d04f0683d7f806e8cdc2"">
  <meta name=""theme-color"" content=""hsl(213, 100%, 50%)"">



</head>

<body lang=""en"" class=""c-static a-error flex flex-col"" spellcheck=""false"" data-controller=""static"" data-action=""error"" data-current-user-ip-addr=""110.137.195.145"" data-current-user-save-data=""false"" data-current-user-id=""null"" data-current-user-name=""Anonymous"" data-current-user-level=""0"" data-current-user-level-string=""Anonymous"" data-current-user-theme=""auto"" data-current-user-comment-threshold=""-8"" data-current-user-default-image-size=""large"" data-current-user-time-zone=""Eastern Time (US &amp; Canada)"" data-current-user-per-page=""20"" data-current-user-is-banned=""false"" data-current-user-receive-email-notifications=""false"" data-current-user-new-post-navigation-layout=""true"" data-current-user-enable-private-favorites=""false"" data-current-user-style-usernames=""false"" data-current-user-show-deleted-children=""false"" data-current-user-can-approve-posts=""false"" data-current-user-can-upload-free=""false"" data-current-user-disable-categorized-saved-searches=""false"" data-current-user-disable-tagged-filenames=""false"" data-current-user-disable-mobile-gestures=""false"" data-current-user-enable-safe-mode=""false"" data-current-user-enable-desktop-mode=""false"" data-current-user-disable-post-tooltips=""false"" data-current-user-requires-verification=""false"" data-current-user-is-verified=""false"" data-current-user-show-deleted-posts=""false"" data-current-user-is-anonymous=""true"" data-current-user-is-member=""false"" data-current-user-is-gold=""false"" data-current-user-is-platinum=""false"" data-current-user-is-builder=""false"" data-current-user-is-moderator=""false"" data-current-user-is-restricted=""false"" data-current-user-is-admin=""false"" data-current-user-is-owner=""false"" data-current-user-is-approver=""false"" data-cookie-news-ticker=""null"" data-cookie-hide-upgrade-account-notice=""null"" data-cookie-hide-verify-account-notice=""null"" data-cookie-hide-dmail-notice=""null"" data-cookie-dab=""null"" data-cookie-show-relationship-previews=""null"" data-cookie-post-preview-size=""null"" data-cookie-post-preview-show-votes=""null"">

  <header id=""top"">
    <div id=""app-name-header"" class=""font-bold font-header leading-normal inline-flex items-center gap-1"">
      <a id=""app-logo"" href=""/""><img src=""/packs/static/danbooru-logo-128x128-ea111b6658173e847734.png"" /></a>
      <a id=""app-name"" href=""/"">Danbooru</a>
    </div>

    <div id=""maintoggle"" class=""mobile-only"">
      <a href=""#""><i class=""icon fas fa-bars "" id=""maintoggle-on""></i></a>
      <a href=""#""><i class=""icon fas fa-times "" id=""maintoggle-off"" style=""display: none;""></i></a>
    </div>

    <nav id=""nav"">
      <menu id=""main-menu"" class=""main"">
          <li id=""nav-login""><a id=""nav-login-link"" rel=""nofollow"" href=""/login?url=%2Fposts%2Fx"">Login</a></li>

        <li id=""nav-posts"" class="" current""><a id=""nav-posts-link"" href=""/posts"">Posts</a></li>
        <li id=""nav-comments""><a id=""nav-comments-link"" href=""/comments"">Comments</a></li>
        <li id=""nav-notes""><a id=""nav-notes-link"" href=""/notes"">Notes</a></li>
        <li id=""nav-artists""><a id=""nav-artists-link"" href=""/artists"">Artists</a></li>
        <li id=""nav-tags""><a id=""nav-tags-link"" href=""/tags"">Tags</a></li>
        <li id=""nav-pools""><a id=""nav-pools-link"" href=""/pools/gallery"">Pools</a></li>
        <li id=""nav-wiki""><a id=""nav-wiki-link"" href=""/wiki_pages/help:home"">Wiki</a></li>
        <li id=""nav-forum""><a id=""nav-forum-link"" href=""/forum_topics"">Forum</a></li>


        <li id=""nav-more""><a id=""nav-more-link"" href=""/static/site_map"">More »</a></li>
      </menu>

    </nav>
  </header>

  <div id=""page"" class=""flex-1 mt-4"">





    <div class=""notice notice-info"" id=""notice"" style=""display: none;"">
      <span class=""prose"">.</span>
      <a href=""#"" id=""close-notice-link"">close</a>
    </div>


<h1>Error</h1>

  <p>Error: That record was not found.</p>

  </div>

  <div id=""tooltips"">
    <div id=""post-tooltips""></div>
    <div id=""comment-votes-tooltips""></div>
    <div id=""post-votes-tooltips""></div>
    <div id=""post-favorites-tooltips""></div>
    <div id=""user-tooltips""></div>
    <div id=""popup-menus""></div>
  </div>

  <footer id=""page-footer"" class=""text-sm flex-initial"">
    <span class=""page-footer-app-name"" title=""Running commit: 81bd86d20"">Danbooru</span>
    / <a href=""/terms_of_service"">Terms</a>
    / <a href=""/privacy"">Privacy</a>
    / <a href=""/2257"">2257</a>
    / <a href=""/upgrade"">Upgrade</a>
    / <a href=""/contact"">Contact</a>
    </span>
  </footer>
<script defer src=""https://static.cloudflareinsights.com/beacon.min.js/v652eace1692a40cfa3763df669d7439c1639079717194"" integrity=""sha512-Gi7xpJR8tSkrpF7aordPZQlW2DLtzUlZcumS8dMQjwDHEnw9I7ZLyiOj/6tZStRBGtGgN6ceN6cMH8z7etPGlw=="" data-cf-beacon='{""rayId"":""71309fefffd0017a"",""token"":""d251ec98e50f454880fc91e7b6fa8ba8"",""version"":""2021.12.0"",""si"":100}' crossorigin=""anonymous""></script>
</body></html>
";

    const string POSTS_RAW_1 = @"
<!doctype html>
<html lang=""en"">
<head>
  <meta charset=""utf-8"">
  <title>Kafuuin Hiiro | Danbooru</title>

  <link rel=""icon"" href=""/favicon.ico"" sizes=""16x16"" type=""image/x-icon"">
  <link rel=""icon"" href=""/favicon.svg"" sizes=""any"" type=""image/svg+xml"">

  <link rel=""canonical"" href=""https://danbooru.donmai.us/posts?tags=kafuuin_hiiro"">
  <link rel=""search"" type=""application/opensearchdescription+xml"" href=""https://danbooru.donmai.us/opensearch.xml?version=2"" title=""Search posts"">

  <meta name=""csrf-param"" content=""authenticity_token"" />
<meta name=""csrf-token"" content=""fT5dn0da7-nLtkw-QgQCkZvggs_YGP5yAz9QEdpDsKgCnYYba3Ul7hWsOfjtPwEgT2bBzq9SLOHNuk8sd7J9tw"" />
    <meta name=""viewport"" content=""width=device-width,initial-scale=1"">
    <meta name=""blacklisted-tags"" content=""guro,scat,furry -rating:s"">

  <meta name=""autocomplete-tag-prefixes"" content=""[&quot;ch:&quot;,&quot;co:&quot;,&quot;gen:&quot;,&quot;char:&quot;,&quot;copy:&quot;,&quot;art:&quot;,&quot;meta:&quot;,&quot;general:&quot;,&quot;character:&quot;,&quot;copyright:&quot;,&quot;artist:&quot;]"">

    <script src=""/packs/js/runtime-502cc943ed047c87eac6.js""></script>
<script src=""/packs/js/20-34b75d7170810064ff25.js""></script>
<script src=""/packs/js/application-1f97225d1d2f4b3370ec.js""></script>
<script src=""/packs/js/780-9a149046694c464f5a58.js""></script>
<script src=""/packs/js/alpine-983543cca5034868c8ce.js""></script>

  <link rel=""stylesheet"" href=""/packs/css/20-1647607e.css"" />
<link rel=""stylesheet"" href=""/packs/css/application-2632588c.css"" />


  <meta name=""description"" content=""See 2 Kafuuin Hiiro images on Danbooru. "">
  <meta property=""og:type"" content=""website"">
  <meta property=""og:site_name"" content=""Danbooru"">
  <meta property=""og:title"" content=""Kafuuin Hiiro | Danbooru"">
  <meta property=""og:description"" content=""See 2 Kafuuin Hiiro images on Danbooru. "">
  <meta property=""og:url"" content=""https://danbooru.donmai.us/posts?tags=kafuuin_hiiro"">

    <meta name=""twitter:site"" content=""@danboorubot"">

  <meta name=""twitter:title"" content=""Kafuuin Hiiro | Danbooru"">
  <meta name=""twitter:description"" content=""See 2 Kafuuin Hiiro images on Danbooru. "">

  <meta name=""git-hash"" content=""81bd86d202afc2558f73d04f0683d7f806e8cdc2"">
  <meta name=""theme-color"" content=""hsl(213, 100%, 50%)"">

  <link rel=""alternate"" type=""application/atom+xml"" title=""Posts: Kafuuin Hiiro"" href=""https://danbooru.donmai.us/posts.atom?tags=kafuuin_hiiro"" />



    <meta property=""og:image"" content=""https://cdn.donmai.us/sample/52/16/sample-52166402f523aaa96bb6e26e7d22d8e6.jpg"">
    <meta name=""twitter:image"" content=""https://cdn.donmai.us/sample/52/16/sample-52166402f523aaa96bb6e26e7d22d8e6.jpg"">
    <meta name=""twitter:card"" content=""summary_large_image"">


</head>

<body lang=""en"" class=""c-posts a-index flex flex-col"" spellcheck=""false"" data-controller=""posts"" data-action=""index"" data-layout=""sidebar"" data-current-user-ip-addr=""180.252.157.228"" data-current-user-save-data=""false"" data-current-user-id=""null"" data-current-user-name=""Anonymous"" data-current-user-level=""0"" data-current-user-level-string=""Anonymous"" data-current-user-theme=""auto"" data-current-user-comment-threshold=""-8"" data-current-user-default-image-size=""large"" data-current-user-time-zone=""Eastern Time (US &amp; Canada)"" data-current-user-per-page=""20"" data-current-user-is-banned=""false"" data-current-user-receive-email-notifications=""false"" data-current-user-new-post-navigation-layout=""true"" data-current-user-enable-private-favorites=""false"" data-current-user-style-usernames=""false"" data-current-user-show-deleted-children=""false"" data-current-user-can-approve-posts=""false"" data-current-user-can-upload-free=""false"" data-current-user-disable-categorized-saved-searches=""false"" data-current-user-disable-tagged-filenames=""false"" data-current-user-disable-mobile-gestures=""false"" data-current-user-enable-safe-mode=""false"" data-current-user-enable-desktop-mode=""false"" data-current-user-disable-post-tooltips=""false"" data-current-user-requires-verification=""false"" data-current-user-is-verified=""false"" data-current-user-show-deleted-posts=""false"" data-current-user-is-anonymous=""true"" data-current-user-is-member=""false"" data-current-user-is-gold=""false"" data-current-user-is-platinum=""false"" data-current-user-is-builder=""false"" data-current-user-is-moderator=""false"" data-current-user-is-restricted=""false"" data-current-user-is-admin=""false"" data-current-user-is-owner=""false"" data-current-user-is-approver=""false"" data-cookie-news-ticker=""null"" data-cookie-hide-upgrade-account-notice=""null"" data-cookie-hide-verify-account-notice=""null"" data-cookie-hide-dmail-notice=""null"" data-cookie-dab=""null"" data-cookie-show-relationship-previews=""null"" data-cookie-post-preview-size=""null"" data-cookie-post-preview-show-votes=""null"">

  <header id=""top"">
    <div id=""app-name-header"" class=""font-bold font-header leading-normal inline-flex items-center gap-1"">
      <a id=""app-logo"" href=""/""><img src=""/packs/static/danbooru-logo-128x128-ea111b6658173e847734.png"" /></a>
      <a id=""app-name"" href=""/"">Danbooru</a>
    </div>

    <div id=""maintoggle"" class=""mobile-only"">
      <a href=""#""><i class=""icon fas fa-bars "" id=""maintoggle-on""></i></a>
      <a href=""#""><i class=""icon fas fa-times "" id=""maintoggle-off"" style=""display: none;""></i></a>
    </div>

    <nav id=""nav"">
      <menu id=""main-menu"" class=""main"">
          <li id=""nav-login""><a id=""nav-login-link"" rel=""nofollow"" href=""/login?url=%2Fposts%3Ftags%3Dkafuuin_hiiro"">Login</a></li>

        <li id=""nav-posts"" class="" current""><a id=""nav-posts-link"" href=""/posts"">Posts</a></li>
        <li id=""nav-comments""><a id=""nav-comments-link"" href=""/comments"">Comments</a></li>
        <li id=""nav-notes""><a id=""nav-notes-link"" href=""/notes"">Notes</a></li>
        <li id=""nav-artists""><a id=""nav-artists-link"" href=""/artists"">Artists</a></li>
        <li id=""nav-tags""><a id=""nav-tags-link"" href=""/tags"">Tags</a></li>
        <li id=""nav-pools""><a id=""nav-pools-link"" href=""/pools/gallery"">Pools</a></li>
        <li id=""nav-wiki""><a id=""nav-wiki-link"" href=""/wiki_pages/help:home"">Wiki</a></li>
        <li id=""nav-forum""><a id=""nav-forum-link"" href=""/forum_topics"">Forum</a></li>


        <li id=""nav-more""><a id=""nav-more-link"" href=""/static/site_map"">More »</a></li>
      </menu>

        <menu id=""subnav-menu"">
            <li id=""subnav-listing""><a id=""subnav-listing-link"" href=""/posts"">Listing</a></li>
    <li id=""subnav-upload""><a id=""subnav-upload-link"" href=""/login?url=%2Fuploads%2Fnew"">Upload</a></li>
  <li id=""subnav-hot""><a id=""subnav-hot-link"" href=""/posts?d=1&amp;tags=order%3Arank"">Hot</a></li>
  <li id=""subnav-changes""><a id=""subnav-changes-link"" href=""/post_versions"">Changes</a></li>
  <li id=""subnav-help""><a id=""subnav-help-link"" href=""/wiki_pages/help:posts"">Help</a></li>

        </menu>
    </nav>
  </header>

  <div id=""page"" class=""flex-1 mt-4"">





    <div class=""notice notice-info"" id=""notice"" style=""display: none;"">
      <span class=""prose"">.</span>
      <a href=""#"" id=""close-notice-link"">close</a>
    </div>



<div id=""saved-searches-nav"">
  <div id=""save-search-dialog"" title=""Save Search"" style=""display: none;"">

  <form class=""simple_form new_saved_search"" id=""new_saved_search"" autocomplete=""off"" novalidate=""novalidate"" action=""/saved_searches"" accept-charset=""UTF-8"" data-remote=""true"" method=""post""><input type=""hidden"" name=""authenticity_token"" value=""6LTMwbYRqkAfCywribIOK3Ost8n_PGBcH0wUixDfvrST9xP0tdLf9J1oef-OBlgpLcllduBZJSx5wmE5ouZlRQ"" autocomplete=""off"" />
    <div class=""input string required saved_search_query""><label class=""string required"" for=""saved_search_query""><abbr title=""required"">*</abbr> Query</label><input value=""kafuuin_hiiro"" data-autocomplete=""tag-query"" class=""string required"" type=""text"" name=""saved_search[query]"" id=""saved_search_query"" /></div>
    <div class=""input string optional saved_search_label_string field_with_hint""><label class=""string optional"" for=""saved_search_label_string"">Labels</label><input data-autocomplete=""saved-search-label"" class=""string optional"" type=""text"" value="""" name=""saved_search[label_string]"" id=""saved_search_label_string"" /><span class=""hint"">A list of tags to help categorize this search. Space delimited.</span></div>
    <div class=""input boolean optional saved_search_disable_labels""><input name=""saved_search[disable_labels]"" type=""hidden"" value=""0"" autocomplete=""off"" /><input class=""boolean optional"" type=""checkbox"" value=""1"" name=""saved_search[disable_labels]"" id=""saved_search_disable_labels"" /><label class=""boolean optional"" for=""saved_search_disable_labels"">Don&#39;t show this dialog again</label></div>
</form></div>

</div>



    <script type=""text/javascript"">
  $(function() {
    $.post(""https://isshiki.donmai.us/post_searches"", {
      sig: ""InBzLWthZnV1aW5faGlpcm8sZDZkMmJhNmQzOTcyNzU3ODhmOTdkM2E1NDc0ZWJiMjUi--bbd17ea3b924b5202317cc4dfc3f9dd1d8305ad1c2badbe32033b310fb6de879""
    });
  });
</script>


  <div id=""c-posts"">
    <div id=""a-index"">

      <div class=""sidebar-container"">
        <aside id=""sidebar"">

<section id=""search-box"">
  <h2>Search</h2>
  <form id=""search-box-form"" class=""flex"" action=""/posts"" accept-charset=""UTF-8"" method=""get"">
    <input type=""text"" name=""tags"" id=""tags"" value=""kafuuin_hiiro"" class=""flex-auto"" data-shortcut=""q"" data-autocomplete=""tag-query"" />
    <input type=""hidden"" name=""z"" id=""z"" value=""5"" autocomplete=""off"" />
    <button id=""search-box-submit"" type=""submit""><i class=""icon fas fa-search ""></i></button>
</form></section>



  <div id=""blacklist-box"" class=""sidebar-blacklist"">
  <h2>Blacklisted (<a class=""wiki-link"" href=""/wiki_pages/help:blacklists"">help</a>)</h2>
  <ul id=""blacklist-list"" class=""list-bulleted""></ul>
  <a id=""disable-all-blacklists"" style=""display: none;"" href=""#"">Disable all</a>
  <a id=""re-enable-all-blacklists"" style=""display: none;"" href=""#"">Re-enable all</a>
</div>


  <section id=""tag-box"">
    <h2>Tags</h2>
    <ul class=""tag-list search-tag-list"">
    <li class=""tag-type-4"" data-tag-name=""kafuuin_hiiro"" data-is-deprecated=""false"">
        <a class=""wiki-link"" href=""/wiki_pages/kafuuin_hiiro?z=2"">?</a>


      <a class=""search-tag"" href=""/posts?tags=kafuuin_hiiro&amp;z=2"">kafuuin hiiro</a>
      <span class=""post-count"" title=""2"">2</span>
    </li>
    <li class=""tag-type-4"" data-tag-name=""kinoshita_ran"" data-is-deprecated=""false"">
        <a class=""wiki-link"" href=""/wiki_pages/kinoshita_ran?z=2"">?</a>


      <a class=""search-tag"" href=""/posts?tags=kinoshita_ran&amp;z=2"">kinoshita ran</a>
      <span class=""post-count"" title=""2"">2</span>
    </li>
    <li class=""tag-type-3"" data-tag-name=""mob_shika_katan!"" data-is-deprecated=""false"">
        <a class=""wiki-link"" href=""/wiki_pages/mob_shika_katan!?z=2"">?</a>


      <a class=""search-tag"" href=""/posts?tags=mob_shika_katan%21&amp;z=2"">mob shika katan!</a>
      <span class=""post-count"" title=""4"">4</span>
    </li>
    <li class=""tag-type-1"" data-tag-name=""nekoyashiki_pushio"" data-is-deprecated=""false"">
        <a class=""wiki-link"" href=""/artists/show_or_new?name=nekoyashiki_pushio&amp;z=2"">?</a>


      <a class=""search-tag"" href=""/posts?tags=nekoyashiki_pushio&amp;z=2"">nekoyashiki pushio</a>
      <span class=""post-count"" title=""171"">171</span>
    </li>
    <li class=""tag-type-0"" data-tag-name=""shell_earrings"" data-is-deprecated=""false"">
        <a class=""wiki-link"" href=""/wiki_pages/shell_earrings?z=2"">?</a>


      <a class=""search-tag"" href=""/posts?tags=shell_earrings&amp;z=2"">shell earrings</a>
      <span class=""post-count"" title=""263"">263</span>
    </li>
    <li class=""tag-type-5"" data-tag-name=""novel_illustration"" data-is-deprecated=""false"">
        <a class=""wiki-link"" href=""/wiki_pages/novel_illustration?z=2"">?</a>


      <a class=""search-tag"" href=""/posts?tags=novel_illustration&amp;z=2"">novel illustration</a>
      <span class=""post-count"" title=""5277"">5.3k</span>
    </li>
    <li class=""tag-type-0"" data-tag-name=""untucked_shirt"" data-is-deprecated=""false"">
        <a class=""wiki-link"" href=""/wiki_pages/untucked_shirt?z=2"">?</a>


      <a class=""search-tag"" href=""/posts?tags=untucked_shirt&amp;z=2"">untucked shirt</a>
      <span class=""post-count"" title=""1449"">1.4k</span>
    </li>
    <li class=""tag-type-0"" data-tag-name=""wedding_ring"" data-is-deprecated=""false"">
        <a class=""wiki-link"" href=""/wiki_pages/wedding_ring?z=2"">?</a>


      <a class=""search-tag"" href=""/posts?tags=wedding_ring&amp;z=2"">wedding ring</a>
      <span class=""post-count"" title=""1919"">1.9k</span>
    </li>
    <li class=""tag-type-0"" data-tag-name=""hugging_own_legs"" data-is-deprecated=""false"">
        <a class=""wiki-link"" href=""/wiki_pages/hugging_own_legs?z=2"">?</a>


      <a class=""search-tag"" href=""/posts?tags=hugging_own_legs&amp;z=2"">hugging own legs</a>
      <span class=""post-count"" title=""9784"">9.8k</span>
    </li>
    <li class=""tag-type-0"" data-tag-name=""shell"" data-is-deprecated=""false"">
        <a class=""wiki-link"" href=""/wiki_pages/shell?z=2"">?</a>


      <a class=""search-tag"" href=""/posts?tags=shell&amp;z=2"">shell</a>
      <span class=""post-count"" title=""5330"">5.3k</span>
    </li>
    <li class=""tag-type-0"" data-tag-name=""knees_up"" data-is-deprecated=""false"">
        <a class=""wiki-link"" href=""/wiki_pages/knees_up?z=2"">?</a>


      <a class=""search-tag"" href=""/posts?tags=knees_up&amp;z=2"">knees up</a>
      <span class=""post-count"" title=""25584"">25k</span>
    </li>
    <li class=""tag-type-0"" data-tag-name=""star_earrings"" data-is-deprecated=""false"">
        <a class=""wiki-link"" href=""/wiki_pages/star_earrings?z=2"">?</a>


      <a class=""search-tag"" href=""/posts?tags=star_earrings&amp;z=2"">star earrings</a>
      <span class=""post-count"" title=""7331"">7.3k</span>
    </li>
    <li class=""tag-type-0"" data-tag-name=""green_skirt"" data-is-deprecated=""false"">
        <a class=""wiki-link"" href=""/wiki_pages/green_skirt?z=2"">?</a>


      <a class=""search-tag"" href=""/posts?tags=green_skirt&amp;z=2"">green skirt</a>
      <span class=""post-count"" title=""34896"">34k</span>
    </li>
    <li class=""tag-type-0"" data-tag-name=""loafers"" data-is-deprecated=""false"">
        <a class=""wiki-link"" href=""/wiki_pages/loafers?z=2"">?</a>


      <a class=""search-tag"" href=""/posts?tags=loafers&amp;z=2"">loafers</a>
      <span class=""post-count"" title=""35399"">35k</span>
    </li>
    <li class=""tag-type-0"" data-tag-name=""toenail_polish"" data-is-deprecated=""false"">
        <a class=""wiki-link"" href=""/wiki_pages/toenail_polish?z=2"">?</a>


      <a class=""search-tag"" href=""/posts?tags=toenail_polish&amp;z=2"">toenail polish</a>
      <span class=""post-count"" title=""10219"">10k</span>
    </li>
    <li class=""tag-type-0"" data-tag-name=""bob_cut"" data-is-deprecated=""false"">
        <a class=""wiki-link"" href=""/wiki_pages/bob_cut?z=2"">?</a>


      <a class=""search-tag"" href=""/posts?tags=bob_cut&amp;z=2"">bob cut</a>
      <span class=""post-count"" title=""41620"">41k</span>
    </li>
    <li class=""tag-type-0"" data-tag-name=""brown_footwear"" data-is-deprecated=""false"">
        <a class=""wiki-link"" href=""/wiki_pages/brown_footwear?z=2"">?</a>


      <a class=""search-tag"" href=""/posts?tags=brown_footwear&amp;z=2"">brown footwear</a>
      <span class=""post-count"" title=""51576"">51k</span>
    </li>
    <li class=""tag-type-0"" data-tag-name=""anklet"" data-is-deprecated=""false"">
        <a class=""wiki-link"" href=""/wiki_pages/anklet?z=2"">?</a>


      <a class=""search-tag"" href=""/posts?tags=anklet&amp;z=2"">anklet</a>
      <span class=""post-count"" title=""14002"">14k</span>
    </li>
    <li class=""tag-type-0"" data-tag-name=""arm_support"" data-is-deprecated=""false"">
        <a class=""wiki-link"" href=""/wiki_pages/arm_support?z=2"">?</a>


      <a class=""search-tag"" href=""/posts?tags=arm_support&amp;z=2"">arm support</a>
      <span class=""post-count"" title=""59987"">59k</span>
    </li>
    <li class=""tag-type-0"" data-tag-name=""profile"" data-is-deprecated=""false"">
        <a class=""wiki-link"" href=""/wiki_pages/profile?z=2"">?</a>


      <a class=""search-tag"" href=""/posts?tags=profile&amp;z=2"">profile</a>
      <span class=""post-count"" title=""70893"">70k</span>
    </li>
    <li class=""tag-type-0"" data-tag-name=""toenails"" data-is-deprecated=""false"">
        <a class=""wiki-link"" href=""/wiki_pages/toenails?z=2"">?</a>


      <a class=""search-tag"" href=""/posts?tags=toenails&amp;z=2"">toenails</a>
      <span class=""post-count"" title=""17757"">17k</span>
    </li>
    <li class=""tag-type-0"" data-tag-name=""skin_fang"" data-is-deprecated=""false"">
        <a class=""wiki-link"" href=""/wiki_pages/skin_fang?z=2"">?</a>


      <a class=""search-tag"" href=""/posts?tags=skin_fang&amp;z=2"">skin fang</a>
      <span class=""post-count"" title=""20998"">20k</span>
    </li>
    <li class=""tag-type-0"" data-tag-name=""pink_nails"" data-is-deprecated=""false"">
        <a class=""wiki-link"" href=""/wiki_pages/pink_nails?z=2"">?</a>


      <a class=""search-tag"" href=""/posts?tags=pink_nails&amp;z=2"">pink nails</a>
      <span class=""post-count"" title=""23079"">23k</span>
    </li>
    <li class=""tag-type-0"" data-tag-name=""red_ribbon"" data-is-deprecated=""false"">
        <a class=""wiki-link"" href=""/wiki_pages/red_ribbon?z=2"">?</a>


      <a class=""search-tag"" href=""/posts?tags=red_ribbon&amp;z=2"">red ribbon</a>
      <span class=""post-count"" title=""101163"">101k</span>
    </li>
    <li class=""tag-type-0"" data-tag-name=""socks"" data-is-deprecated=""false"">
        <a class=""wiki-link"" href=""/wiki_pages/socks?z=2"">?</a>


      <a class=""search-tag"" href=""/posts?tags=socks&amp;z=2"">socks</a>
      <span class=""post-count"" title=""131369"">131k</span>
    </li>
</ul>

  </section>

  <section id=""options-box"">
    <h2>Options</h2>
    <ul>
    </ul>
  </section>

  <section id=""related-box"">
    <h2>Related</h2>
    <ul id=""related-list"">

      <li><a rel=""nofollow"" href=""/posts?tags=kafuuin_hiiro+status%3Adeleted"">Deleted</a></li>
      <li><a id=""random-post"" data-shortcut=""r"" rel=""nofollow"" href=""/posts/random?tags=kafuuin_hiiro"">Random</a></li>
        <li><a rel=""nofollow"" href=""/post_versions?search%5Bchanged_tags%5D=kafuuin_hiiro"">History</a></li>
          <li><a rel=""nofollow"" href=""/forum_posts?search%5Blinked_to%5D=kafuuin_hiiro"">Discussions</a></li>
      <li><a rel=""nofollow"" href=""/counts/posts?tags=kafuuin_hiiro"">Count</a></li>
    </ul>
  </section>

        </aside>

        <section id=""content"">
            <menu id=""post-sections"" class=""flex"">
    <li class=""flex-grow-1 space-x-2"">
      <a href=""#"" id=""show-posts-link"" class=""active"">Posts</a>

        <a id=""show-excerpt-link"" class=""blank-wiki-excerpt-link"" href=""/wiki_pages/new?wiki_page%5Btitle%5D=kafuuin_hiiro"">Wiki</a>

      <a class=""mobile-only"" href=""#search-box"">Search &raquo;</a>
    </li>

    <li class=""flex items-center gap-2"">
      <div class=""popup-menu inline-block preview-size-menu"">
  <a class=""popup-menu-button inline-block rounded p-1"" href=""javascript:void(0)"">
          <svg class=""icon svg-icon image-icon "" role=""img"" xmlns=""http://www.w3.org/2000/svg"" viewBox=""0 0 512 512""><path fill=""currentColor"" d=""M447.1 32h-384C28.64 32-.0091 60.65-.0091 96v320c0 35.35 28.65 64 63.1 64h384c35.35 0 64-28.65 64-64V96C511.1 60.65 483.3 32 447.1 32zM111.1 96c26.51 0 48 21.49 48 48S138.5 192 111.1 192s-48-21.49-48-48S85.48 96 111.1 96zM446.1 407.6C443.3 412.8 437.9 416 432 416H82.01c-6.021 0-11.53-3.379-14.26-8.75c-2.73-5.367-2.215-11.81 1.334-16.68l70-96C142.1 290.4 146.9 288 152 288s9.916 2.441 12.93 6.574l32.46 44.51l93.3-139.1C293.7 194.7 298.7 192 304 192s10.35 2.672 13.31 7.125l128 192C448.6 396 448.9 402.3 446.1 407.6z"" /></svg> <span class=""text-sm"">Size</span> <svg class=""icon svg-icon caret-down-icon text-xs"" role=""img"" xmlns=""http://www.w3.org/2000/svg"" viewBox=""0 0 320 512""><path fill=""currentColor"" d=""M310.6 246.6l-127.1 128C176.4 380.9 168.2 384 160 384s-16.38-3.125-22.63-9.375l-127.1-128C.2244 237.5-2.516 223.7 2.438 211.8S19.07 192 32 192h255.1c12.94 0 24.62 7.781 29.58 19.75S319.8 237.5 310.6 246.6z"" /></svg>

  </a>

  <ul class=""popup-menu-content"">
      <li>    <a rel=""nofollow"" href=""/posts?size=150&amp;tags=kafuuin_hiiro"">Small</a>
</li>
      <li>    <a class=""font-bold"" rel=""nofollow"" href=""/posts?size=180&amp;tags=kafuuin_hiiro"">Medium</a>
</li>
      <li>    <a rel=""nofollow"" href=""/posts?size=225&amp;tags=kafuuin_hiiro"">Large</a>
</li>
      <li>    <a class=""mobile-only"" rel=""nofollow"" href=""/posts?size=360&amp;tags=kafuuin_hiiro"">Huge</a>
</li>
      <li>    <a class=""desktop-only"" rel=""nofollow"" href=""/posts?size=270&amp;tags=kafuuin_hiiro"">Huge</a>
</li>
      <li>    <a class=""desktop-only"" rel=""nofollow"" href=""/posts?size=360&amp;tags=kafuuin_hiiro"">Gigantic</a>
</li>
  </ul>
</div>


      <div class=""popup-menu inline-block post-preview-options-menu"">
  <a class=""popup-menu-button inline-block rounded p-1"" href=""javascript:void(0)"">
      <i class=""icon fas fa-ellipsis-h ""></i>
  </a>

  <ul class=""popup-menu-content"">
      <li>            <a class=""post-preview-show-votes"" rel=""nofollow"" href=""/posts?show_votes=true&amp;tags=kafuuin_hiiro"">Show scores</a>
</li>
  </ul>
</div>
    </li>
  </menu>

  <div id=""quick-edit-div"" style=""display: none;"">
    <h2>Edit</h2>

    <form autocomplete=""off"" id=""quick-edit-form"" novalidate=""novalidate"" class=""simple_form post"" action=""/posts"" accept-charset=""UTF-8"" method=""post""><input type=""hidden"" name=""authenticity_token"" value=""FKOHcQ-bxvLKNe-yrsLgVrlOwAKouQPQ3I9qZUhnUDrjil02tuesztaUAkTI7nk_Y7EcqqRItDFaf3ILhVlZWg"" autocomplete=""off"" />
      <div class=""input text required post_tag_string""><label class=""text required"" for=""post_tag_string""><abbr title=""required"">*</abbr> Tags</label><textarea data-autocomplete=""tag-edit"" class=""text required"" name=""post[tag_string]"" id=""post_tag_string""></textarea></div>
      <input type=""submit"" name=""commit"" value=""Submit"" />
      <button name=""cancel"" type=""button"" class=""btn"">Cancel</button>
</form>  </div>

  <div id=""excerpt"" style=""display: none;"">
      <p>There is no wiki page yet for the tag <a class=""wiki-link"" href=""/wiki_pages/kafuuin%20hiiro"">kafuuin hiiro</a>. <a rel=""nofollow"" href=""/wiki_pages/new?wiki_page%5Btitle%5D=kafuuin_hiiro"">Create new wiki page</a>.</p>







  </div>

  <div id=""posts"">
    <div class=""post-gallery post-gallery-grid post-gallery-180"">
    <div class=""posts-container"">
<article id=""post_4928081"" class=""post-preview post-status-has-parent post-preview-fit-compact post-preview-180"" data-id=""4928081"" data-tags=""2girls arm_support bangs black_hair black_legwear blunt_bangs blush bob_cut brown_footwear brown_hair closed_mouth commentary_request eyebrows_visible_through_hair from_behind full_body green_skirt hair_ornament hair_ribbon hairclip highres hugging_own_legs kafuuin_hiiro kinoshita_ran knees_up loafers long_sleeves looking_at_viewer looking_back miniskirt mob_shika_katan! multiple_girls nekoyashiki_pushio novel_illustration official_art pantyhose ponytail profile purple_eyes red_eyes red_ribbon ribbon school_uniform shirt shoes short_hair sidelocks simple_background skirt socks untucked_shirt white_legwear white_shirt"" data-rating=""s"" data-flags="""" data-score=""2"" data-uploader-id=""624174"">  <div class=""post-preview-container"">
    <a class=""post-preview-link"" draggable=""false"" href=""/posts/4928081?q=kafuuin_hiiro"">
      <picture>
            <source type=""image/jpeg"" srcset=""https://cdn.donmai.us/180x180/c4/56/c4560222ebe8a87818dd8f8667108c58.jpg 1x, https://cdn.donmai.us/360x360/c4/56/c4560222ebe8a87818dd8f8667108c58.jpg 2x"">

        <img src=""https://cdn.donmai.us/180x180/c4/56/c4560222ebe8a87818dd8f8667108c58.jpg"" width=""180"" height=""117"" class=""post-preview-image"" title=""2girls arm_support bangs black_hair black_legwear blunt_bangs blush bob_cut brown_footwear brown_hair closed_mouth commentary_request eyebrows_visible_through_hair from_behind full_body green_skirt hair_ornament hair_ribbon hairclip highres hugging_own_legs kafuuin_hiiro kinoshita_ran knees_up loafers long_sleeves looking_at_viewer looking_back miniskirt mob_shika_katan! multiple_girls nekoyashiki_pushio novel_illustration official_art pantyhose ponytail profile purple_eyes red_eyes red_ribbon ribbon school_uniform shirt shoes short_hair sidelocks simple_background skirt socks untucked_shirt white_legwear white_shirt rating:s score:2"" alt=""post #4928081"" crossorigin=""anonymous"" draggable=""false"">      </picture>
</a>  </div>
</article><article id=""post_4928078"" class=""post-preview post-status-has-children post-preview-fit-compact post-preview-180"" data-id=""4928078"" data-tags=""3girls :d ahoge anklet arm_support bangs bare_shoulders barefoot black_hair black_legwear black_ribbon blonde_hair blunt_bangs blush bob_cut braid breasts brown_footwear brown_hair character_request cleavage closed_mouth collarbone commentary_request earrings eyebrows_visible_through_hair fang full_body green_skirt hair_ornament hair_ribbon hairclip hand_up head_tilt highres hugging_own_legs jewelry kafuuin_hiiro kinoshita_ran knees_up large_breasts loafers long_hair long_sleeves looking_at_viewer looking_back miniskirt mob_shika_katan! multicolored_hair multiple_girls nail_polish necklace nekoyashiki_pushio novel_illustration off_shoulder official_art open_mouth pantyhose pink_nails ponytail profile purple_eyes red_eyes red_ribbon ribbon ring school_uniform shell shell_earrings shirt shoes short_hair sidelocks simple_background sitting skin_fang skirt smile socks sparkle star_(symbol) star_earrings streaked_hair thighs toenail_polish toenails toes very_long_hair wariza wedding_ring white_background white_legwear white_shirt"" data-rating=""s"" data-flags="""" data-score=""5"" data-uploader-id=""624174"">  <div class=""post-preview-container"">
    <a class=""post-preview-link"" draggable=""false"" href=""/posts/4928078?q=kafuuin_hiiro"">
      <picture>
            <source type=""image/jpeg"" srcset=""https://cdn.donmai.us/180x180/52/16/52166402f523aaa96bb6e26e7d22d8e6.jpg 1x, https://cdn.donmai.us/360x360/52/16/52166402f523aaa96bb6e26e7d22d8e6.jpg 2x"">

        <img src=""https://cdn.donmai.us/180x180/52/16/52166402f523aaa96bb6e26e7d22d8e6.jpg"" width=""172"" height=""180"" class=""post-preview-image"" title=""3girls :d ahoge anklet arm_support bangs bare_shoulders barefoot black_hair black_legwear black_ribbon blonde_hair blunt_bangs blush bob_cut braid breasts brown_footwear brown_hair character_request cleavage closed_mouth collarbone commentary_request earrings eyebrows_visible_through_hair fang full_body green_skirt hair_ornament hair_ribbon hairclip hand_up head_tilt highres hugging_own_legs jewelry kafuuin_hiiro kinoshita_ran knees_up large_breasts loafers long_hair long_sleeves looking_at_viewer looking_back miniskirt mob_shika_katan! multicolored_hair multiple_girls nail_polish necklace nekoyashiki_pushio novel_illustration off_shoulder official_art open_mouth pantyhose pink_nails ponytail profile purple_eyes red_eyes red_ribbon ribbon ring school_uniform shell shell_earrings shirt shoes short_hair sidelocks simple_background sitting skin_fang skirt smile socks sparkle star_(symbol) star_earrings streaked_hair thighs toenail_polish toenails toes very_long_hair wariza wedding_ring white_background white_legwear white_shirt rating:s score:5"" alt=""post #4928078"" crossorigin=""anonymous"" draggable=""false"">      </picture>
</a>  </div>
</article>    </div>



        <div class=""paginator numbered-paginator mt-8 mb-4 space-x-2 flex justify-center items-center"">
  <span class=""paginator-prev"" rel=""prev"" data-shortcut=""a left""><i class=""icon fas fa-chevron-left ""></i></span>

      <span class=""paginator-current font-bold"">1</span>

  <span class=""paginator-next"" rel=""next"" data-shortcut=""d right""><i class=""icon fas fa-chevron-right ""></i></span>
</div>


</div>
  </div>

        </section>
      </div>
    </div>
  </div>

  </div>

  <div id=""tooltips"">
    <div id=""post-tooltips""></div>
    <div id=""comment-votes-tooltips""></div>
    <div id=""post-votes-tooltips""></div>
    <div id=""post-favorites-tooltips""></div>
    <div id=""user-tooltips""></div>
    <div id=""popup-menus""></div>
  </div>

  <footer id=""page-footer"" class=""text-sm flex-initial"">
    <span class=""page-footer-app-name"" title=""Running commit: 81bd86d20"">Danbooru</span>
    / <a href=""/terms_of_service"">Terms</a>
    / <a href=""/privacy"">Privacy</a>
    / <a href=""/2257"">2257</a>
    / <a href=""/upgrade"">Upgrade</a>
    / <a href=""/contact"">Contact</a>
    </span>
  </footer>
<script defer src=""https://static.cloudflareinsights.com/beacon.min.js/v652eace1692a40cfa3763df669d7439c1639079717194"" integrity=""sha512-Gi7xpJR8tSkrpF7aordPZQlW2DLtzUlZcumS8dMQjwDHEnw9I7ZLyiOj/6tZStRBGtGgN6ceN6cMH8z7etPGlw=="" data-cf-beacon='{""rayId"":""71387bf869bb8938"",""token"":""d251ec98e50f454880fc91e7b6fa8ba8"",""version"":""2021.12.0"",""si"":100}' crossorigin=""anonymous""></script>
</body></html>
";

    const string POSTS_RAW_2 = @"
<!doctype html>
<html lang=""en"">
<head>
  <meta charset=""utf-8"">
  <title>Kafuuin  | Danbooru</title>

  <link rel=""icon"" href=""/favicon.ico"" sizes=""16x16"" type=""image/x-icon"">
  <link rel=""icon"" href=""/favicon.svg"" sizes=""any"" type=""image/svg+xml"">

  <link rel=""canonical"" href=""https://danbooru.donmai.us/posts?tags=kafuuin_"">
  <link rel=""search"" type=""application/opensearchdescription+xml"" href=""https://danbooru.donmai.us/opensearch.xml?version=2"" title=""Search posts"">

  <meta name=""csrf-param"" content=""authenticity_token"" />
<meta name=""csrf-token"" content=""aaYTDRuSo5UFQBHNJH4ak657cVWi43jjCXWoupV1KDRa9DZMBSI8lJsRcjURiM-8YZZTBeL_HAxYC_N1LB2Ajg"" />
    <meta name=""viewport"" content=""width=device-width,initial-scale=1"">
    <meta name=""blacklisted-tags"" content=""guro,scat,furry -rating:s"">

  <meta name=""autocomplete-tag-prefixes"" content=""[&quot;ch:&quot;,&quot;co:&quot;,&quot;gen:&quot;,&quot;char:&quot;,&quot;copy:&quot;,&quot;art:&quot;,&quot;meta:&quot;,&quot;general:&quot;,&quot;character:&quot;,&quot;copyright:&quot;,&quot;artist:&quot;]"">

    <script src=""/packs/js/runtime-502cc943ed047c87eac6.js""></script>
<script src=""/packs/js/20-34b75d7170810064ff25.js""></script>
<script src=""/packs/js/application-1f97225d1d2f4b3370ec.js""></script>
<script src=""/packs/js/780-9a149046694c464f5a58.js""></script>
<script src=""/packs/js/alpine-983543cca5034868c8ce.js""></script>

  <link rel=""stylesheet"" href=""/packs/css/20-1647607e.css"" />
<link rel=""stylesheet"" href=""/packs/css/application-2632588c.css"" />


  <meta name=""description"" content=""See 0 Kafuuin  images on Danbooru. "">
  <meta property=""og:type"" content=""website"">
  <meta property=""og:site_name"" content=""Danbooru"">
  <meta property=""og:title"" content=""Kafuuin  | Danbooru"">
  <meta property=""og:description"" content=""See 0 Kafuuin  images on Danbooru. "">
  <meta property=""og:url"" content=""https://danbooru.donmai.us/posts?tags=kafuuin_"">

    <meta name=""twitter:site"" content=""@danboorubot"">

  <meta name=""twitter:title"" content=""Kafuuin  | Danbooru"">
  <meta name=""twitter:description"" content=""See 0 Kafuuin  images on Danbooru. "">

  <meta name=""git-hash"" content=""81bd86d202afc2558f73d04f0683d7f806e8cdc2"">
  <meta name=""theme-color"" content=""hsl(213, 100%, 50%)"">

  <link rel=""alternate"" type=""application/atom+xml"" title=""Posts: Kafuuin "" href=""https://danbooru.donmai.us/posts.atom?tags=kafuuin_"" />

</head>

<body lang=""en"" class=""c-posts a-index flex flex-col"" spellcheck=""false"" data-controller=""posts"" data-action=""index"" data-layout=""sidebar"" data-current-user-ip-addr=""180.252.157.228"" data-current-user-save-data=""false"" data-current-user-id=""null"" data-current-user-name=""Anonymous"" data-current-user-level=""0"" data-current-user-level-string=""Anonymous"" data-current-user-theme=""auto"" data-current-user-comment-threshold=""-8"" data-current-user-default-image-size=""large"" data-current-user-time-zone=""Eastern Time (US &amp; Canada)"" data-current-user-per-page=""20"" data-current-user-is-banned=""false"" data-current-user-receive-email-notifications=""false"" data-current-user-new-post-navigation-layout=""true"" data-current-user-enable-private-favorites=""false"" data-current-user-style-usernames=""false"" data-current-user-show-deleted-children=""false"" data-current-user-can-approve-posts=""false"" data-current-user-can-upload-free=""false"" data-current-user-disable-categorized-saved-searches=""false"" data-current-user-disable-tagged-filenames=""false"" data-current-user-disable-mobile-gestures=""false"" data-current-user-enable-safe-mode=""false"" data-current-user-enable-desktop-mode=""false"" data-current-user-disable-post-tooltips=""false"" data-current-user-requires-verification=""false"" data-current-user-is-verified=""false"" data-current-user-show-deleted-posts=""false"" data-current-user-is-moderator=""false"" data-current-user-is-restricted=""false"" data-current-user-is-anonymous=""true"" data-current-user-is-gold=""false"" data-current-user-is-platinum=""false"" data-current-user-is-builder=""false"" data-current-user-is-admin=""false"" data-current-user-is-member=""false"" data-current-user-is-owner=""false"" data-current-user-is-approver=""false"" data-cookie-news-ticker=""null"" data-cookie-hide-upgrade-account-notice=""null"" data-cookie-hide-verify-account-notice=""null"" data-cookie-hide-dmail-notice=""null"" data-cookie-dab=""null"" data-cookie-show-relationship-previews=""null"" data-cookie-post-preview-size=""null"" data-cookie-post-preview-show-votes=""null"">

  <header id=""top"">
    <div id=""app-name-header"" class=""font-bold font-header leading-normal inline-flex items-center gap-1"">
      <a id=""app-logo"" href=""/""><img src=""/packs/static/danbooru-logo-128x128-ea111b6658173e847734.png"" /></a>
      <a id=""app-name"" href=""/"">Danbooru</a>
    </div>

    <div id=""maintoggle"" class=""mobile-only"">
      <a href=""#""><i class=""icon fas fa-bars "" id=""maintoggle-on""></i></a>
      <a href=""#""><i class=""icon fas fa-times "" id=""maintoggle-off"" style=""display: none;""></i></a>
    </div>

    <nav id=""nav"">
      <menu id=""main-menu"" class=""main"">
          <li id=""nav-login""><a id=""nav-login-link"" rel=""nofollow"" href=""/login?url=%2Fposts%3Ftags%3Dkafuuin_"">Login</a></li>

        <li id=""nav-posts"" class="" current""><a id=""nav-posts-link"" href=""/posts"">Posts</a></li>
        <li id=""nav-comments""><a id=""nav-comments-link"" href=""/comments"">Comments</a></li>
        <li id=""nav-notes""><a id=""nav-notes-link"" href=""/notes"">Notes</a></li>
        <li id=""nav-artists""><a id=""nav-artists-link"" href=""/artists"">Artists</a></li>
        <li id=""nav-tags""><a id=""nav-tags-link"" href=""/tags"">Tags</a></li>
        <li id=""nav-pools""><a id=""nav-pools-link"" href=""/pools/gallery"">Pools</a></li>
        <li id=""nav-wiki""><a id=""nav-wiki-link"" href=""/wiki_pages/help:home"">Wiki</a></li>
        <li id=""nav-forum""><a id=""nav-forum-link"" href=""/forum_topics"">Forum</a></li>


        <li id=""nav-more""><a id=""nav-more-link"" href=""/static/site_map"">More »</a></li>
      </menu>

        <menu id=""subnav-menu"">
            <li id=""subnav-listing""><a id=""subnav-listing-link"" href=""/posts"">Listing</a></li>
    <li id=""subnav-upload""><a id=""subnav-upload-link"" href=""/login?url=%2Fuploads%2Fnew"">Upload</a></li>
  <li id=""subnav-hot""><a id=""subnav-hot-link"" href=""/posts?d=1&amp;tags=order%3Arank"">Hot</a></li>
  <li id=""subnav-changes""><a id=""subnav-changes-link"" href=""/post_versions"">Changes</a></li>
  <li id=""subnav-help""><a id=""subnav-help-link"" href=""/wiki_pages/help:posts"">Help</a></li>

        </menu>
    </nav>
  </header>

  <div id=""page"" class=""flex-1 mt-4"">





    <div class=""notice notice-info"" id=""notice"" style=""display: none;"">
      <span class=""prose"">.</span>
      <a href=""#"" id=""close-notice-link"">close</a>
    </div>



<div id=""saved-searches-nav"">
  <div id=""save-search-dialog"" title=""Save Search"" style=""display: none;"">

  <form class=""simple_form new_saved_search"" id=""new_saved_search"" autocomplete=""off"" novalidate=""novalidate"" action=""/saved_searches"" accept-charset=""UTF-8"" data-remote=""true"" method=""post""><input type=""hidden"" name=""authenticity_token"" value=""O7TRE-rg1WWK316A6E-HX52FS00pqhglYQkvswyQnz9_va2dwc37lcY-wssbECzHYkD1KuF9HR18dAwffjK9jw"" autocomplete=""off"" />
    <div class=""input string required saved_search_query""><label class=""string required"" for=""saved_search_query""><abbr title=""required"">*</abbr> Query</label><input value=""kafuuin_"" data-autocomplete=""tag-query"" class=""string required"" type=""text"" name=""saved_search[query]"" id=""saved_search_query"" /></div>
    <div class=""input string optional saved_search_label_string field_with_hint""><label class=""string optional"" for=""saved_search_label_string"">Labels</label><input data-autocomplete=""saved-search-label"" class=""string optional"" type=""text"" value="""" name=""saved_search[label_string]"" id=""saved_search_label_string"" /><span class=""hint"">A list of tags to help categorize this search. Space delimited.</span></div>
    <div class=""input boolean optional saved_search_disable_labels""><input name=""saved_search[disable_labels]"" type=""hidden"" value=""0"" autocomplete=""off"" /><input class=""boolean optional"" type=""checkbox"" value=""1"" name=""saved_search[disable_labels]"" id=""saved_search_disable_labels"" /><label class=""boolean optional"" for=""saved_search_disable_labels"">Don&#39;t show this dialog again</label></div>
</form></div>

</div>



    <script type=""text/javascript"">
  $(function() {
    $.post(""https://isshiki.donmai.us/missed_searches"", {
      sig: ""ImthZnV1aW5fLGU3OTllM2VjNjIzYjc5ZmZkYTcyY2U2NjkyZTZmNTAyIg==--2f7ab8729d2ae5d9a57ec8e66fae55c8ad8ef708bc599d2ff280934b029f3d07""
    });
  });
</script>


  <div id=""c-posts"">
    <div id=""a-index"">

      <div class=""sidebar-container"">
        <aside id=""sidebar"">

<section id=""search-box"">
  <h2>Search</h2>
  <form id=""search-box-form"" class=""flex"" action=""/posts"" accept-charset=""UTF-8"" method=""get"">
    <input type=""text"" name=""tags"" id=""tags"" value=""kafuuin_"" class=""flex-auto"" data-shortcut=""q"" data-autocomplete=""tag-query"" />
    <input type=""hidden"" name=""z"" id=""z"" value=""5"" autocomplete=""off"" />
    <button id=""search-box-submit"" type=""submit""><i class=""icon fas fa-search ""></i></button>
</form></section>



  <div id=""blacklist-box"" class=""sidebar-blacklist"">
  <h2>Blacklisted (<a class=""wiki-link"" href=""/wiki_pages/help:blacklists"">help</a>)</h2>
  <ul id=""blacklist-list"" class=""list-bulleted""></ul>
  <a id=""disable-all-blacklists"" style=""display: none;"" href=""#"">Disable all</a>
  <a id=""re-enable-all-blacklists"" style=""display: none;"" href=""#"">Re-enable all</a>
</div>


  <section id=""tag-box"">
    <h2>Tags</h2>
    <ul class=""tag-list search-tag-list"">
</ul>

  </section>

  <section id=""options-box"">
    <h2>Options</h2>
    <ul>
    </ul>
  </section>

  <section id=""related-box"">
    <h2>Related</h2>
    <ul id=""related-list"">

      <li><a rel=""nofollow"" href=""/posts?tags=kafuuin_+status%3Adeleted"">Deleted</a></li>
      <li><a id=""random-post"" data-shortcut=""r"" rel=""nofollow"" href=""/posts/random?tags=kafuuin_"">Random</a></li>
        <li><a rel=""nofollow"" href=""/post_versions?search%5Bchanged_tags%5D=kafuuin_"">History</a></li>
          <li><a rel=""nofollow"" href=""/forum_posts?search%5Blinked_to%5D=kafuuin_"">Discussions</a></li>
      <li><a rel=""nofollow"" href=""/counts/posts?tags=kafuuin_"">Count</a></li>
    </ul>
  </section>

        </aside>

        <section id=""content"">
            <menu id=""post-sections"" class=""flex"">
    <li class=""flex-grow-1 space-x-2"">
      <a href=""#"" id=""show-posts-link"" class=""active"">Posts</a>


      <a class=""mobile-only"" href=""#search-box"">Search &raquo;</a>
    </li>

    <li class=""flex items-center gap-2"">
      <div class=""popup-menu inline-block preview-size-menu"">
  <a class=""popup-menu-button inline-block rounded p-1"" href=""javascript:void(0)"">
          <svg class=""icon svg-icon image-icon "" role=""img"" xmlns=""http://www.w3.org/2000/svg"" viewBox=""0 0 512 512""><path fill=""currentColor"" d=""M447.1 32h-384C28.64 32-.0091 60.65-.0091 96v320c0 35.35 28.65 64 63.1 64h384c35.35 0 64-28.65 64-64V96C511.1 60.65 483.3 32 447.1 32zM111.1 96c26.51 0 48 21.49 48 48S138.5 192 111.1 192s-48-21.49-48-48S85.48 96 111.1 96zM446.1 407.6C443.3 412.8 437.9 416 432 416H82.01c-6.021 0-11.53-3.379-14.26-8.75c-2.73-5.367-2.215-11.81 1.334-16.68l70-96C142.1 290.4 146.9 288 152 288s9.916 2.441 12.93 6.574l32.46 44.51l93.3-139.1C293.7 194.7 298.7 192 304 192s10.35 2.672 13.31 7.125l128 192C448.6 396 448.9 402.3 446.1 407.6z"" /></svg> <span class=""text-sm"">Size</span> <svg class=""icon svg-icon caret-down-icon text-xs"" role=""img"" xmlns=""http://www.w3.org/2000/svg"" viewBox=""0 0 320 512""><path fill=""currentColor"" d=""M310.6 246.6l-127.1 128C176.4 380.9 168.2 384 160 384s-16.38-3.125-22.63-9.375l-127.1-128C.2244 237.5-2.516 223.7 2.438 211.8S19.07 192 32 192h255.1c12.94 0 24.62 7.781 29.58 19.75S319.8 237.5 310.6 246.6z"" /></svg>

  </a>

  <ul class=""popup-menu-content"">
      <li>    <a rel=""nofollow"" href=""/posts?size=150&amp;tags=kafuuin_"">Small</a>
</li>
      <li>    <a class=""font-bold"" rel=""nofollow"" href=""/posts?size=180&amp;tags=kafuuin_"">Medium</a>
</li>
      <li>    <a rel=""nofollow"" href=""/posts?size=225&amp;tags=kafuuin_"">Large</a>
</li>
      <li>    <a class=""mobile-only"" rel=""nofollow"" href=""/posts?size=360&amp;tags=kafuuin_"">Huge</a>
</li>
      <li>    <a class=""desktop-only"" rel=""nofollow"" href=""/posts?size=270&amp;tags=kafuuin_"">Huge</a>
</li>
      <li>    <a class=""desktop-only"" rel=""nofollow"" href=""/posts?size=360&amp;tags=kafuuin_"">Gigantic</a>
</li>
  </ul>
</div>


      <div class=""popup-menu inline-block post-preview-options-menu"">
  <a class=""popup-menu-button inline-block rounded p-1"" href=""javascript:void(0)"">
      <i class=""icon fas fa-ellipsis-h ""></i>
  </a>

  <ul class=""popup-menu-content"">
      <li>            <a class=""post-preview-show-votes"" rel=""nofollow"" href=""/posts?show_votes=true&amp;tags=kafuuin_"">Show scores</a>
</li>
  </ul>
</div>
    </li>
  </menu>

  <div id=""quick-edit-div"" style=""display: none;"">
    <h2>Edit</h2>

    <form autocomplete=""off"" id=""quick-edit-form"" novalidate=""novalidate"" class=""simple_form post"" action=""/posts"" accept-charset=""UTF-8"" method=""post""><input type=""hidden"" name=""authenticity_token"" value=""9JrgPVbvnMykr8Q-4O1-FG2owAVEgsnImJM9ugimSgKK3p72BEj9mvr8TJa6AamyzPcjqO0ACiQ39f065DwKEg"" autocomplete=""off"" />
      <div class=""input text required post_tag_string""><label class=""text required"" for=""post_tag_string""><abbr title=""required"">*</abbr> Tags</label><textarea data-autocomplete=""tag-edit"" class=""text required"" name=""post[tag_string]"" id=""post_tag_string""></textarea></div>
      <input type=""submit"" name=""commit"" value=""Submit"" />
      <button name=""cancel"" type=""button"" class=""btn"">Cancel</button>
</form>  </div>

  <div id=""excerpt"" style=""display: none;"">
  </div>

  <div id=""posts"">
    <div class=""post-gallery post-gallery-grid post-gallery-180"">
    <p>No posts found.</p>



        <div class=""paginator numbered-paginator mt-8 mb-4 space-x-2 flex justify-center items-center"">
  <span class=""paginator-prev"" rel=""prev"" data-shortcut=""a left""><i class=""icon fas fa-chevron-left ""></i></span>

      <span class=""paginator-current font-bold"">1</span>

  <span class=""paginator-next"" rel=""next"" data-shortcut=""d right""><i class=""icon fas fa-chevron-right ""></i></span>
</div>


</div>
  </div>

        </section>
      </div>
    </div>
  </div>

  </div>

  <div id=""tooltips"">
    <div id=""post-tooltips""></div>
    <div id=""comment-votes-tooltips""></div>
    <div id=""post-votes-tooltips""></div>
    <div id=""post-favorites-tooltips""></div>
    <div id=""user-tooltips""></div>
    <div id=""popup-menus""></div>
  </div>

  <footer id=""page-footer"" class=""text-sm flex-initial"">
    <span class=""page-footer-app-name"" title=""Running commit: 81bd86d20"">Danbooru</span>
    / <a href=""/terms_of_service"">Terms</a>
    / <a href=""/privacy"">Privacy</a>
    / <a href=""/2257"">2257</a>
    / <a href=""/upgrade"">Upgrade</a>
    / <a href=""/contact"">Contact</a>
    </span>
  </footer>
<script defer src=""https://static.cloudflareinsights.com/beacon.min.js/v652eace1692a40cfa3763df669d7439c1639079717194"" integrity=""sha512-Gi7xpJR8tSkrpF7aordPZQlW2DLtzUlZcumS8dMQjwDHEnw9I7ZLyiOj/6tZStRBGtGgN6ceN6cMH8z7etPGlw=="" data-cf-beacon='{""rayId"":""713899f86f4d8938"",""token"":""d251ec98e50f454880fc91e7b6fa8ba8"",""version"":""2021.12.0"",""si"":100}' crossorigin=""anonymous""></script>
</body></html>
";

    const string POST_PREVIEW_RAW_1 = @"
<div class=""post-tooltip-header"">
  <a class=""user user-platinum "" data-user-id=""624174"" data-user-name=""mikoya"" data-user-level=""31"" href=""/users/624174"">mikoya</a>

  <span class=""post-tooltip-favorites post-tooltip-info"">
    <span>4</span>
    <i class=""icon far fa-heart fa-xs""></i>
  </span>

  <span class=""post-tooltip-score post-tooltip-info"">
    <span>2</span>
    <svg class=""icon svg-icon upvote-icon "" role=""img"" xmlns=""http://www.w3.org/2000/svg"" viewBox=""0 0 448 512""><path fill=""currentColor"" d=""M272 480h-96c-13.3 0-24-10.7-24-24V256H48.2c-21.4 0-32.1-25.8-17-41L207 39c9.4-9.4 24.6-9.4 34 0l175.8 176c15.1 15.1 4.4 41-17 41H296v200c0 13.3-10.7 24-24 24z"" /></svg>
  </span>


  <a class=""post-tooltip-date post-tooltip-info"" href=""/posts?tags=date%3A2021-11-20"">
    <time datetime=""2021-11-20T18:12-05:00"" title=""2021-11-20 18:12:18 -0500"" class=""compact-timestamp"">6mo</time> ago
</a>
    <a class=""post-tooltip-source post-tooltip-info"" href=""https://twitter.com/MF_bunkoJ/status/1403290921446215683"">twitter.com</a>

  <a class=""post-tooltip-rating post-tooltip-info"" href=""/posts?tags=rating%3ASensitive"">S</a>
  <a class=""post-tooltip-dimensions post-tooltip-info"" href=""https://cdn.donmai.us/original/c4/56/c4560222ebe8a87818dd8f8667108c58.jpg"">2000x1297</a>

  <div class=""popup-menu inline-block "">
  <a class=""popup-menu-button inline-block rounded p-1"" href=""javascript:void(0)"">
      <i class=""icon fas fa-ellipsis-h ""></i>
  </a>

  <ul class=""popup-menu-content"">
      <li>      <a class=""post-tooltip-disable"" href=""javascript:void(0)"">
        <i class=""icon fas fa-times ""></i> Disable tooltips
</a></li>
  </ul>
</div>
</div>

<div class=""post-tooltip-body thin-scrollbar "">
  <div class=""post-tooltip-body-left"">
  </div>

  <div class=""post-tooltip-body-right"">
    <div class=""post-tooltip-pools"">
    </div>

    <div class=""tag-list inline-tag-list"">
      <a class=""search-tag tag-type-1"" data-tag-name=""nekoyashiki_pushio"" data-is-deprecated=""false"" href=""/posts?tags=nekoyashiki_pushio"">nekoyashiki_pushio</a>
      <a class=""search-tag tag-type-3"" data-tag-name=""mob_shika_katan!"" data-is-deprecated=""false"" href=""/posts?tags=mob_shika_katan%21"">mob_shika_katan!</a>
      <a class=""search-tag tag-type-4"" data-tag-name=""kafuuin_hiiro"" data-is-deprecated=""false"" href=""/posts?tags=kafuuin_hiiro"">kafuuin_hiiro</a>
      <a class=""search-tag tag-type-4"" data-tag-name=""kinoshita_ran"" data-is-deprecated=""false"" href=""/posts?tags=kinoshita_ran"">kinoshita_ran</a>
      <a class=""search-tag tag-type-5"" data-tag-name=""commentary_request"" data-is-deprecated=""false"" href=""/posts?tags=commentary_request"">commentary_request</a>
      <a class=""search-tag tag-type-5"" data-tag-name=""highres"" data-is-deprecated=""false"" href=""/posts?tags=highres"">highres</a>
      <a class=""search-tag tag-type-5"" data-tag-name=""novel_illustration"" data-is-deprecated=""false"" href=""/posts?tags=novel_illustration"">novel_illustration</a>
      <a class=""search-tag tag-type-5"" data-tag-name=""official_art"" data-is-deprecated=""false"" href=""/posts?tags=official_art"">official_art</a>
      <a class=""search-tag tag-type-0"" data-tag-name=""2girls"" data-is-deprecated=""false"" href=""/posts?tags=2girls"">2girls</a>
      <a class=""search-tag tag-type-0"" data-tag-name=""arm_support"" data-is-deprecated=""false"" href=""/posts?tags=arm_support"">arm_support</a>
      <a class=""search-tag tag-type-0"" data-tag-name=""bangs"" data-is-deprecated=""false"" href=""/posts?tags=bangs"">bangs</a>
      <a class=""search-tag tag-type-0"" data-tag-name=""black_hair"" data-is-deprecated=""false"" href=""/posts?tags=black_hair"">black_hair</a>
      <a class=""search-tag tag-type-0"" data-tag-name=""black_legwear"" data-is-deprecated=""false"" href=""/posts?tags=black_legwear"">black_legwear</a>
      <a class=""search-tag tag-type-0"" data-tag-name=""blunt_bangs"" data-is-deprecated=""false"" href=""/posts?tags=blunt_bangs"">blunt_bangs</a>
      <a class=""search-tag tag-type-0"" data-tag-name=""blush"" data-is-deprecated=""false"" href=""/posts?tags=blush"">blush</a>
      <a class=""search-tag tag-type-0"" data-tag-name=""bob_cut"" data-is-deprecated=""false"" href=""/posts?tags=bob_cut"">bob_cut</a>
      <a class=""search-tag tag-type-0"" data-tag-name=""brown_footwear"" data-is-deprecated=""false"" href=""/posts?tags=brown_footwear"">brown_footwear</a>
      <a class=""search-tag tag-type-0"" data-tag-name=""brown_hair"" data-is-deprecated=""false"" href=""/posts?tags=brown_hair"">brown_hair</a>
      <a class=""search-tag tag-type-0"" data-tag-name=""closed_mouth"" data-is-deprecated=""false"" href=""/posts?tags=closed_mouth"">closed_mouth</a>
      <a class=""search-tag tag-type-0"" data-tag-name=""eyebrows_visible_through_hair"" data-is-deprecated=""false"" href=""/posts?tags=eyebrows_visible_through_hair"">eyebrows_visible_through_hair</a>
      <a class=""search-tag tag-type-0"" data-tag-name=""from_behind"" data-is-deprecated=""false"" href=""/posts?tags=from_behind"">from_behind</a>
      <a class=""search-tag tag-type-0"" data-tag-name=""full_body"" data-is-deprecated=""false"" href=""/posts?tags=full_body"">full_body</a>
      <a class=""search-tag tag-type-0"" data-tag-name=""green_skirt"" data-is-deprecated=""false"" href=""/posts?tags=green_skirt"">green_skirt</a>
      <a class=""search-tag tag-type-0"" data-tag-name=""hair_ornament"" data-is-deprecated=""false"" href=""/posts?tags=hair_ornament"">hair_ornament</a>
      <a class=""search-tag tag-type-0"" data-tag-name=""hair_ribbon"" data-is-deprecated=""false"" href=""/posts?tags=hair_ribbon"">hair_ribbon</a>
      <a class=""search-tag tag-type-0"" data-tag-name=""hairclip"" data-is-deprecated=""false"" href=""/posts?tags=hairclip"">hairclip</a>
      <a class=""search-tag tag-type-0"" data-tag-name=""hugging_own_legs"" data-is-deprecated=""false"" href=""/posts?tags=hugging_own_legs"">hugging_own_legs</a>
      <a class=""search-tag tag-type-0"" data-tag-name=""knees_up"" data-is-deprecated=""false"" href=""/posts?tags=knees_up"">knees_up</a>
      <a class=""search-tag tag-type-0"" data-tag-name=""loafers"" data-is-deprecated=""false"" href=""/posts?tags=loafers"">loafers</a>
      <a class=""search-tag tag-type-0"" data-tag-name=""long_sleeves"" data-is-deprecated=""false"" href=""/posts?tags=long_sleeves"">long_sleeves</a>
      <a class=""search-tag tag-type-0"" data-tag-name=""looking_at_viewer"" data-is-deprecated=""false"" href=""/posts?tags=looking_at_viewer"">looking_at_viewer</a>
      <a class=""search-tag tag-type-0"" data-tag-name=""looking_back"" data-is-deprecated=""false"" href=""/posts?tags=looking_back"">looking_back</a>
      <a class=""search-tag tag-type-0"" data-tag-name=""miniskirt"" data-is-deprecated=""false"" href=""/posts?tags=miniskirt"">miniskirt</a>
      <a class=""search-tag tag-type-0"" data-tag-name=""multiple_girls"" data-is-deprecated=""false"" href=""/posts?tags=multiple_girls"">multiple_girls</a>
      <a class=""search-tag tag-type-0"" data-tag-name=""pantyhose"" data-is-deprecated=""false"" href=""/posts?tags=pantyhose"">pantyhose</a>
      <a class=""search-tag tag-type-0"" data-tag-name=""ponytail"" data-is-deprecated=""false"" href=""/posts?tags=ponytail"">ponytail</a>
      <a class=""search-tag tag-type-0"" data-tag-name=""profile"" data-is-deprecated=""false"" href=""/posts?tags=profile"">profile</a>
      <a class=""search-tag tag-type-0"" data-tag-name=""purple_eyes"" data-is-deprecated=""false"" href=""/posts?tags=purple_eyes"">purple_eyes</a>
      <a class=""search-tag tag-type-0"" data-tag-name=""red_eyes"" data-is-deprecated=""false"" href=""/posts?tags=red_eyes"">red_eyes</a>
      <a class=""search-tag tag-type-0"" data-tag-name=""red_ribbon"" data-is-deprecated=""false"" href=""/posts?tags=red_ribbon"">red_ribbon</a>
      <a class=""search-tag tag-type-0"" data-tag-name=""ribbon"" data-is-deprecated=""false"" href=""/posts?tags=ribbon"">ribbon</a>
      <a class=""search-tag tag-type-0"" data-tag-name=""school_uniform"" data-is-deprecated=""false"" href=""/posts?tags=school_uniform"">school_uniform</a>
      <a class=""search-tag tag-type-0"" data-tag-name=""shirt"" data-is-deprecated=""false"" href=""/posts?tags=shirt"">shirt</a>
      <a class=""search-tag tag-type-0"" data-tag-name=""shoes"" data-is-deprecated=""false"" href=""/posts?tags=shoes"">shoes</a>
      <a class=""search-tag tag-type-0"" data-tag-name=""short_hair"" data-is-deprecated=""false"" href=""/posts?tags=short_hair"">short_hair</a>
      <a class=""search-tag tag-type-0"" data-tag-name=""sidelocks"" data-is-deprecated=""false"" href=""/posts?tags=sidelocks"">sidelocks</a>
      <a class=""search-tag tag-type-0"" data-tag-name=""simple_background"" data-is-deprecated=""false"" href=""/posts?tags=simple_background"">simple_background</a>
      <a class=""search-tag tag-type-0"" data-tag-name=""skirt"" data-is-deprecated=""false"" href=""/posts?tags=skirt"">skirt</a>
      <a class=""search-tag tag-type-0"" data-tag-name=""socks"" data-is-deprecated=""false"" href=""/posts?tags=socks"">socks</a>
      <a class=""search-tag tag-type-0"" data-tag-name=""untucked_shirt"" data-is-deprecated=""false"" href=""/posts?tags=untucked_shirt"">untucked_shirt</a>
      <a class=""search-tag tag-type-0"" data-tag-name=""white_legwear"" data-is-deprecated=""false"" href=""/posts?tags=white_legwear"">white_legwear</a>
      <a class=""search-tag tag-type-0"" data-tag-name=""white_shirt"" data-is-deprecated=""false"" href=""/posts?tags=white_shirt"">white_shirt</a>
</div>

  </div>
</div>
";

    const string POST_PREVIEW_RAW_2 = @"
<div class=""post-tooltip-header"">
  <a class=""user user-admin  user-post-approver user-post-uploader"" data-user-id=""1"" data-user-name=""albert"" data-user-level=""50"" href=""/users/1"">albert</a>

  <span class=""post-tooltip-favorites post-tooltip-info"">
    <span>19</span>
    <i class=""icon far fa-heart fa-xs""></i>
  </span>

  <span class=""post-tooltip-score post-tooltip-info"">
    <span>2</span>
    <svg class=""icon svg-icon upvote-icon "" role=""img"" xmlns=""http://www.w3.org/2000/svg"" viewBox=""0 0 448 512""><path fill=""currentColor"" d=""M272 480h-96c-13.3 0-24-10.7-24-24V256H48.2c-21.4 0-32.1-25.8-17-41L207 39c9.4-9.4 24.6-9.4 34 0l175.8 176c15.1 15.1 4.4 41-17 41H296v200c0 13.3-10.7 24-24 24z"" /></svg>
  </span>

    <span class=""post-tooltip-comments post-tooltip-info"">
      <span>3</span>
      <i class=""icon far fa-comments fa-xs""></i>
    </span>

  <a class=""post-tooltip-date post-tooltip-info"" href=""/posts?tags=date%3A2005-05-24"">
    <time datetime=""2005-05-24T00:24-04:00"" title=""2005-05-24 00:24:22 -0400"" class=""compact-timestamp"">17y</time> ago
</a>
    <a class=""post-tooltip-source post-tooltip-info"" href=""/posts?tags=source%3Anone"">no source</a>

  <a class=""post-tooltip-rating post-tooltip-info"" href=""/posts?tags=rating%3AGeneral"">G</a>
  <a class=""post-tooltip-dimensions post-tooltip-info"" href=""https://cdn.donmai.us/original/7c/2b/7c2b4c3197d68c69488643d74815e790.png"">400x400</a>

  <div class=""popup-menu inline-block "">
  <a class=""popup-menu-button inline-block rounded p-1"" href=""javascript:void(0)"">
      <i class=""icon fas fa-ellipsis-h ""></i>
  </a>

  <ul class=""popup-menu-content"">
      <li>      <a class=""post-tooltip-disable"" href=""javascript:void(0)"">
        <i class=""icon fas fa-times ""></i> Disable tooltips
</a></li>
  </ul>
</div>
</div>

<div class=""post-tooltip-body thin-scrollbar "">
  <div class=""post-tooltip-body-left"">
  </div>

  <div class=""post-tooltip-body-right"">
    <div class=""post-tooltip-pools"">
    </div>

    <div class=""tag-list inline-tag-list"">
      <a class=""search-tag tag-type-3"" data-tag-name=""iriya_no_sora_ufo_no_natsu"" data-is-deprecated=""false"" href=""/posts?tags=iriya_no_sora_ufo_no_natsu"">iriya_no_sora_ufo_no_natsu</a>
      <a class=""search-tag tag-type-4"" data-tag-name=""iriya_kana"" data-is-deprecated=""false"" href=""/posts?tags=iriya_kana"">iriya_kana</a>
      <a class=""search-tag tag-type-5"" data-tag-name=""artist_request"" data-is-deprecated=""false"" href=""/posts?tags=artist_request"">artist_request</a>
      <a class=""search-tag tag-type-5"" data-tag-name=""lowres"" data-is-deprecated=""false"" href=""/posts?tags=lowres"">lowres</a>
      <a class=""search-tag tag-type-5"" data-tag-name=""source_request"" data-is-deprecated=""false"" href=""/posts?tags=source_request"">source_request</a>
      <a class=""search-tag tag-type-5"" data-tag-name=""translated"" data-is-deprecated=""false"" href=""/posts?tags=translated"">translated</a>
      <a class=""search-tag tag-type-0"" data-tag-name=""1girl"" data-is-deprecated=""false"" href=""/posts?tags=1girl"">1girl</a>
      <a class=""search-tag tag-type-0"" data-tag-name="":o"" data-is-deprecated=""false"" href=""/posts?tags=%3Ao"">:o</a>
      <a class=""search-tag tag-type-0"" data-tag-name=""ahoge"" data-is-deprecated=""false"" href=""/posts?tags=ahoge"">ahoge</a>
      <a class=""search-tag tag-type-0"" data-tag-name=""aircraft"" data-is-deprecated=""false"" href=""/posts?tags=aircraft"">aircraft</a>
      <a class=""search-tag tag-type-0"" data-tag-name=""arm_behind_head"" data-is-deprecated=""false"" href=""/posts?tags=arm_behind_head"">arm_behind_head</a>
      <a class=""search-tag tag-type-0"" data-tag-name=""arm_up"" data-is-deprecated=""false"" href=""/posts?tags=arm_up"">arm_up</a>
      <a class=""search-tag tag-type-0"" data-tag-name=""bangs"" data-is-deprecated=""false"" href=""/posts?tags=bangs"">bangs</a>
      <a class=""search-tag tag-type-0"" data-tag-name=""blood"" data-is-deprecated=""false"" href=""/posts?tags=blood"">blood</a>
      <a class=""search-tag tag-type-0"" data-tag-name=""blush_stickers"" data-is-deprecated=""false"" href=""/posts?tags=blush_stickers"">blush_stickers</a>
      <a class=""search-tag tag-type-0"" data-tag-name=""breasts"" data-is-deprecated=""false"" href=""/posts?tags=breasts"">breasts</a>
      <a class=""search-tag tag-type-0"" data-tag-name=""cloud"" data-is-deprecated=""false"" href=""/posts?tags=cloud"">cloud</a>
      <a class=""search-tag tag-type-0"" data-tag-name=""day"" data-is-deprecated=""false"" href=""/posts?tags=day"">day</a>
      <a class=""search-tag tag-type-0"" data-tag-name=""eyebrows_visible_through_hair"" data-is-deprecated=""false"" href=""/posts?tags=eyebrows_visible_through_hair"">eyebrows_visible_through_hair</a>
      <a class=""search-tag tag-type-0"" data-tag-name=""from_side"" data-is-deprecated=""false"" href=""/posts?tags=from_side"">from_side</a>
      <a class=""search-tag tag-type-0"" data-tag-name=""glint"" data-is-deprecated=""false"" href=""/posts?tags=glint"">glint</a>
      <a class=""search-tag tag-type-0"" data-tag-name=""head_tilt"" data-is-deprecated=""false"" href=""/posts?tags=head_tilt"">head_tilt</a>
      <a class=""search-tag tag-type-0"" data-tag-name=""long_hair"" data-is-deprecated=""false"" href=""/posts?tags=long_hair"">long_hair</a>
      <a class=""search-tag tag-type-0"" data-tag-name=""looking_at_viewer"" data-is-deprecated=""false"" href=""/posts?tags=looking_at_viewer"">looking_at_viewer</a>
      <a class=""search-tag tag-type-0"" data-tag-name=""looking_back"" data-is-deprecated=""false"" href=""/posts?tags=looking_back"">looking_back</a>
      <a class=""search-tag tag-type-0"" data-tag-name=""nosebleed"" data-is-deprecated=""false"" href=""/posts?tags=nosebleed"">nosebleed</a>
      <a class=""search-tag tag-type-0"" data-tag-name=""oekaki"" data-is-deprecated=""false"" href=""/posts?tags=oekaki"">oekaki</a>
      <a class=""search-tag tag-type-0"" data-tag-name=""outdoors"" data-is-deprecated=""false"" href=""/posts?tags=outdoors"">outdoors</a>
      <a class=""search-tag tag-type-0"" data-tag-name=""parted_lips"" data-is-deprecated=""false"" href=""/posts?tags=parted_lips"">parted_lips</a>
      <a class=""search-tag tag-type-0"" data-tag-name=""purple_eyes"" data-is-deprecated=""false"" href=""/posts?tags=purple_eyes"">purple_eyes</a>
      <a class=""search-tag tag-type-0"" data-tag-name=""purple_hair"" data-is-deprecated=""false"" href=""/posts?tags=purple_hair"">purple_hair</a>
      <a class=""search-tag tag-type-0"" data-tag-name=""school_uniform"" data-is-deprecated=""false"" href=""/posts?tags=school_uniform"">school_uniform</a>
      <a class=""search-tag tag-type-0"" data-tag-name=""serafuku"" data-is-deprecated=""false"" href=""/posts?tags=serafuku"">serafuku</a>
      <a class=""search-tag tag-type-0"" data-tag-name=""shirt"" data-is-deprecated=""false"" href=""/posts?tags=shirt"">shirt</a>
      <a class=""search-tag tag-type-0"" data-tag-name=""short_sleeves"" data-is-deprecated=""false"" href=""/posts?tags=short_sleeves"">short_sleeves</a>
      <a class=""search-tag tag-type-0"" data-tag-name=""sky"" data-is-deprecated=""false"" href=""/posts?tags=sky"">sky</a>
      <a class=""search-tag tag-type-0"" data-tag-name=""small_breasts"" data-is-deprecated=""false"" href=""/posts?tags=small_breasts"">small_breasts</a>
      <a class=""search-tag tag-type-0"" data-tag-name=""solo"" data-is-deprecated=""false"" href=""/posts?tags=solo"">solo</a>
      <a class=""search-tag tag-type-0"" data-tag-name=""surprised"" data-is-deprecated=""false"" href=""/posts?tags=surprised"">surprised</a>
      <a class=""search-tag tag-type-0"" data-tag-name=""ufo"" data-is-deprecated=""false"" href=""/posts?tags=ufo"">ufo</a>
      <a class=""search-tag tag-type-0"" data-tag-name=""upper_body"" data-is-deprecated=""false"" href=""/posts?tags=upper_body"">upper_body</a>
      <a class=""search-tag tag-type-0"" data-tag-name=""white_shirt"" data-is-deprecated=""false"" href=""/posts?tags=white_shirt"">white_shirt</a>
</div>

  </div>
</div>
";

    const string POST_PREVIEW_RAW_3 = @"
<div class=""post-tooltip-header"">
  <a class=""user user-builder  user-post-uploader"" data-user-id=""782896"" data-user-name=""Ardouru"" data-user-level=""32"" href=""/users/782896"">Ardouru</a>

  <span class=""post-tooltip-favorites post-tooltip-info"">
    <span>10</span>
    <i class=""icon far fa-heart fa-xs""></i>
  </span>

  <span class=""post-tooltip-score post-tooltip-info"">
    <span>15</span>
    <svg class=""icon svg-icon upvote-icon "" role=""img"" xmlns=""http://www.w3.org/2000/svg"" viewBox=""0 0 448 512""><path fill=""currentColor"" d=""M272 480h-96c-13.3 0-24-10.7-24-24V256H48.2c-21.4 0-32.1-25.8-17-41L207 39c9.4-9.4 24.6-9.4 34 0l175.8 176c15.1 15.1 4.4 41-17 41H296v200c0 13.3-10.7 24-24 24z"" /></svg>
  </span>


  <a class=""post-tooltip-date post-tooltip-info"" href=""/posts?tags=date%3A2022-05-29"">
    <time datetime=""2022-05-29T11:36-04:00"" title=""2022-05-29 11:36:15 -0400"" class=""compact-timestamp"">1d</time> ago
</a>
    <a class=""post-tooltip-source post-tooltip-info"" href=""https://twitter.com/swing94139193/status/1530521038424928257"">twitter.com</a>

  <a class=""post-tooltip-rating post-tooltip-info"" href=""/posts?tags=rating%3AGeneral"">G</a>
  <a class=""post-tooltip-dimensions post-tooltip-info"" href=""https://cdn.donmai.us/original/ee/8d/ee8daff3bed3d0d2430af3e8a736e86a.jpg"">1628x874</a>

  <div class=""popup-menu inline-block "">
  <a class=""popup-menu-button inline-block rounded p-1"" href=""javascript:void(0)"">
      <i class=""icon fas fa-ellipsis-h ""></i>
  </a>

  <ul class=""popup-menu-content"">
      <li>      <a class=""post-tooltip-disable"" href=""javascript:void(0)"">
        <i class=""icon fas fa-times ""></i> Disable tooltips
</a></li>
  </ul>
</div>
</div>

<div class=""post-tooltip-body thin-scrollbar "">
  <div class=""post-tooltip-body-left"">
  </div>

  <div class=""post-tooltip-body-right"">
    <div class=""post-tooltip-pools"">
    </div>

    <div class=""tag-list inline-tag-list"">
      <a class=""search-tag tag-type-1"" data-tag-name=""suzu_(susan_slr97)"" data-is-deprecated=""false"" href=""/posts?tags=suzu_%28susan_slr97%29"">suzu_(susan_slr97)</a>
      <a class=""search-tag tag-type-3"" data-tag-name=""touhou"" data-is-deprecated=""false"" href=""/posts?tags=touhou"">touhou</a>
      <a class=""search-tag tag-type-5"" data-tag-name=""commentary"" data-is-deprecated=""false"" href=""/posts?tags=commentary"">commentary</a>
      <a class=""search-tag tag-type-5"" data-tag-name=""highres"" data-is-deprecated=""false"" href=""/posts?tags=highres"">highres</a>
      <a class=""search-tag tag-type-0"" data-tag-name=""blue_flower"" data-is-deprecated=""false"" href=""/posts?tags=blue_flower"">blue_flower</a>
      <a class=""search-tag tag-type-0"" data-tag-name=""building"" data-is-deprecated=""false"" href=""/posts?tags=building"">building</a>
      <a class=""search-tag tag-type-0"" data-tag-name=""bush"" data-is-deprecated=""false"" href=""/posts?tags=bush"">bush</a>
      <a class=""search-tag tag-type-0"" data-tag-name=""cloud"" data-is-deprecated=""false"" href=""/posts?tags=cloud"">cloud</a>
      <a class=""search-tag tag-type-0"" data-tag-name=""day"" data-is-deprecated=""false"" href=""/posts?tags=day"">day</a>
      <a class=""search-tag tag-type-0"" data-tag-name=""flower"" data-is-deprecated=""false"" href=""/posts?tags=flower"">flower</a>
      <a class=""search-tag tag-type-0"" data-tag-name=""forest"" data-is-deprecated=""false"" href=""/posts?tags=forest"">forest</a>
      <a class=""search-tag tag-type-0"" data-tag-name=""leaf"" data-is-deprecated=""false"" href=""/posts?tags=leaf"">leaf</a>
      <a class=""search-tag tag-type-0"" data-tag-name=""mansion"" data-is-deprecated=""false"" href=""/posts?tags=mansion"">mansion</a>
      <a class=""search-tag tag-type-0"" data-tag-name=""meadow"" data-is-deprecated=""false"" href=""/posts?tags=meadow"">meadow</a>
      <a class=""search-tag tag-type-0"" data-tag-name=""mountainous_horizon"" data-is-deprecated=""false"" href=""/posts?tags=mountainous_horizon"">mountainous_horizon</a>
      <a class=""search-tag tag-type-0"" data-tag-name=""nature"" data-is-deprecated=""false"" href=""/posts?tags=nature"">nature</a>
      <a class=""search-tag tag-type-0"" data-tag-name=""no_humans"" data-is-deprecated=""false"" href=""/posts?tags=no_humans"">no_humans</a>
      <a class=""search-tag tag-type-0"" data-tag-name=""path"" data-is-deprecated=""false"" href=""/posts?tags=path"">path</a>
      <a class=""search-tag tag-type-0"" data-tag-name=""rock"" data-is-deprecated=""false"" href=""/posts?tags=rock"">rock</a>
      <a class=""search-tag tag-type-0"" data-tag-name=""scarlet_devil_mansion"" data-is-deprecated=""false"" href=""/posts?tags=scarlet_devil_mansion"">scarlet_devil_mansion</a>
      <a class=""search-tag tag-type-0"" data-tag-name=""scenery"" data-is-deprecated=""false"" href=""/posts?tags=scenery"">scenery</a>
      <a class=""search-tag tag-type-0"" data-tag-name=""sky"" data-is-deprecated=""false"" href=""/posts?tags=sky"">sky</a>
      <a class=""search-tag tag-type-0"" data-tag-name=""tree"" data-is-deprecated=""false"" href=""/posts?tags=tree"">tree</a>
      <a class=""search-tag tag-type-0"" data-tag-name=""white_flower"" data-is-deprecated=""false"" href=""/posts?tags=white_flower"">white_flower</a>
      <a class=""search-tag tag-type-0"" data-tag-name=""yellow_flower"" data-is-deprecated=""false"" href=""/posts?tags=yellow_flower"">yellow_flower</a>
</div>

  </div>
</div>
";
}
