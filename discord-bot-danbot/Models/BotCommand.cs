using System;

namespace discord_bot_danbot.Models;

public record BotCommandParameter(
    string Name,
    string[] Values);

public record BotCommand(
    string Command,
    BotCommandParameter[] Parameters,
    BotCommand? SubCommand);
