using System;

namespace discord_bot_danbot.Models;

public record Post(
    long Id,
    DateTime Time,
    string? Artist,
    string[] CharacterTags,
    string[] Tags,
    string MediaUrl);
