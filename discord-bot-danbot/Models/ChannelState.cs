using System;

namespace discord_bot_danbot.Models;

public record ChannelSubscription(
    string[] Tags,
    string[] Excludes,
    long LastPostId);

public record ChannelBotCommand(
    string MessageId,
    string SenderId,
    int CurrentPage,
    BotCommand Command);

public record ChannelState(
    string Id,
    ChannelSubscription[] Subscriptions,
    ChannelBotCommand[] Commands,
    string[] Excludes);
