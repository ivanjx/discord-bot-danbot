using System;
using System.Net.Http;
using System.Threading.Tasks;

namespace discord_bot_danbot.Services;

public interface IApiService
{
    Task<string> DownloadStringAsync(string path);
    Task<byte[]> DownloadDataAsync(string path);
}

public class ApiService : IApiService
{
    HttpClient m_client;

    public ApiService()
    {
        m_client = new HttpClient();
    }

    public Task<byte[]> DownloadDataAsync(string path)
    {
        return m_client.GetByteArrayAsync(path);
    }

    public Task<string> DownloadStringAsync(string path)
    {
        return m_client.GetStringAsync(path);
    }
}
