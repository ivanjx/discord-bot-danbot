using System;
using System.Collections.Generic;
using System.Linq;
using discord_bot_danbot.Exceptions;
using discord_bot_danbot.Models;
using HtmlAgilityPack;

namespace discord_bot_danbot.Services;

public interface IDanParserService
{
    Post[] ParsePostIdsAndTags(string raw);
    Post ParsePost(string raw);
    Post ParsePostPreview(long id, string raw);
}

public class DanParserService : IDanParserService
{
    public Post[] ParsePostIdsAndTags(string raw)
    {
        HtmlDocument html = new HtmlDocument();
        html.LoadHtml(raw);
        HtmlNodeCollection? postNodes = html.DocumentNode
            .SelectSingleNode(@".//div[@class=""posts-container""]")?
            .SelectNodes(@".//article");
        
        if (postNodes == null)
        {
            return new Post[0];
        }
        
        List<Post> result = new List<Post>();

        foreach (HtmlNode postNode in postNodes)
        {
            string idStr = postNode.GetAttributeValue("data-id", "");
            long id;

            if (idStr == "" ||
                !long.TryParse(idStr, out id))
            {
                continue;
            }

            string tagsStr = postNode.GetAttributeValue("data-tags", "");

            if (tagsStr == "")
            {
                continue;
            }

            string[] tags = tagsStr.Split(
                ' ',
                StringSplitOptions.RemoveEmptyEntries);
            result.Add(
                new Post(
                    id,
                    DateTime.MinValue,
                    null,
                    new string[0],
                    tags,
                    ""));
        }

        return result.ToArray();
    }

    public Post ParsePost(string raw)
    {
        HtmlDocument html = new HtmlDocument();
        html.LoadHtml(raw);
        HtmlNode rootNode = html.DocumentNode;

        // Parsing id.
        HtmlNode? idNode = rootNode
            .SelectSingleNode(@".//li[@id=""post-info-id""]");
        
        if (idNode == null)
        {
            throw new InvalidDanApiResponseException("No post id");
        }

        string idStr = idNode.InnerText
            .Split(':')
            .Last()
            .Trim();
        long id;

        if (!long.TryParse(idStr, out id))
        {
            throw new InvalidDanApiResponseException("Invalid post id");
        }

        // Parsing time.
        HtmlNode? timeNode = rootNode
            .SelectSingleNode(@".//li[@id=""post-info-date""]")?
            .SelectSingleNode(".//time");
        
        if (timeNode == null)
        {
            throw new InvalidDanApiResponseException("No post time");
        }

        string timeStr = timeNode.GetAttributeValue("datetime", "");
        DateTime time;

        if (timeStr == "" ||
            !DateTime.TryParse(timeStr, out time))
        {
            throw new InvalidDanApiResponseException("Invalid post time");
        }

        // Parsing artist.
        HtmlNode? artistNode = rootNode
            .SelectSingleNode(@".//ul[@class=""artist-tag-list""]")?
            .SelectSingleNode(@".//li");
        string? artist = null;
        
        if (artistNode != null)
        {
            artist = artistNode.GetAttributeValue("data-tag-name", "");
            
            if (artist == "")
            {
                artist = null;
            }
        }

        // Parsing character tags.
        HtmlNodeCollection? characterNodes = rootNode
            .SelectSingleNode(@".//ul[@class=""character-tag-list""]")?
            .SelectNodes(".//li");
        List<string> characters = new List<string>();

        if (characterNodes != null)
        {
            foreach (HtmlNode characterNode in characterNodes)
            {
                string character = characterNode.GetAttributeValue(
                    "data-tag-name",
                    "");
                
                if (character != "")
                {
                    characters.Add(character);
                }
            }
        }

        // Parsing tags.
        HtmlNodeCollection? tagNodes = rootNode
            .SelectSingleNode(@".//ul[@class=""general-tag-list""]")?
            .SelectNodes(".//li");
        List<string> tags = new List<string>();

        if (tagNodes != null)
        {
            foreach (HtmlNode tagNode in tagNodes)
            {
                string tag = tagNode.GetAttributeValue(
                    "data-tag-name",
                    "");
                
                if (tag != "")
                {
                    tags.Add(tag);
                }
            }
        }

        // Parsing media url.
        HtmlNode? urlNode = rootNode
            .SelectSingleNode(@".//li[@id=""post-info-size""]")?
            .SelectSingleNode(@".//a[contains(@href, ""http"")]");
        
        if (urlNode == null)
        {
            throw new InvalidDanApiResponseException("No post media url");
        }

        string url = urlNode.GetAttributeValue("href", "");

        if (url == "")
        {
            throw new InvalidDanApiResponseException("Invalid post media url");
        }

        // Done.
        return new Post(
            id,
            time,
            artist,
            characters.ToArray(),
            tags.ToArray(),
            url);
    }

    public Post ParsePostPreview(long id, string raw)
    {
        HtmlDocument html = new HtmlDocument();
        html.LoadHtml(raw);
        HtmlNode rootNode = html.DocumentNode;

        // Parsing time.
        HtmlNode? timeNode = rootNode
            .SelectSingleNode(@".//a[contains(@class, ""post-tooltip-date"")]")?
            .SelectSingleNode(".//time");
        
        if (timeNode == null)
        {
            throw new InvalidDanApiResponseException("No post time");
        }

        string timeStr = timeNode.GetAttributeValue("datetime", "");
        DateTime time;

        if (timeStr == "" ||
            !DateTime.TryParse(timeStr, out time))
        {
            throw new InvalidDanApiResponseException("Invalid post time");
        }

        // Parsing artist.
        HtmlNode? artistNode = rootNode
            .SelectSingleNode(@".//div[contains(@class, ""tag-list"")]")?
            .SelectSingleNode(@".//a[contains(@class, ""tag-type-1"")]");
        string? artist = null;
        
        if (artistNode != null)
        {
            artist = artistNode.GetAttributeValue("data-tag-name", "");
            
            if (artist == "")
            {
                artist = null;
            }
        }

        // Parsing character tags.
        HtmlNodeCollection? characterNodes = rootNode
            .SelectSingleNode(@".//div[contains(@class, ""tag-list"")]")?
            .SelectNodes(@".//a[contains(@class, ""tag-type-4"")]");
        List<string> characters = new List<string>();

        if (characterNodes != null)
        {
            foreach (HtmlNode characterNode in characterNodes)
            {
                string character = characterNode.GetAttributeValue(
                    "data-tag-name",
                    "");
                
                if (character != "")
                {
                    characters.Add(character);
                }
            }
        }

        // Parsing tags.
        HtmlNodeCollection? tagNodes = rootNode
            .SelectSingleNode(@".//div[contains(@class, ""tag-list"")]")?
            .SelectNodes(@".//a[contains(@class, ""tag-type-0"")]");
        List<string> tags = new List<string>();

        if (tagNodes != null)
        {
            foreach (HtmlNode tagNode in tagNodes)
            {
                string tag = tagNode.GetAttributeValue(
                    "data-tag-name",
                    "");
                
                if (tag != "")
                {
                    tags.Add(tag);
                }
            }
        }

        // Parsing media url.
        HtmlNode? urlNode = rootNode
            .SelectSingleNode(@".//a[contains(@class, ""post-tooltip-dimensions"")]");
        
        if (urlNode == null)
        {
            throw new InvalidDanApiResponseException("No post media url");
        }

        string url = urlNode.GetAttributeValue("href", "");

        if (url == "")
        {
            throw new InvalidDanApiResponseException("Invalid post media url");
        }

        // Done.
        return new Post(
            id,
            time,
            artist,
            characters.ToArray(),
            tags.ToArray(),
            url);
    }
}
