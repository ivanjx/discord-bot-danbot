using System;
using discord_bot_danbot.Models;

namespace discord_bot_danbot.Services;

public interface IBotCommandParserService
{
    BotCommand Parse(string command);
}

public class BotCommandParserService : IBotCommandParserService
{
    public BotCommand Parse(string command)
    {
        throw new NotImplementedException();
    }
}
