using System;

namespace discord_bot_danbot.Exceptions;

public class InvalidDanApiResponseException : Exception
{
    public InvalidDanApiResponseException(string details) :
        base("Invalid Dan api response: " + details)
    {
    }
}
